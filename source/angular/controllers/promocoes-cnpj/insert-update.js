app.controller('PromotionStationsInserirCtrl', [
	'$scope'
	, '$rootScope'
	, '$routeParams'
	, 'API'
	, '$q'
	, '_'
	, function(ng, ngRoot, $routeParams, API, $q, _) {

		ng.insert = function(form) {

			ng.formData = true;

			if (form.$valid) {

				ng.loaderPromotionStationsPromotion = true;

				var objSend = ng.user_promotionstations;
				objSend.promotions = ng.idPromo;

				API.insertPromotionStations(objSend)
				.then(function(response) {

					ngRoot.notification.title = 'Inclusão realizada com sucesso :)';
					ngRoot.notification.type  = 'success';
					notify(ngRoot.notification);
					ng.formData = false;

					ng.user_promotionstations = {};

				}, function(response) {

					if (response.status == 409) {
						ngRoot.notification.type  = 'error';
						ngRoot.notification.title = response.data.message;
					} else {
						ngRoot.notification.type  = 'error';
						ngRoot.notification.title = 'Erro inesperado :(';
					}

					notify(ngRoot.notification);

				})
				.finally(function() {

					ng.loaderPromotionStationsPromotion = false;

				})
			}

		}

		var init = function() {
			ng.formData = false;
			ng.idPromo = $routeParams.idPromo;

		}
		init();
	}
]);


