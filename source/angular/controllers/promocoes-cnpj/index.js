app.controller('PromotionStationsCtrl', [
	'$scope'
	, '$rootScope'
	, 'API'
	, '_'
	, '$filter'
	, '$routeParams'
	, function(ng, ngRoot, API, _, $filter, $routeParams) {

		// PAGINACAO
		ng.goToPage = function(event) {
			if (event.charCode == 13) {
				if (ng.filters.currentPage == 0) {
					ng.init = [];
				} else {
					ng.get_promotion_stations();
				}
			}
		}

		ng.nextPage = function() {
			if (ng.filters.currentPage < ng.filters.amtPages) {
				ng.filters.currentPage++;
				ng.get_promotion_stations();
			}
		}
		ng.previousPage = function() {
			if (ng.filters.currentPage > 1) {
				ng.filters.currentPage--;
				ng.get_promotion_stations();
			}
		}

		ng.delete = function (id) {

			if (ng.loaderListPromotionStations == false) {

				ng.loaderListPromotionStations = true;
//ng.idPromo
				API.deleteUpdatePromotionStations(ng.idPromo, id)
				.then(function(response){

					ngRoot.notification.title = 'Usuário removido com sucesso :)';
					ngRoot.notification.type  = 'success';
					notify(ngRoot.notification);

					ng.get_promotion_stations();

				}, function(response){

					ngRoot.notification.title = 'Erro ao remover usuário :(';
					ngRoot.notification.type  = 'error';
					notify(ngRoot.notification);

				})
			}
		}


		ng.get_promotion_stations = function(resetPage) {

			if (resetPage) {
				ng.filters.currentPage = 1;
				ng.calcSkip();
			}

			ng.loaderListPromotionStations = true;
			ng.listPromotionStations = [];

			var filtro = {
				'page'   : ng.filters.currentPage-1
				, 'size' : ng.filters.amtItemsPage
				, 'cnpj'  : ng.filters.cnpj
			}

			API.getPromotionStations( ng.idPromo , filtro)
			.then(function(response){

				if (response.data.items.length > 0) {

					ng.filters.totalItems = response.data.totalItems;
					ng.filters.amtPages   = response.data.pageCount;

					angular.forEach(response.data.items, function(value, index){
						value.date = value.date ? $filter('date')(value.date, 'dd/MM/yyyy H:mm:ss') : '-';
						value.cnpj  = value.cnpj ? $filter('cnpj')(value.cnpj) : '-';
						ng.listPromotionStations.push(value);
					})
				}

			})
			.finally(function(){
				 ng.loaderListPromotionStations = false;
			})
		}


		ng.init = function() {

			ng.filters    = {
				amtItemsPage    : 10
				, amtPages      : 0
				, currentPage   : 1
				, codigoposto   : ''
				, loaderReports : false
				, totalItems    : 0
				, cnpj           : null
			}

			ng.idPromo    = $routeParams.idPromo;

			ng.listAction = [
				{
					label    : 'Excluir'
					, fn     : 'delete'
					, param  : ['id']
				}
			];

			ng.get_promotion_stations();
		}

		ng.init();
	}
]);


