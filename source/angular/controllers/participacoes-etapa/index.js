app.controller('ParticipacoesEtapaCtrl', [
	'$scope'
	, '$rootScope'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, '$timeout'
	, function(ng, ngRoot, $routeParams, _, API, $q, $filter, $timeout) {

		ng.getReports = function() {
			ng.loaderReports = true;

			API.getParticipationStep($routeParams.idPromo, 0, 50)
			.then(function(response) {
				// SUCESSO
				var data = response.data;
				if (!data) {
					return $q.reject(response);
				}

				// SUCESSO
				if (!response.data) {
					return $q.reject(response);
				}
				if (response.data.records.length > 0) {
					ng.filters.totalItems = response.data.totalRecordsCount;
					ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
					ng.reports = response.data.records;
				}

				var labels     = [];
				var aa         = [];
				var bb         = [];
				var cc         = [];
				var dd         = [];
				var ee         = [];


				var registros = 0
				approvedTransactions_TOTAL = 0
				cupomTransactions_TOTAL = 0
				invalidTransactions_TOTAL = 0
				moderated_TOTAL = 0
				payPallTransactions_TOTAL = 0
				pendingToModeration_TOTAL = 0
				registeredUsers_TOTAL = 0
				rejectedTransactions_TOTAL = 0
				totalTransactions_TOTAL = 0
				transactedCNPJ_TOTAL = 0
				unmoderateTransactions_TOTAL = 0
				transactedCNPJTotal_TOTAL = 0
				typeCount = ''

				angular.forEach(data.records, function (value, key) {


					labels.push('Etapa '+ value.Etapa);
					typeCount = 'Etapa'


					aa.push(value.Aprovado);
					bb.push(value.EmCurso);
					cc.push(value.EmModeracao);
					dd.push(value.Expirado);
					ee.push(value.Reprovado);
					totalparcial = value.Aprovado + value.EmCurso + value.EmModeracao + value.Expirado + value.Reprovado;
					ng.totalreport1 += totalparcial;
					//ff.push(totalparcial)
					ng.total['Aprovado'] += value.Aprovado;
					ng.total['EmCurso'] += value.EmCurso;
					ng.total['EmModeracao'] += value.EmModeracao;
					ng.total['Expirado'] += value.Expirado;
					ng.total['Reprovado'] += value.Reprovado;



					registros += 1
				});

				if (data){
					registeredUsers_TOTAL = data.registeredUsers;
					transactedCNPJTotal_TOTAL = data.transactedCNPJTotal;
				}



				ng.chartReport = {
					x: {
						labels: labels
					}
					, y: {
						label: 'Valores'
					}
					, exporting: {
						enabled : true
						, filename: 'grafico'
					}
					, series: [
						, {
							label: 'Aprovado'
							, value: aa
						}
						, {
							label: 'Em Curso'
							, value: bb
						}
						, {
							label: 'Em Moderacao'
							, value: cc
						}
						, {
							label: 'Expirado'
							, value: dd
						}
						, {
							label: 'Reprovado'
							, value: ee
						}
					]
				}

			}, function() {
				ng.reports = [];
			})
			.finally(function() {
				ng.loaderReports = false;
			})

		}


		ng.getReports2 = function() {
			ng.loaderReports = true;

			API.getWinnerStep($routeParams.idPromo, 0, 50)
			.then(function(response) {
				// SUCESSO
				var data = response.data;
				if (!data) {
					return $q.reject(response);
				}
				// SUCESSO
				if (!response.data) {
					return $q.reject(response);
				}
				if (response.data.records.length > 0) {
					ng.filters.totalItems = response.data.totalRecordsCount;
					ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
					ng.reports2 = response.data.records;
				}

				var labels     = [];
				var aa         = [];
				var bb         = [];
				var cc         = [];
				var dd         = [];
				var ee         = [];
				var ff         = [];


				var registros = 0

				angular.forEach(data.records, function (value, key) {


					labels.push('Etapa '+ value.Etapa);
					typeCount = 'Etapa'


					aa.push(value.QtdUsuariosEmEmModeracao);
					bb.push(value.QtdGenhadores);
					cc.push(value.QtdGanhadoresUnicos);
					dd.push(value.QtdMilhasPadrao);
					ee.push(value.QtdMilhasShell);
					ff.push(value.QtdMilhasSmiles);
					totalparcial = value.QtdUsuariosEmEmModeracao + value.QtdGenhadores + value.QtdGanhadoresUnicos + value.QtdMilhasPadrao + value.QtdMilhasShell + value.QtdMilhasSmiles;
					//ff.push(totalparcial)
					ng.total['QtdUsuariosEmEmModeracao'] += value.QtdUsuariosEmEmModeracao;
					ng.total['QtdGenhadores'] += value.QtdGenhadores;
					ng.total['QtdGanhadoresUnicos'] += value.QtdGanhadoresUnicos;
					ng.total['QtdMilhasPadrao'] += value.QtdMilhasPadrao;
					ng.total['QtdMilhasShell'] += value.QtdMilhasShell;
					ng.total['QtdMilhasSmiles'] += value.QtdMilhasSmiles;
					ng.totalreport2 += totalparcial;


					registros += 1
				});


				ng.chartReport2 = {
					x: {
						labels: labels
					}
					, y: {
						label: 'Valores'
					}
					, exporting: {
						enabled : true
						, filename: 'grafico'
					}
					, series: [
						, {
							label: 'Qtd Usuarios Em Moderacao'
							, value: aa
						}
						, {
							label: 'Qtd Ganhadores'
							, value: bb
						}
						, {
							label: 'Qtd Ganhadores Unicos'
							, value: cc
						}
/*						, {
							label: 'Qtd Milhas Padrao'
							, value: dd
						}
						, {
							label: 'Qtd Milhas (Bônus Shell Box)'
							, value: ee
						}
						, {
							label: 'Qtd Milhas (Bônus Smiles)'
							, value: ff
						}*/
					]
				}

			}, function() {
				ng.reports2 = [];
			})
			.finally(function() {
				ng.loaderReports = false;
			})

		}


		ng.getReports();
		ng.getReports2();
		//ng.getReports3();
		//ng.getReports4();

		ng.dateRangePicker = function(){
			var dateRangePickerIsHide = angular.element($('.angular-date-range-picker__picker.ng-scope.angular-date-range-picker--ranged')).hasClass('ng-hide');

			$timeout(function() {
				if (dateRangePickerIsHide) {
					angular.element($('.angular-date-range-picker__input')).triggerHandler('click');
				}
			}, 100);
		}

		ng.initFilters = function() {
			var start = new Date();
			start     = new Date(start.getFullYear()+'/'+(start.getMonth() + 1)+'/01' );
			var end   = new Date();
			ng.totalreport1 = 0;
			ng.totalreport2 = 0;
			ng.total = {};
			ng.total['Aprovado'] = 0;
			ng.total['EmCurso'] = 0;
			ng.total['EmModeracao'] = 0;
			ng.total['Expirado'] = 0;
			ng.total['Reprovado'] = 0;
			ng.total['QtdUsuariosEmEmModeracao'] = 0;
			ng.total['QtdGenhadores'] = 0;
			ng.total['QtdGanhadoresUnicos'] = 0;
			ng.total['QtdMilhasPadrao'] = 0;
			ng.total['QtdMilhasShell'] = 0;
			ng.total['QtdMilhasSmiles'] = 0;

			// ng.range   = '';
			ng.range   = moment().range(start, end);
			ng.filters = {
				datainicial : ''
				, datafinal : ''
				, type : 'dayrly'
			}
		}
		ng.initFilters();

		ng.$watch('range', function() {
			//ng.getReports();
		})

	}
]);
