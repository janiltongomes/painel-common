app.controller('RobotsCtrl', [
	'$scope'
	, '$rootScope'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, '$timeout'
	, function(ng, ngRoot, $routeParams, _, API, $q, $filter, $timeout) {

		ng.getReports = function() {
			var params = {
			  "server": "https://raizen-app-prod.freepy.com.br",
			  "limit": 5
			}

			console.log(params);

			ng.loaderReports = true;
			API.getReportRobots(params)
			.then(function(response) {
				// SUCESSO
				var data = response.data;
				if (!data) {
					return $q.reject(response);
				}

				var labels                       = [];
				var serieRegisteredUsers         = [];
				var serieRealizedOcr             = [];

				angular.forEach(data, function (value, key) {
					console.log('aaa');
					//labels.push($filter('date')(value.date, 'dd/MM/yy'));
					serieStart.push(5);
					serieEnd.push(10);
				});

				labels.pop();
				serieRegisteredUsers.pop();
				serieRealizedOcr.pop();

				ng.chartReport = {
					x: {
						labels: labels
					}
					, y: {
						label: 'Valores'
					}
					, series: [
						{
							label: 'Participantes Cadastrados'
							, value: serieStart
						}
						, {
							label: 'OCR'
							, value: serieEnd
						}
					]
				}

			}, function() {
				ng.reports = [];
			})
			.finally(function() {
				ng.loaderReports = false;
			})

		}
		ng.getReports();

		ng.dateRangePicker = function(){
			var dateRangePickerIsHide = angular.element($('.angular-date-range-picker__picker.ng-scope.angular-date-range-picker--ranged')).hasClass('ng-hide');

			$timeout(function() {
				if (dateRangePickerIsHide) {
					angular.element($('.angular-date-range-picker__input')).triggerHandler('click');
				}
			}, 100);
		}

		ng.initFilters = function() {
			var start = new Date();
			start     = new Date(start.getFullYear()+'/'+(start.getMonth() + 1)+'/01' );
			var end   = new Date();

			// ng.range   = '';
			ng.range   = moment().range(start, end);
			ng.filters = {
				datainicial : ''
				, datafinal : ''
			}
		}
		ng.initFilters();

		ng.$watch('range', function() {
			ng.getReports();
		})

	}
]);
