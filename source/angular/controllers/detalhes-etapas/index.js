app.controller('DetalhesEtapaCtrl', [
	'$scope'
	, '$rootScope'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, '$timeout'
	, function(ng, ngRoot, $routeParams, _, API, $q, $filter, $timeout) {

		ng.getReports3 = function() {
			ng.loaderReports = true;

			API.getstepdetails($routeParams.idPromo)
			.then(function(response) {
				// SUCESSO
				if (!response.data) {
					return $q.reject(response);
				}

				if (response.data.records.length > 0) {
					ng.filters.totalItems = response.data.totalRecordsCount;
					ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
				}

				var graph = [];
				angular.forEach(response.data.records, function(obj, value) {
					if (typeof graph[obj.Etapa] === 'undefined') {
						graph[obj.Etapa] = {
							x: {
								labels: ['Início', '1 participação', '2 participações', 'Moderação']
							}
							, y: {
								label: ''
							}
							, legend: {
								enabled: true
							}
							, colors : ['#c0392b', '#d35400', '#f1c40f', '#2ecc71']
							, series: [
								{
									label: 'Vermelho (30+)'
									, value: []
								}
								, {
									label: 'Laranja (21-30)'
									, value: []
								}
								, {
									label: 'Amarelo (11-20)'
									, value: []
								}
								, {
									label: 'Verde (0-10)'
									, value: []
								}
							]
							, Etapa : obj.Etapa
							, Linha : obj.Linha
						}
					}

				})

				for (var i = 1; i <= graph.length; i++) {
					for (var j = 0; j < response.data.records.length; j++) {
						if (graph[i] && graph[i].Etapa == response.data.records[j].Etapa) {
							graph[i].series[3].value.push(response.data.records[j].QtdVerde == 0 ? null : response.data.records[j].QtdVerde)
							graph[i].series[2].value.push(response.data.records[j].QtdAmarelo == 0 && response.data.records[j].QtdLaranja == 0 && response.data.records[j].QtdVermelho == 0 ? null : response.data.records[j].QtdAmarelo)
							graph[i].series[1].value.push(response.data.records[j].QtdAmarelo == 0 && response.data.records[j].QtdLaranja == 0 && response.data.records[j].QtdVermelho == 0 ? null : response.data.records[j].QtdLaranja)
							graph[i].series[0].value.push(response.data.records[j].QtdAmarelo == 0 && response.data.records[j].QtdLaranja == 0 && response.data.records[j].QtdVermelho == 0 ? null : response.data.records[j].QtdVermelho)
						}
					}
				}

				graph = graph.filter(function() {return true;});
				ng.graphs = graph;


			}, function() {
				ng.reports3 = [];
			})
			.finally(function() {
				ng.loaderReports = false;
			})

		}

		ng.getReports3();

		ng.initFilters = function() {
			var start = new Date();
			start     = new Date(start.getFullYear()+'/'+(start.getMonth() + 1)+'/01' );
			var end   = new Date();

			// ng.range   = '';
			ng.range   = moment().range(start, end);
			ng.filters = {
				datainicial : ''
				, datafinal : ''
				, type : 'dayrly'
			}
		}
		ng.initFilters();


	}
]);
