app.controller('IndicatorsCtrl', [
	'$scope'
	, '$rootScope'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, '$timeout'
	, function(ng, ngRoot, $routeParams, _, API, $q, $filter, $timeout) {

		ng.getReports = function() {
			if (!_.isEmpty(ng.range)) {
				// var params = {
				// 	dateStart: ng.range.start._d.toJSON().slice(0,10)
				// 	, dateEnd: ng.range.end._d.toJSON().slice(0,10)
				// }
				//+ (24*60*60*1000) -1
				var params = {
					dateStart: Math.floor(ng.range.start._d.getTime()/1000)
					, dateEnd: Math.floor(ng.range.end._d.getTime())
				}
				dateEndFormated = moment(params.dateEnd).format('MM/DD/YYYY')
				dateEndFormated = new Date(dateEndFormated)
				dateEndFormated.setDate(dateEndFormated.getDate() + 1);
				dateEndFormated.setSeconds(dateEndFormated.getSeconds() - 1);
				params.dateEnd = Math.floor(dateEndFormated / 1000)
			} else {
				return false;
			}

			params.promotionId = $routeParams.idPromo

			ng.loaderReports = true;
			//API.getReportIndicators($routeParams.idPromo, params)
			API.getReportIndicators(ng.filters.type, params)
			.then(function(response) {
				// SUCESSO
				var data = response.data;
				if (!data) {
					return $q.reject(response);
				}

				var labels                       = [];
				var approvedTransactions         = [];
				var cupomTransactions            = [];
				var cupomQrCodeTransactions      = [];
				var invalidTransactions			 = [];
				var moderated                    = [];
				var payPallTransactions          = [];
				var pendingToModeration          = [];
				var registeredUsers              = [];
				var transactedCNPJTotal          = [];
				var rejectedTransactions         = [];
				var totalTransactions            = [];
				var transactedCNPJ               = [];
				var unmoderateTransactions       = [];

				registros = 0
				approvedTransactions_TOTAL = 0
				cupomTransactions_TOTAL = 0
				qrCodeTransactions_TOTAL = 0
				invalidTransactions_TOTAL = 0
				moderated_TOTAL = 0
				payPallTransactions_TOTAL = 0
				pendingToModeration_TOTAL = 0
				registeredUsers_TOTAL = 0
				rejectedTransactions_TOTAL = 0
				totalTransactions_TOTAL = 0
				transactedCNPJ_TOTAL = 0
				unmoderateTransactions_TOTAL = 0
				transactedCNPJTotal_TOTAL = 0
				typeCount = ''

				angular.forEach(data.consolidated, function (value, key) {
					if (value._id) {
						if (value._id.day < 10)
							value._id.day = '0' +value._id.day;

						if (value._id.month < 10)
							value._id.month = '0' +value._id.month;

						value.timeStart = moment(value._id.year + '-' + value._id.month + '-' + value._id.day).toDate()
						//value.timeStart = new Date(value.timeStart.getTime()+ 3*3600*1000)
						format = 'dd/MM/yy';
						labels.push($filter('date')(value.timeStart, format));
						typeCount = 'DIA'
					}else{
						value.timeStart = new Date(value.timeStart * 1000)
						//value.timeStart = new Date(value.timeStart.getTime()+ 3*3600*1000)
						format = 'dd/MM/yy HH';
						labels.push($filter('date')(value.timeStart, format) + 'H');
						typeCount = 'HORA'

					}


					approvedTransactions.push(value.approvedTransactions);
					cupomTransactions.push(value.cupomTransactions);
					cupomQrCodeTransactions.push(value.qrCodeTransactions);
					invalidTransactions.push(value.invalidTransactions);
					moderated.push(value.moderated);
					payPallTransactions.push(value.payPallTransactions);
					pendingToModeration.push(value.pendingToModeration);
					registeredUsers.push(value.registeredUsers);
					transactedCNPJTotal.push(value.transactedCNPJTotal);
					rejectedTransactions.push(value.rejectedTransactions);
					totalTransactions.push(value.totalTransactions);
					transactedCNPJ.push(value.transactedCNPJ);
					unmoderateTransactions.push(value.unmoderateTransactions);

					approvedTransactions_TOTAL += value.approvedTransactions;
					cupomTransactions_TOTAL += value.cupomTransactions;
					qrCodeTransactions_TOTAL += value.qrCodeTransactions;
					invalidTransactions_TOTAL += value.invalidTransactions;
					moderated_TOTAL += value.moderated;
					payPallTransactions_TOTAL += value.payPallTransactions;
					pendingToModeration_TOTAL += value.pendingToModeration;
					//registeredUsers_TOTAL += value.registeredUsers;
					transactedCNPJTotal_TOTAL += value.transactedCNPJTotal;
					rejectedTransactions_TOTAL += value.rejectedTransactions;
					totalTransactions_TOTAL += value.totalTransactions;
					//transactedCNPJ_TOTAL += value.transactedCNPJ;
					unmoderateTransactions_TOTAL += value.unmoderateTransactions;

					registros += 1
				});

				if (data){
					registeredUsers_TOTAL = data.registeredUsers;
					transactedCNPJTotal_TOTAL = data.transactedCNPJTotal;
				}

				ng.reportTotal = [
					{
						main: {
							icon: 'tnt-edit'
							, listText : [
								{
									text: 'CPFs'
									, class: 'small'
								}, {
									text: $filter('number')(registeredUsers_TOTAL) || 0
									, class: 'bold small'
								}
							]
						}
					}
					, {
						main: {
							icon: 'tnt-edit'
							, listText : [
								{
									text: 'CNPJs'
									, class: 'small'
								}, {
									text: $filter('number')(transactedCNPJTotal_TOTAL) || 0
									, class: 'bold small'
								}
							]
						}
					}
				];

				ng.reportTotal2 = [
					{
						main: {
							icon: 'tnt-coupon'
							, listText : [
								{
									text: 'Cupons'
									, class: 'small'
								}, {
									text: $filter('number')(cupomTransactions_TOTAL) || 0
									, class: 'bold small'
								}
							]
						}
					}
					, {
						main: {
							icon: 'tnt-photo-coupon'
							, listText : [
								{
									text: 'QrCode'
									, class: 'small'
								}, {
									text: $filter('number')(qrCodeTransactions_TOTAL) || 0
									, class: 'bold small'
								}
							]
						}
					}
					, {
						main: {
							icon: 'tnt-photo-coupon'
							, listText : [
								{
									text: 'Abastecimentos'
									, class: 'small'
								}, {
									text: $filter('number')(payPallTransactions_TOTAL) || 0
									, class: 'bold small'
								}
							]
						}
					}
					, {
						main: {
							icon: 'tnt-photo-coupon'
							, listText : [
								{
									text: 'Participações'
									, class: 'small'
								}, {
									text: $filter('number')(cupomTransactions_TOTAL + qrCodeTransactions_TOTAL + payPallTransactions_TOTAL) || 0
									, class: 'bold small'
								}
							]
						}
					}
					, {
						main: {
							icon: 'tnt-coupon'
							, listText : [
								{
									text: 'Cupons / '+typeCount
									, class: 'small'
								}, {
									text: $filter('number')(cupomTransactions_TOTAL / registros) || 0
									, class: 'bold small'
								}
							]
						}
					}
					, {
						main: {
							icon: 'tnt-coupon'
							, listText : [
								{
									text: 'QrCode / '+typeCount
									, class: 'small'
								}, {
									text: $filter('number')(qrCodeTransactions_TOTAL / registros) || 0
									, class: 'bold small'
								}
							]
						}
					}
					, {
						main: {
							icon: 'tnt-coupon'
							, listText : [
								{
									text: 'Abastecimentos / '+typeCount
									, class: 'small'
								}, {
									text: $filter('number')(payPallTransactions_TOTAL / registros) || 0
									, class: 'bold small'
								}
							]
						}
					}
					, {
						main: {
							icon: 'tnt-photo-coupon'
							, listText : [
								{
									text: 'Participações / '+typeCount
									, class: 'small'
								}, {
									text: $filter('number')((cupomTransactions_TOTAL + qrCodeTransactions_TOTAL + payPallTransactions_TOTAL) / registros) || 0
									, class: 'bold small'
								}
							]
						}
					}
					, {
						main: {
							icon: 'tnt-coupon'
							, listText : [
								{
									text: 'Cupons / CPF (média)'
									, class: 'small'
								}, {
									text: (cupomTransactions_TOTAL / registeredUsers_TOTAL).toFixed(2) || 0
									, class: 'bold small'
								}
							]
						}
					}
					, {
						main: {
							icon: 'tnt-coupon'
							, listText : [
								{
									text: 'QrCode / CPF (média)'
									, class: 'small'
								}, {
									text: (qrCodeTransactions_TOTAL / registeredUsers_TOTAL).toFixed(2) || 0
									, class: 'bold small'
								}
							]
						}
					}
					, {
						main: {
							icon: 'tnt-coupon'
							, listText : [
								{
									text: 'Abastecimentos / CPF (média)'
									, class: 'small'
								}, {
									text: (payPallTransactions_TOTAL / registeredUsers_TOTAL).toFixed(2) || 0
									, class: 'bold small'
								}
							]
						}
					}
					, {
						main: {
							icon: 'tnt-photo-coupon'
							, listText : [
								{
									text: 'Participações / CPF (média)'
									, class: 'small'
								}, {
									text: ((cupomTransactions_TOTAL + qrCodeTransactions_TOTAL + payPallTransactions_TOTAL) / registeredUsers_TOTAL).toFixed(2) || 0
									, class: 'bold small'
								}
							]
						}
					}
					// , {
					// 	main: {
					// 		icon: 'tnt-perfis_ativos'
					// 		, listText : [
					// 			{
					// 				text: 'Transações Inválidas'
					// 				, class: 'small'
					// 			}, {
					// 				text: $filter('number')(invalidTransactions_TOTAL) || 0
					// 				, class: 'bold small'
					// 			}
					// 		]
					// 	}
					// }
				];

				// labels.pop();
				// approvedTransactions.pop();
				// cupomTransactions.pop();
				// invalidTransactions.pop();
				// moderated.pop();
				// payPallTransactions.pop();
				// pendingToModeration.pop();
				// registeredUsers.pop();
				// rejectedTransactions.pop();
				// totalTransactions.pop();
				// transactedCNPJ.pop();
				// unmoderateTransactions.pop();

				// if (ng.filters.type == "dayrly"){
				// 	payPallTransactions.reverse()
				// 	cupomTransactions.reverse()
				// 	labels.reverse()
				// }

				ng.chartReport = {
					x: {
						labels: labels
					}
					, y: {
						label: 'Valores'
					}
					, exporting: {
						enabled : true
						, filename: 'grafico-de-indicadores'
					}
					, series: [
						, {
							label: 'Abastecimentos Válidos'
							, value: payPallTransactions
						}
						, {
							label: 'Cupons'
							, value: cupomTransactions
						}
						, {
							label: 'QrCode'
							, value: cupomQrCodeTransactions
						}
					]
				}

			}, function() {
				ng.reports = [];
			})
			.finally(function() {
				ng.loaderReports = false;
			})

		}
		ng.getReports();

		ng.dateRangePicker = function(){
			var dateRangePickerIsHide = angular.element($('.angular-date-range-picker__picker.ng-scope.angular-date-range-picker--ranged')).hasClass('ng-hide');

			$timeout(function() {
				if (dateRangePickerIsHide) {
					angular.element($('.angular-date-range-picker__input')).triggerHandler('click');
				}
			}, 100);
		}

		ng.initFilters = function() {
			var start = new Date();
			start     = new Date(start.getFullYear()+'/'+(start.getMonth() + 1)+'/01' );
			var end   = new Date();

			// ng.range   = '';
			ng.range   = moment().range(start, end);
			ng.filters = {
				datainicial : ''
				, datafinal : ''
				, type : 'dayrly'
			}
		}
		ng.initFilters();

		ng.$watch('range', function() {
			ng.getReports();
		})

	}
]);
