app.controller('TopGanhadoresViewCtrl', [
	'$scope'
	, '$rootScope'
	, '$location'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, function(ng, ngRoot, $location, $routeParams, _, API, $q, $filter) {

	ng.premioResgatado = function(idUser) {
		ng.loaderPremioResgatado = true;
		API.getPromoWinnerRedeemed($routeParams.idPromo, idUser)
		.then(function() {
			// SUCESSO
			notify({
				type: 'success'
				, title: 'Sucesso!'
			})
		}, function() {
			notify({
				type: 'error'
				, title: 'Erro ao executar a operação!'
			})
		})
		.finally(function() {
			ng.loaderPremioResgatado = false;
			ng.getParticipant();
		})
	}

	ng.transactionsList = [];
	ng.pushCouponTransaction = function(item) {
		if (item.elegible) {
			// APROVADO
			ng.disapproved = false;
		} else {
			// DESAPROVADO
			ng.approved = false;
		}
		var id = item.id.toString();
		if (ng.transactionsList.indexOf(id) == -1) {
			ng.transactionsList.push(id);
		} else {
			var index = ng.transactionsList.indexOf(id);
			ng.transactionsList.splice(index, 1);
		}
	}

	// ngRoot.toggleCouponQualify = function(status, idTransaction) {
	// 	var coupon_obj = {
	// 		userId         : $routeParams.idUser
	// 		, promotionId  : $routeParams.idPromo
	// 		, transactions : idTransaction ? [idTransaction] : ng.transactionsList
	// 	}
	// 	ng.transactionsList = [];
	// 	ngRoot.modalControl = false;

	// 	if (status) {
	// 		// APROVAR

	// 		API.postCouponQualify(coupon_obj)
	// 		.then(function(response) {
	// 			// SUCESSO
	// 			notify({
	// 				type: 'success'
	// 				, title: 'Aprovado com sucesso!'
	// 			})
	// 		}, function() {
	// 			notify({
	// 				type: 'error'
	// 				, title: 'Erro'
	// 				, desc: 'Ocorreu um erro ao qualificar o cupom, tente novamente!'
	// 			})
	// 		})
	// 		.then(function() {
	// 			ng.getParticipant();
	// 		})

	// 	} else {
	// 		// DESAPROVAR

	// 		API.postCouponDisqualify(coupon_obj)
	// 		.then(function(response) {
	// 			// SUCESSO
	// 			notify({
	// 				type: 'success'
	// 				, title: 'Desaprovado com sucesso!'
	// 			})
	// 		}, function() {
	// 			notify({
	// 				type: 'error'
	// 				, title: 'Erro'
	// 				, desc: 'Ocorreu um erro ao desqualificar o cupom, tente novamente!'
	// 			})
	// 		})
	// 		.then(function() {
	// 			ng.getParticipant();
	// 		})
	// 	}
	// }

	ngRoot.toggleCouponProduct = function(status, idTransaction) {
		var coupon_obj = {
			userId         : $routeParams.idUser
			, promotionId  : $routeParams.idPromo
			, transactions : idTransaction ? [idTransaction] : ng.transactionsList
		}
		ng.transactionsList = [];
		ngRoot.modalControl = false;

		if (status) {
			// APROVAR

			API.postProductFound(coupon_obj)
			.then(function(response) {
				// SUCESSO
				notify({
					type: 'success'
					, title: 'Cupom atualizado com sucesso!'
				})
			}, function() {
				notify({
					type: 'error'
					, title: 'Erro'
					, desc: 'Ocorreu um erro ao atualizar cupom, tente novamente!'
				})
			})
			.then(function() {
				ng.getParticipant();
			})

		} else {
			// DESAPROVAR

			API.postProductNotFound(coupon_obj)
			.then(function(response) {
				// SUCESSO
				notify({
					type: 'success'
					, title: 'Cupom atualizado com sucesso!'
				})
			}, function() {
				notify({
					type: 'error'
					, title: 'Erro'
					, desc: 'Ocorreu um erro ao atualizar cupom, tente novamente!'
				})
			})
			.then(function() {
				ng.getParticipant();
			})
		}
	}

	// ng.getParticipant = function() {
	// 	ng.loaderReports = true;
	// 	API.getPromoById($routeParams.idPromo)
	// 	.then(function(response) {
	// 		// SUCESSO
	// 		var data = response.data;
	// 		if (!data) {
	// 			return $q.reject(response);
	// 		}

	// 		ng.promo = data;

	// 		if ($routeParams.typePromo == 'BUY_AND_WIN') {
	// 			return API.getPromoBuyAndWinWinnerDetails($routeParams.idPromo, $routeParams.idUser)
	// 		} else {
	// 			return API.getPromoFoodRewardWinnerDetails($routeParams.idPromo, $routeParams.idUser)
	// 		}
	// 	})

	// 	.then(function(response) {
	// 		// SUCESSO
	// 		if (!response.data) {
	// 			return $q.reject(response);
	// 		}

	// 		ngRoot.participant = response.data;
	// 		ngRoot.redeemDate  = ngRoot.participant.redeemDate;

	// 		ng.transactions = [];

	// 		angular.forEach(ngRoot.participant.transactions, function (value, key) {

	// 			ng.transactions.push({
	// 				id                 : value.transactionId
	// 				, image            : value.image
	// 				, elegible         : value.elegible
	// 				, approved         : value.approved
	// 				, hasFoundProducts : value.hasFoundProducts
	// 				, fnClick          : '$root.modalShow(obj)'
	// 				, cupom_info       : [
	// 					{
	// 						label: 'Tipo'
	// 						, label_class: 'col-8'
	// 						, value: value.paymentMethod
	// 						, value_class: 'col-16'
	// 					}
	// 					, {
	// 						label: 'Data Envio'
	// 						, label_class: 'col-8'
	// 						, value: $filter('data_timestamp')(value.promotionProcessTimeStamp)
	// 						, value_class: 'col-16'
	// 					}
	// 					, {
	// 						label: 'Data Cupom'
	// 						, label_class: 'col-8'
	// 						, value: $filter('data_timestamp')(value.date)
	// 						, value_class: 'col-16'
	// 					}
	// 					, {
	// 						label: 'CNPJ'
	// 						, label_class: 'col-8'
	// 						, value: $filter('cnpj')(value.cnpj)
	// 						, value_class: 'col-16'
	// 					}
	// 					, {
	// 						label: 'COO'
	// 						, label_class: 'col-8'
	// 						, value: value.coo
	// 						, value_class: 'col-16'
	// 					}

	// 					, {
	// 						label: 'Total'
	// 						, label_class: 'col-8'
	// 						, value: $filter('currency')(value.total)
	// 						, value_class: 'col-16'
	// 					}

	// 					, {
	// 						label: 'Status'
	// 						, label_class: 'col-8'
	// 						, value: (value.statusCupom == 'DISAPPROVED' ? 'Reprovado' : (value.statusCupom == 'APPROVED' ? 'Aprovado' : ''))
	// 						, value_class: 'col-16'
	// 					}
	// 					, {
	// 						label: 'Motivo'
	// 						, label_class: 'col-8'
	// 						, value: (value.reason ? value.reason : '')
	// 						, value_class: 'col-16'
	// 					}
	// 				]
	// 			});

	// 		});

	// 	})
	// 	.finally(function() {
	// 		ng.loaderReports = false;
	// 	})
	// }
	//ng.getParticipant();

		ng.getCouponsList = function() {
			if (!_.isEmpty($routeParams.idCard)) {
				API.getPromoById($routeParams.idPromo)
				.then(function(response) {
					// SUCESSO
					var data = response.data;
					if (!data) {
						return $q.reject(response);
					}

					ng.promo = data;

				})

				ng.reports = [];
				ng.loaderReports = true;

				var object = {
					promotionId   : $routeParams.idPromo
					, cardId      : $routeParams.idCard
					, userId      : $routeParams.idUser
				}


				API.getCouponsListbyCard(object)
				.then(function(response) {
					// SUCESSO
					if (_.isEmpty(response.data)) {
						return $q.reject(response);
					}

					ng.reports = [];

					ngRoot.participant = response.data;
					ngRoot.redeemDate  = ngRoot.participant.redeemDate;
					ngRoot.earnedPoints  = ngRoot.participant.earnedPoints;

					ng.transactions = [];

					angular.forEach(ngRoot.participant.participations, function (value, key) {

						ngRoot.participant.cpf = value.cpf;
						ngRoot.participant.cumulativeTotal
						ng.transactions.push({
							id                 : value.transactionId
							, cpf              : value.cpf
							, image            : value.imageUrl
							, approved         : value.statusCupom
							, fnClick          : '$root.modalShow(obj)'
							, cupom_info       : [
								{
									label: 'Tipo'
									, label_class: 'col-8'
									, value: value.paymentMethod
									, value_class: 'col-16'
								}
								, {
									label: 'Data Envio'
									, label_class: 'col-8'
									, value: $filter('data_timestamp')(value.promotionProcessTimeStamp)
									, value_class: 'col-16'
								}
								, {
									label: 'Data Cupom'
									, label_class: 'col-8'
									, value: $filter('data_timestamp')(value.cupomDate)
									, value_class: 'col-16'
								}
								, {
									label: 'CNPJ'
									, label_class: 'col-8'
									, value: $filter('cnpj')(value.cnpj)
									, value_class: 'col-16'
								}
								, {
									label: 'COO'
									, label_class: 'col-8'
									, value: value.coo
									, value_class: 'col-16'
								}

								, {
									label: 'Total'
									, label_class: 'col-8'
									, value: $filter('currency')(value.total)
									, value_class: 'col-16'
								}

								, {
									label: 'Status'
									, label_class: 'col-8'
									, value: (value.statusCupom == 'DISAPPROVED' ? 'Reprovado' : (value.statusCupom == 'APPROVED' ? 'Aprovado' : ''))
									, value_class: 'col-16'
								}
								, {
									label: 'Motivo'
									, label_class: 'col-8'
									, value: (value.reason ? value.reason : '')
									, value_class: 'col-16'
								}
							]
						});

					});

					// ng.filters.amtPages   = Math.ceil(response.data.count/ng.filters.object.top);
					// ng.filters.totalItems = response.data.count;

					// if (response.data.count == 0) {
					// 	ng.filters.amtPages = 0;
					// }

				}, function(response) {
					ng.reports = [];
				})
				.finally(function(response) {
					ng.loaderReports = false;



				})
			}
		}
		ng.getCouponsList();

}]);
