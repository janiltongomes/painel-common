app.controller('MainController', [
	'$scope'
	, '$rootScope'
	, '_'
	, 'API'
	, '$q'
	, '$timeout'
	, function(ng, ngRoot, _, API, $q, $timeout) {

	ngRoot.canceller = $q.defer();

	ngRoot.closeModal = function() {
		ngRoot.modalControl = false;
		ngRoot.modalOverlay = false;
		ngRoot.modalInfo = {};
	}

	ngRoot.modalControl = false;
	ngRoot.modalShow = function(item) {
		ngRoot.rotateImg(0);

		ngRoot.modalControl = true;
		ngRoot.modalOverlay = true;
		ngRoot.modalInfo    = item;
	}

	ngRoot.default = {
		top: 10
	}

	ngRoot.getDataBase64ByUrlImage = function(url, callback) {
		var xhr = new XMLHttpRequest();
		xhr.responseType = 'blob';
		xhr.onload = function() {
			var reader = new FileReader();
			reader.onloadend = function() {
				callback(reader.result);
			}
			reader.readAsDataURL(xhr.response);
		};
		xhr.open('GET', url);
		xhr.send();
	}

	var rotate_angle = 0;
	ngRoot.rotateImg = function(angle) {
		if (angle != 0) {
			rotate_angle += angle;
		} else {
			rotate_angle = 0;
		}

		rotate_angle = (Math.abs(rotate_angle) == 360) ? 0 : rotate_angle

		$('.coupon-img img')
			.css({
				'-webkit-transform': 'rotate(' + rotate_angle + 'deg)'
				, '-moz-transform': 'rotate(' + rotate_angle + 'deg)'
			});

		if ($('.coupon-img img') && $('.coupon-img img')[0].offsetWidth < $('.coupon-img img')[0].offsetHeight) {

			if (Math.abs(rotate_angle%180) == 90) {
				$('.coupon-img img')
					.css({
						'max-height' : $('.coupon-img')[0].offsetWidth + 'px'
					})
			} else {
				$('.coupon-img img')
					.css({
						'max-height'         : 'initial'
					})
			}
		} else {

			if (rotate_angle == 90 || rotate_angle == -270) {
				$('.coupon-img img')
					.css({
						'max-height'         : $('.coupon-img')[0].offsetWidth + 'px'
						, 'transform-origin' : 'bottom center 0px'
						, 'position'         : 'absolute'
						, 'right'            : '50%'
						, 'left'             : 'initial'
						, 'margin-right'     : -($('.coupon-img img')[0].offsetWidth/4) + 'px'
					})
			} else if (rotate_angle == 270 || rotate_angle == -90) {
				$('.coupon-img img')
					.css({
						'max-height'         : $('.coupon-img')[0].offsetWidth + 'px'
						, 'transform-origin' : 'bottom center 0px'
						, 'position'         : 'absolute'
						, 'right'            : 'initial'
						, 'left'             : '50%'
						, 'margin-left'      : -($('.coupon-img img')[0].offsetWidth/4) + 'px'
					})
			} else {
				$('.coupon-img img')
					.css({
						'max-height'         : 'initial'
						, 'transform-origin' : 'center'
						, 'position'         : 'relative'
						, 'left'             : 'initial'
						, 'right'            : 'initial'
						, 'margin'           : '0px'
					})
			}
		}

	}

	/* (RE)ANALISE DE CUPOM
	---------------------------------------------------------------------------- */
	ngRoot.validateDate = function(form, input, inputyear) {

		if (_.isEmpty(input)) return;

		input = input + inputyear;

		var pattern   = new RegExp('[^0-9]', 'g');
		var dataInput = input.replace(pattern, '');
		var input     = dataInput.substring(0,2)+'/'+dataInput.substring(2,4)+'/'+dataInput.substring(4,8);

		// VALIDA SE FOR UMA DATA
		if (input.match(/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g)) {
			form.date.$setValidity('data', true); // CAMPO VALIDO
		} else {
			return form.date.$setValidity('data', false); // CAMPO INVALIDO
		}

		// VALIDA SE FOR UMA DATA MAIOR QUE HOJE
		input = _.replaceAll(input, "/", "");

		// DATA NO FORMATO MM-DD-YYYY
		var dateInput = new Date(input.substring(2,4) + '/' + input.substring(0,2) + '/' + input.substring(4,8));
		var dateNow   = new Date();

		if (dateInput <= dateNow) {
			form.date.$setValidity('dategreaterthantoday', true); // CAMPO VALIDO
		} else {
			return form.date.$setValidity('dategreaterthantoday', false); // CAMPO INVALIDO
		}
	}

}]);
