app.controller('IndicadoresRecusaCtrl', [
	'$scope'
	, '$rootScope'
	, 'API'
	, '_'
	, '$filter'
	, '$routeParams'
	, '$timeout'
	, '$q'
	, function(ng, ngRoot, API, _, $filter, $routeParams, $timeout, $q) {

	ng.$routeParams = $routeParams;

	// PAGINACAO
	ng.goToPage = function(event) {
		if (event.charCode == 13) {
			if (ng.filters.currentPage == 0) {
				ng.reports = [];
			} else {
				ng.calcSkip();
				ng.getParticipants();
			}
		}
	}
	ng.calcSkip = function() {
		if (ng.filters.currentPage == 1) {
			ng.filters.skip = 0;
		} else {
			ng.filters.skip = (ng.filters.currentPage-1)*ng.filters.top;
		}
	}
	ng.nextPage = function() {
		if (ng.filters.currentPage < ng.filters.amtPages) {
			ng.filters.currentPage++;
			ng.calcSkip();
			ng.getParticipants();
		}
	}
	ng.previousPage = function() {
		if (ng.filters.currentPage > 1) {
			ng.filters.currentPage--;
			ng.calcSkip();
			ng.getParticipants();
		}
	}

	ng.getParticipants = function() {

		ng.loaderReports = true;

		// FIXME: Remover estes campos
		if (!_.isEmpty(ng.range)) {
			var params = {
				date1: ng.range.start._d.toJSON().slice(0,10)
				, date2: ng.range.end._d.toJSON().slice(0,10)
			}
		}

		var callAPI = API.getIndicatorsError($routeParams.idPromo, ng.filters.skip, 50000)

		ng.reports = [];
		ng.registros = [];

		callAPI
		.then(function(response) {
			// SUCESSO
			if (!response.data) {
				return $q.reject(response);
			}
			//console.log(response.data.records);
			if (response.data.records.length > 0) {
				ng.filters.totalItems = response.data.totalRecordsCount;
				ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
				ng.reports = response.data.records;
			}


				var registros = 0
				var dia = {};
				var linhas = [];

				ng.registros = {};
				ng.semanas = {};

				//console.log('RECORDS', response.data.records);

				// for (var i = response.data.records.length - 1; i >= 0; i--) {
				// 	value = response.data.records[i];
				// 	value.Semana = parseInt(value.Semana);
				// 	//typeCount = 'Etapa'
				// 	if (!dia[value.Semana]){
				// 		dia[value.Semana] = [];
				// 	}
				// 	if (!dia[value.Semana][value.Nome]){
				// 		dia[value.Semana][value.Nome] = [];
				// 	}

				// 	dia[value.Semana][value.Nome] = value.Qtd;



				// 	//typeCount = 'Etapa'
				// 	value.Nome = String(value.Nome);
				// 	value.Semana = String(value.Semana);

				// 	// value.Nome = 1;
				// 	// value.Semana = 2;

				// 	// value.Nome = 1;
				// 	// value.Semana = 2;
				// 	if (!ng.registros[value.Nome]){
				// 		ng.registros[value.Nome] = {};
				// 	}

				// 	if (!ng.registros[value.Nome][value.Semana]){
				// 		ng.registros[value.Nome][value.Semana] = {};
				// 	}

				// 	//console.log(value);

				// 	ng.registros[value.Nome][value.Semana] = value.Qtd;
				// }

				angular.forEach(response.data.records, function (value, key) {

					//console.log(value);
					//labels.push('Etapa '+ value.Etapa);
					value.Semana = parseInt(value.Semana);
					//typeCount = 'Etapa'
					if (!ng.semanas[value.Semana]){
						ng.semanas[value.Semana] = {};
					}
					if (!ng.semanas[value.Semana][value.Nome]){
						ng.semanas[value.Semana][value.Nome] = {};
					}



					// if (value.Nome.indexOf("Média") >= 0){
					// 	qtd = String(value.Qtd);
					// 	value.Qtd = qtd.replace('.', ',');
					// }else{
					// 	value.Qtd = $filter('number')(value.Qtd)
					// }

					ng.semanas[value.Semana][value.Nome] = value.Qtd;



					//typeCount = 'Etapa'
					value.Nome = String(value.Erro);
					value.Semana = String(value.Semana);

					// value.Nome = 1;
					// value.Semana = 2;

					// value.Nome = 1;
					// value.Semana = 2;
					if (!ng.registros[value.Nome]){
						ng.registros[value.Nome] = {};
					}

					if (!ng.registros[value.Nome][value.Semana]){
						ng.registros[value.Nome][value.Semana] = {};
					}

					//console.log(value);

					qtd = String(value.Percentual);
					value.Percentual = qtd.replace('.', ',');
					if (value.Percentual == 0){
						value.Percentual = "0,00";
					}

					ng.registros[value.Nome][value.Semana] = value.Percentual;

					//console.log(value.Promocao);
					//registros += 1
				});


				//ng.registros = ["aa"]
				//console.log(dia);

				//ng.semanas =
				//ng.registros = 'bbbb;


				string = '';
				string += ';RECUSA PROMOÇÃO';
				angular.forEach(ng.semanas, function (value, key) {

					string += '; Semana ' + key;
				});

				string += "\n";

				angular.forEach(ng.registros, function (value, key) {

					string += ';' + key;

					angular.forEach(ng.registros[key], function (value2, key2) {

						if (_.isEmpty(value2) || value2 == "NaN" || value2 == 0){
							value2 = '0,00';
						}

						string += '; '+ value2;
					});

					string += "\n";
				});

				ng.string = string;

		}, function() {
			ng.reports = [];
		})
		.finally(function() {
			console.log(ng.registros);
			ng.loaderReports = false;
		})






				// var registros = 0
				// var dia = [];

				// angular.forEach(data.records, function (value, key) {

				// 	//console.log(value);
				// 	//labels.push('Etapa '+ value.Etapa);
				// 	value.Indice = parseInt(value.Indice);
				// 	typeCount = 'Etapa'
				// 	if (!dia[value.Indice]){
				// 		dia[value.Indice] = [];
				// 	}

				// 	dia[value.Indice][value.Promocao] = value.Quantidade;
				// 	//console.log(value.Promocao);
				// 	registros += 1
				// });

				// console.log(dia);

				// dia.forEach(function (value2, key2) {
				// 	/*if (key2 == 67){
				// 		console.log(value2);
				// 		console.log(value2['Milhas Extras']);
				// 	}*/
				// 	//console.log(value2);
				// 	labels.push('Dia ' + key2);
				// 	aa.push(value2['Ganhe Sempre'] ? value2['Ganhe Sempre'] : null)
				// 	bb.push(value2['Ferrari'] ? value2['Ferrari'] : null)
				// 	cc.push(value2['Milhas'] ? value2['Milhas'] : null)
				// 	dd.push(value2['Milhas Extras'] ? value2['Milhas Extras'] : null)

				// })






	}

	ng.rangeSelectOptions = [
		{
			label : 'Hoje',
			range : moment().range(
				moment().range(moment().startOf('day').startOf('day'),
				moment().endOf('day').startOf('day'))
			)
		}
		, {
			label : 'Esta semana',
			range : moment().range(
				moment().startOf('week').startOf('dom'),
				moment().endOf('week').startOf('sex')
			)
		}
		, {
			label : 'Semana anterior',
			range : moment().range(
				moment().startOf('week').add(-1, 'week').startOf('day'),
				moment().add(-1, 'week').endOf('week').startOf('sex')
			)
		}
		, {
			label : 'Esta quinzena',
			range : moment().range(
				moment().startOf('week').add(-1, 'week').startOf('day'),
				moment().endOf('week').startOf('sex')
			)
		}
		, {
			label : 'Este mês',
			range : moment().range(
				moment().startOf('month').startOf('day'),
				moment().endOf('week').startOf('day')
			)
		}
		, {
			label : 'Mês anterior',
			range : moment().range(
				moment().startOf('month').add(-1, 'month').startOf('day'),
				moment().add(-1, 'month').endOf('month').startOf('day')
			)
		},
	]

	ng.dateRangePicker = function(){
		var dateRangePickerIsHide = angular.element($('.angular-date-range-picker__picker.ng-scope.angular-date-range-picker--ranged')).hasClass('ng-hide');

		$timeout(function() {
			if (dateRangePickerIsHide) {
				angular.element($('.angular-date-range-picker__input')).triggerHandler('click');
			}
		}, 100);
	}

	ng.openExcel = function() {
		ng.loaderExcel = true;

		_.downloadArchive(ng.string, 'excel', 'relatorio.csv');

		ng.loaderExcel = false;
	}

	ng.resetFilters = function() {
		// PAGINATION
		ng.filters.amtPages    = 0;
		ng.filters.currentPage = 1;
		ng.filters.totalItems  = 0;
		ng.filters.skip        = 0;
		ng.filters.top         = 10;
	}

	ng.initFilters = function() {
		var start = new Date();
		start.setDate(start.getDate() - 7);
		var end   = new Date();

		// ng.range   = moment().range(start, end);
		ng.registros = [];
		ng.range   = '';
		ng.filters = {
			type          : 'products'
			, searchField : 'cpf'
			, searchTerm  : ''
			// FIXME
			, startDate   : ''
			, endDate     : ''
		}

		ng.resetFilters();
	}
	ng.initFilters();

	ng.reports = [];
	ng.getParticipants();

}]);
