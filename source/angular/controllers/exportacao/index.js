app.controller('ExportCtrl', ['$scope', '$rootScope', '$routeParams', '_', 'API', function(ng, ngRoot, $routeParams, _, API) {

	ng.exportReportsRegister = function() {
		ng.loaderExcel = true;
		API.getReportsRegisterExcel($routeParams.idPromo)
		.then(function(response) {
			_.downloadArchive(response.data, 'excel', 'exportacao-de-cadastro.csv');
		})
		.finally(function() {
			ng.loaderExcel = false;
		});
	}

	ng.exportReportsRegisterDetails = function() {
		ng.loaderExcel = true;
		API.getReportsRegisterDetailsExcel($routeParams.idPromo)
		.then(function(response) {
			_.downloadArchive(response.data, 'excel', 'exportacao-de-cadastro-detalhado.csv');
		})
		.finally(function() {
			ng.loaderExcel = false;
		});
	}

	ng.infoBlocks = [
		{
			main : {
				icon: 'tnt-users'
				, listText : [{
					text: 'Exportação de Cadastro'
					, class: 'small'
				}]
			}
			, feature : {
				text: 'Exportar'
				, class: 'tnt-excel small'
			}
			, fnClick: 'exportReportsRegister()'
		}
		, {
			main : {
				icon: 'tnt-users'
				, listText : [{
					text: 'Exportação de Cadastro Detalhado'
					, class: 'small'
				}]
			}
			, feature : {
				text: 'Exportar'
				, class: 'tnt-excel small'
			}
			, fnClick: 'exportReportsRegisterDetails()'
		}
	];

}]);
