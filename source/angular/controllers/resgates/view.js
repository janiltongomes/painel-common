app.controller('ResgatesDetailsCtrl', [
	'$scope'
	, '$rootScope'
	, '$location'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, function(ng, ngRoot, $location, $routeParams, _, API, $q, $filter) {

	ng.premioResgatado = function(idUser) {
		ng.loaderPremioResgatado = true;
		API.getPromoWinnerRedeemed($routeParams.idPromo, idUser)
		.then(function() {
			// SUCESSO
			notify({
				type: 'success'
				, title: 'Sucesso!'
			})
		}, function() {
			notify({
				type: 'error'
				, title: 'Erro ao executar a operação!'
			})
		})
		.finally(function() {
			ng.loaderPremioResgatado = false;
			ng.getResgates();
		})
	}

	ng.transactionsList = [];
	ng.pushCouponTransaction = function(item) {
		if (item.elegible) {
			// APROVADO
			ng.disapproved = false;
		} else {
			// DESAPROVADO
			ng.approved = false;
		}
		var id = item.id.toString();
		if (ng.transactionsList.indexOf(id) == -1) {
			ng.transactionsList.push(id);
		} else {
			var index = ng.transactionsList.indexOf(id);
			ng.transactionsList.splice(index, 1);
		}
	}


	ngRoot.toggleCouponProduct = function(status, idTransaction) {
		var coupon_obj = {
			userId         : $routeParams.idUser
			, promotionId  : $routeParams.idPromo
			, transactions : idTransaction ? [idTransaction] : ng.transactionsList
		}
		ng.transactionsList = [];
		ngRoot.modalControl = false;

		if (status) {
			// APROVAR

			API.postProductFound(coupon_obj)
			.then(function(response) {
				// SUCESSO
				notify({
					type: 'success'
					, title: 'Cupom atualizado com sucesso!'
				})
			}, function() {
				notify({
					type: 'error'
					, title: 'Erro'
					, desc: 'Ocorreu um erro ao atualizar cupom, tente novamente!'
				})
			})
			.then(function() {
				ng.getResgates();
			})

		} else {
			// DESAPROVAR

			API.postProductNotFound(coupon_obj)
			.then(function(response) {
				// SUCESSO
				notify({
					type: 'success'
					, title: 'Cupom atualizado com sucesso!'
				})
			}, function() {
				notify({
					type: 'error'
					, title: 'Erro'
					, desc: 'Ocorreu um erro ao atualizar cupom, tente novamente!'
				})
			})
			.then(function() {
				ng.getResgates();
			})
		}
	}

	ng.getResgates = function() {
		ng.loaderReports = true;
		API.getPromoById($routeParams.idPromo)
		.then(function(response) {
			// SUCESSO
			var data = response.data;
			if (!data) {
				return $q.reject(response);
			}

			ng.promo = data;

/*			if ($routeParams.typePromo == 'BUY_AND_WIN') {
				return API.getPromoBuyAndWinWinnerDetails($routeParams.idPromo, $routeParams.idUser)
			} else {
				return API.getPromoFoodRewardWinnerDetails($routeParams.idPromo, $routeParams.idUser)
			}*/

			return API.getReportResgates($routeParams.idPromo, $routeParams.typePromo, $routeParams.idUser)
		})

		.then(function(response) {
			// SUCESSO
			if (!response.data) {
				return $q.reject(response);
			}

			ngRoot.resgates = response.data;
			ngRoot.redeemDate  = ngRoot.resgates.redeemDate;

			ng.transactions = [];

			angular.forEach(ngRoot.resgates.transactions, function (value, key) {

				ng.transactions.push({
					id                 : value.transactionId
					, image            : value.image
					, elegible         : value.elegible
					, hasFoundProducts : value.hasFoundProducts
					, fnClick          : '$root.modalShow(obj)'
					, cupom_info       : [
						{
							label: 'CNPJ'
							, label_class: 'col-8'
							, value: $filter('cnpj')(value.cnpj)
							, value_class: 'col-16'
						}
						, {
							label: 'COO'
							, label_class: 'col-8'
							, value: value.coo
							, value_class: 'col-16'
						}
						, {
							label: 'Data'
							, label_class: 'col-8'
							, value: $filter('data_timestamp')(value.date)
							, value_class: 'col-16'
						}
						, {
							label: 'Total'
							, label_class: 'col-8'
							, value: $filter('currency')(value.total)
							, value_class: 'col-16'
						}
						, {
							label: 'Produto Encontrado'
							, label_class: 'col-8'
							, value: (value.hasFoundProducts ? 'Sim' : 'Não')
							, value_class: 'col-16'
						}
					]
				});

			});

		})
		.finally(function() {
			ng.loaderReports = false;
		})
	}
	ng.getResgates();

}]);
