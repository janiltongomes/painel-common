app.controller('ResgatesCtrl', [
	'$scope'
	, '$rootScope'
	, 'API'
	, '_'
	, '$filter'
	, '$routeParams'
	, '$timeout'
	, '$q'
	, function(ng, ngRoot, API, _, $filter, $routeParams, $timeout, $q) {

	ng.$routeParams = $routeParams;

	ng.openExcel = function() {
		ng.loaderExcel = true;
		_.downloadArchive(ng.reports, 'excel', 'relatorio.csv');
		ng.loaderExcel = false;
	}

	ng.getCallFilter = function() {

		if (!_.isEmpty(ng.range)) {
			var params = {
				date1: ng.range.start._d.toJSON().slice(0,10)
				, date2: ng.range.end._d.toJSON().slice(0,10)
			}

		}

		str_url = '?';

		if (!_.isEmpty(ng.filters.searchTerm)) {
			if (ng.filters.searchField == 'cpf') {
				str_url+= 'cpf=' + ng.filters.searchTerm;
			} else if (ng.filters.searchField == 'name') {
				str_url+= 'name=' + ng.filters.searchTerm;
			} else {
				str_url+= 'email=' + ng.filters.searchTerm;
			}

		}
		if (!_.isEmpty(ng.range)) {
			params.date1 = new Date(params.date1).getTime();
			params.date2 = new Date(params.date2).getTime();
			data2 = new Date(params.date2)
			data2.setDate(data2.getDate()+1)
			params.date2 = new Date(data2).getTime();

			str_url+= '&startDate=' + params.date1 + '&endDate=' + params.date2;
		}

		if (ng.filters.type == 'all') {
			str_url+= '&noredeem=false';
		} else {
			str_url+= '&noredeem=true';
		}

		if (str_url == '?'){
			str_url = '';
		}

		return str_url;
	}

	ng.openExcel = function() {
		ng.loaderExcel = true;
		str_url = ng.getCallFilter();
		API.getReportResgates($routeParams.idPromo, $routeParams.typePromo, str_url, true, ng.filters.skip, ng.filters.top)
		.then(function(response) {
			_.downloadArchive(response.data, 'excel', 'relatorio.csv');
		})
		.finally(function() {
			ng.loaderExcel = false;
		});
	}

	ng.getResgates = function() {

		ng.loaderReports = true;

		ng.reports = [];

		str_url = ng.getCallFilter();


		API.getReportResgates($routeParams.idPromo, $routeParams.typePromo, str_url, null, ng.filters.skip, ng.filters.top)
		.then(function(response) {
			// SUCESSO
			if (!response.data) {
				return $q.reject(response);
			}

			if (response.data.records.length > 0) {
				ng.filters.totalItems = response.data.totalRecordsCount;
				ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
				ng.reports = response.data.records;
				ngRoot.promotionName  = response.data.records[0].promotionName;
			}

		}, function() {
			ng.reports = [];
		})
		.finally(function() {
			ng.loaderReports = false;
		})

	}

	ng.rangeSelectOptions = [
		{
			label : 'Hoje',
			range : moment().range(
				moment().range(moment().startOf('day').startOf('day'),
				moment().endOf('day').startOf('day'))
			)
		}
		, {
			label : 'Esta semana',
			range : moment().range(
				moment().startOf('week').startOf('dom'),
				moment().endOf('week').startOf('sex')
			)
		}
		, {
			label : 'Semana anterior',
			range : moment().range(
				moment().startOf('week').add(-1, 'week').startOf('day'),
				moment().add(-1, 'week').endOf('week').startOf('sex')
			)
		}
		, {
			label : 'Esta quinzena',
			range : moment().range(
				moment().startOf('week').add(-1, 'week').startOf('day'),
				moment().endOf('week').startOf('sex')
			)
		}
		, {
			label : 'Este mês',
			range : moment().range(
				moment().startOf('month').startOf('day'),
				moment().endOf('week').startOf('day')
			)
		}
		, {
			label : 'Mês anterior',
			range : moment().range(
				moment().startOf('month').add(-1, 'month').startOf('day'),
				moment().add(-1, 'month').endOf('month').startOf('day')
			)
		},
	]

	ng.dateRangePicker = function(){
		var dateRangePickerIsHide = angular.element($('.angular-date-range-picker__picker.ng-scope.angular-date-range-picker--ranged')).hasClass('ng-hide');

		$timeout(function() {
			if (dateRangePickerIsHide) {
				angular.element($('.angular-date-range-picker__input')).triggerHandler('click');
			}
		}, 100);
	}

	// PAGINACAO
	ng.goToPage = function(event) {
		if (event.charCode == 13) {
			if (ng.filters.currentPage == 0) {
				ng.users = [];
			} else {
				ng.calcSkip();
				ng.getResgates();
			}
		}
	}
	ng.calcSkip = function() {
		if (ng.filters.currentPage == 1) {
			ng.filters.skip = 0;
		} else {
			ng.filters.skip = (ng.filters.currentPage-1)*ng.filters.top;
		}
	}
	ng.nextPage = function() {
		if (ng.filters.currentPage < ng.filters.amtPages) {
			ng.filters.currentPage++;
			ng.calcSkip();
			ng.getResgates();
		}
	}
	ng.previousPage = function() {
		if (ng.filters.currentPage > 1) {
			ng.filters.currentPage--;
			ng.calcSkip();
			ng.getResgates();
		}
	}

	ng.resetFilters = function() {
		// PAGINATION
		ng.filters.amtPages    = 0;
		ng.filters.currentPage = 1;
		ng.filters.totalItems  = 0;
		ng.filters.skip        = 0;
		ng.filters.top         = 10;
	}

	ng.initFilters = function() {
		var start = new Date();
		start.setDate(start.getDate() - 7);
		var end   = new Date();

		// ng.range   = moment().range(start, end);
		ngRoot.promotionName = '';
		ng.range   = '';
		ng.filters = {
			type          : 'all'
			, searchField : 'cpf'
			, searchTerm  : ''
		}

		ng.resetFilters();
	}

	ng.initFilters();

	ng.reports = [];
	ng.getResgates();

}]);
