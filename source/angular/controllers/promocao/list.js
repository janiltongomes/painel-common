app.controller('PromoListCtrl', [
	'$scope'
	, '$rootScope'
	, '_'
	, '$q'
	, 'API'
	, '$filter'
	, function(ng, ngRoot, _, $q, API, $filter) {

	// PROMOCOES
	ng.loaderPromos = true;
	API.getPromos()
	.then(function(response) {
		// SUCESSO
		var data = response.data;

		if (!data) {
			return $q.reject(response);
		}

		ng.listPromos = [];
		angular.forEach(data, function (value, key) {

			var path = '/' + location.pathname.split('/')[1];

			if (data[key].type) {
				path += '/' + data[key].type;
			}

			path += '/' + data[key].id + '/';

			ng.listPromos.push({
				id : data[key].id
				, href : path
				// , header : {
				// 	checkChange : 'comparePromo(\'' + data[key].id + '\')'
				// }
				, main : {
					text    : data[key].title
					, image : data[key].coverImage
				}
				// , feature : {
				// 	text   : 'Takes: ' + $filter('currency')(data[key].takes, '', 0)
				// }
			});
		});

	}, function(response) {
		// ERRO
		return $q.reject(response);
	})
	.finally(function() {
		ng.loaderPromos = false;
	});

}]);
