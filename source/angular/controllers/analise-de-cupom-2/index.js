app.controller('CouponAnalysis2Ctrl', [
	'$scope'
	, '$rootScope'
	, 'API'
	, '$q'
	, '_'
	, '$routeParams'
	, '$filter'
	, '$timeout'
	, function(ng, ngRoot, API, $q, _, $routeParams, $filter, $timeout) {

		ng.$routeParams = $routeParams;

		if (_.isEmpty($routeParams.typeFilter)) {
			_.URL.path('/analise-de-cupom-2/pending/');
		}

		ng.loaderReports = true;
		ng.loaderCoupon = false;
		ng.disableCupom = false;

		if (_.isEmpty(ngRoot.rejectReasons)) {
			API.getCouponRejectReason()
			.then(function(response) {
				// SUCESSO
				ngRoot.rejectReasons = response.data;

			}, function(response) {
				// ERRO
				notify({
					type: 'error'
					, title: 'Não foi possível recuperar os motivos de reprovação'
				});
			})
			.finally(function(response) {
				// FINAL
			})
		}

		ng.filters = {
			amtPages      : 0
			, currentPage : 1
			, totalItems  : 0
			, object      : {
				skip      : 0
				, top     : 8
			}
		}

		ng.goToPage = function(event) {
			if (event.charCode == 13) {
				if (ng.filters.currentPage == 0) {
					ng.reports = [];
				} else {
					ng.calcSkip();
					ng.getCouponsList();
				}
			}
		}



		ng.calcSkip = function() {
			if (ng.filters.currentPage == 1) {
				ng.filters.object.skip = 0;
			} else {
				ng.filters.object.skip = (ng.filters.currentPage-1)*ng.filters.object.top;
			}
		}


		ng.nextPage = function() {
			if (ng.filters.currentPage < ng.filters.amtPages) {
				ng.filters.currentPage++;
				ng.calcSkip();
				ng.getCouponsList();
			}
		}

		ng.previousPage = function() {
			if (ng.filters.currentPage > 1) {
				ng.filters.currentPage--;
				ng.calcSkip();
				ng.getCouponsList();
			}
		}

		ng.getCouponsTotals = function() {
			API.getCouponsTotals()
			.then(function(response) {
				// SUCESSO
				if (_.isEmpty(response.data)) {
					return $q.reject(response);
				}

				ng.tabMenuFilter = {
					class : 'cl-p2-6 bd-p2-3 bb-0'
					, class_active : 'bd-p1-5 cl-p1-5 bb-0'
					, items : [
						{
							typeFilter: 'pending'
							, href: response.data.pending ? '/analise-de-cupom-2/pending/' : ''
							, listText : [
								{
									text: ''
									, class: 'tnt-clock2'
								}
								, {
									text: 'Pendentes'
									, class: ''
								}
								, {
									text: $filter('number')(response.data.pending)
									, class: 'right'
								}
							]
						}
						, {
							typeFilter: 'analyzing'
							, href: response.data.analyzing ? '/analise-de-cupom-2/analyzing/' : '/analise-de-cupom-2/analyzing/'
							, listText : [
								{
									text: ''
									, class: 'tnt-search'
								}
								, {
									text: 'Em Análise'
									, class: ''
								}
								, {
									text: ''
									, class: 'right'
								}
							]
						}
						, {
							typeFilter: 'approved'
							, href: response.data.approved ? '/analise-de-cupom-2/approved/' : ''
							, listText : [
								{
									text: ''
									, class: 'tnt-check3'
								}
								, {
									text: 'Aprovados'
									, class: ''
								}
								, {
									text: $filter('number')(response.data.approved)
									, class: 'right'
								}
							]
						}
						, {
							typeFilter: 'disapproved'
							, href: response.data.disapproved ? '/analise-de-cupom-2/disapproved/' : ''
							, listText : [
								{
									text: ''
									, class: 'tnt-close2'
								}
								, {
									text: 'Reprovados'
									, class: ''
								}
								, {
									text: $filter('number')(response.data.disapproved)
									, class: 'right'
								}
							]
						}
					]
				}

				if ($routeParams.typeFilter == 'pending'){
					count = response.data.pending;
				}else if ($routeParams.typeFilter == 'analyzing'){
					count = response.data.analyzing;
				}else if ($routeParams.typeFilter == 'approved'){
					count = response.data.approved;
				}else if ($routeParams.typeFilter == 'disapproved'){
					count = response.data.disapproved;
				}


				ng.filters.amtPages   = Math.ceil(count/ng.filters.object.top);
				ng.filters.totalItems = count;

				ng.reviewButton = response.data.pending;

				var dateNow = new Date()
				ng.countdownUpdate = new Date(dateNow);
				ng.countdownUpdate.setSeconds(dateNow.getSeconds() + 32);

			}, function(response) {
				ng.infoBlockGroup = [];
			})
		}


		if ($routeParams.typeFilter != undefined){
			ng.getCouponsTotals();
		}


		var stringReject = function(value) {
			if (!_.isEmpty(value)) {
				var obj = _.filter(ngRoot.rejectReasons, function(obj) {
					return obj.key == value
				})

				if (!_.isEmpty(obj)) {
					return obj[0].value;
				}
			}
		}

		ng.getCouponsList = function() {
			if (!_.isEmpty($routeParams.typeFilter)) {
				ng.reports = [];
				ng.loaderReports = true;


				var object = {
					promotionId   : $routeParams.idPromo
					, statusCupom : $routeParams.typeFilter
					, skip        : ng.filters.object.skip
					, top         : ng.filters.object.top
				}

				if (!_.isEmpty($routeParams.filterCPF)) {
					object.cpf = $routeParams.filterCPF;
				}

				API.getCouponsList(object)
				.then(function(response) {
					// SUCESSO
					if (_.isEmpty(response.data)) {
						return $q.reject(response);
					}

					ng.reports = [];

					angular.forEach(response.data.participations, function (value, key) {

						ng.reports.push({
							id                 : value.transactionId
							, index            : key
							, image            : null
							, fnClick          : '$root.modalEditShow(obj)'
							, promotionId      : value.promotionId
							, userId      	   : value.userId
							, cardId      	   : value.cardId
							, cupom_info       : [
								{
									label: 'Data'
									, label_class: 'col-8'
									, value: $filter('data_timestamp')(value.date) +' às '+ $filter('hora_timestamp')(value.date)
									, value_class: 'tnt-calendario'
								}
								, {
									label: 'Data da Análise'
									, label_class: 'col-8'
									, value: $filter('data_timestamp')(value.statusCupomDate) +' às '+ $filter('hora_timestamp')(value.statusCupomDate)
									, value_class: 'tnt-calendario'
								}
								, {
									label: 'Nome Operador'
									, label_class: 'col-8'
									, value: value.userAdminName
									, value_class: 'tnt-user'
								}
								, {
									label: 'Motivo de reprovação'
									, label_class: 'col-8'
									, value: stringReject(value.reason)
									, value_class: 'tnt-tag'
								},
								{
									label: 'Etapa Completada'
									, label_class: 'col-8'
									, value: (!_.isEmpty(value.cardCompleteDate)) ? $filter('data_timestamp')(value.cardCompleteDate) +' às '+ $filter('hora_timestamp')(value.cardCompleteDate) : 'em andamento'
									, value_class: 'tnt-calendario'
								}
							]
						});

					});


					// ng.filters.amtPages   = Math.ceil(response.data.count/ng.filters.object.top);
					// ng.filters.totalItems = response.data.count;

					// if (response.data.count == 0) {
					// 	ng.filters.amtPages = 0;
					// }

				}, function(response) {
					ng.reports = [];
				})
				.finally(function(response) {
					ng.loaderReports = false;

					$timeout(function() {
						var baseHeightItemList = (ngRoot.windowHeight - (70+25+50+15+50+80+60+20+25+30));

						var nodes = $('.block-group.block-image-info .content');
						for (var i = 0; i < nodes.length; i++) {
							// IMAGEM
							var nodesMain = $(nodes[i]).children()
							var heightItemList = (baseHeightItemList - nodesMain[1].clientHeight*2)/2;
							$(nodesMain[0]).children()[0].style.height = heightItemList + 'px';

							// ICONE DE VISUALIZAR
							var nodeIcon = $(nodes[i]).children().children().children()[1];
							nodeIcon.style.height     = (heightItemList+25) + 'px';
							nodeIcon.style.lineHeight = (heightItemList+25) + 'px';
							nodeIcon.style.top        = -(heightItemList+25) + 'px';
						}

						// CONTAINER DA LISTAGEM
						if (ng.reports.length) {
							var nodes = $('.list-coupon').children()[0].clientHeight;
							$('.list-coupon').css('height', ($('.list-coupon').children()[0].clientHeight)*2 + 'px');
						}

					}, 100)

				})
			}
		}
		ng.getCouponsList();

		ngRoot.modalEditControl = false;
		ngRoot.modalEditShow = function(item) {
			ngRoot.rotateImg(0);

			ngRoot.modalEditControl = true;
			ngRoot.modalOverlay = true;
			ngRoot.modalInfo    = item;
		}

		ngRoot.showRejectReasons = false;

		ng.getCouponForAnalysis = function(id) {

			if (ng.loaderCoupon == false){

				ng.loaderCoupon = true;
				ngRoot.showRejectReasons = false;

				var dateNow = new Date()
				ng.dateCountdownAnalysis = new Date(dateNow);
				ng.dateCountdownAnalysis.setMinutes(dateNow.getMinutes() + 2);
				ng.dateCountdownAnalysis.setSeconds(dateNow.getSeconds() + 2);

				API.getCouponForAnalysis(id)
				.then(function(response) {
					// SUCESSO
					if (_.isEmpty(response.data)) {
						ngRoot.closeModalCouponAnalysis()
						notify({
							type: 'success'
							, title: 'Nenhum cupom disponível para análise!'
						});
						//ng.getCouponsTotals();
						return $q.reject(response);
					}

					ngRoot.modalEditShow();



					ng.couponAnalyzing = response.data;
					angular.toJson(ng.couponAnalyzing)
					console.log(ng.couponAnalyzing);

					if (!_.isEmpty(ng.couponAnalyzing.ocr) && !_.isEmpty(ng.couponAnalyzing.ocr.date)) {
						ng.couponAnalyzing.ocr.date  = $filter('data_timestamp')(ng.couponAnalyzing.ocr.date)
						ng.couponAnalyzing.ocr.date  = ng.couponAnalyzing.ocr.date.substring(0, 5);
						ng.couponAnalyzing.ocr.coo  = ng.couponAnalyzing.ocr.coo;
						ng.couponAnalyzing.ocr.total = _.intToFloat(ng.couponAnalyzing.ocr.total)
					}

					ng.couponAnalyzing.ocr.year = ""+dateNow.getFullYear();

				}, function(response) {
					// ERRO
					notify({
						type: 'error'
						, title: 'Ocorreu um erro ao recuperar o cupom para análise.'
					});
				})
				.finally(function(response) {
					// FINAL
					ng.formSubmitted = false;
					ng.loaderCouponAnalyzed = false;

					$timeout(function() {
						ng.loaderCoupon = false;
						ng.disableCupom = false;
					}, 3000)

					$timeout(function() {
						if (document.getElementById("ocrcoo").value == '') {
							document.getElementById("ocrcoo").focus();
						}

						if (
							document.getElementById("ocrtotal").value != '' &&
							document.getElementById("ocrdata").value != '' &&
							document.getElementById("ocrcoo").value != ''
						) {
							document.getElementById("aprovar").focus();
						}

					}, 1000)


				})

			}

		}

		ngRoot.closeModalCouponAnalysis = function() {
			ngRoot.modalEditControl = false;
			ngRoot.modalOverlay     = false;
			ng.formSubmitted        = false;

			if (!_.isEmpty(ng.couponAnalyzing)) {
				var objAPI = {
					transactionId : ng.couponAnalyzing.transactionId
				}

				API.postReleaseCouponForAnalysis(objAPI)
				.then(function(response) {
					// SUCCESS
				}, function(response) {
					// ERRO
					notify({
						type: 'error'
						, title: 'Erro o liberar cupom para análise'
					});
				})
				.finally(function(response) {
					// FINAL
					ng.getCouponsTotals();
					ng.getCouponsList();
				})
			}
		}

		ng.formSubmitted  = false;
		ng.postCouponAnalyzed = function(form, approved) {
			if (!_.isEmpty(ng.couponAnalyzing.productsNew)){
				ng.products = []
				angular.forEach(ng.couponAnalyzing.productsNew, function(value, index2) {
					console.log(value);
					if (value == true) {
						ng.products.push(index2)
					}
				})
				ng.couponAnalyzing.products = ng.products
			}

			ng.formSubmitted  = true;

			if (!approved) {
				ng.couponAnalyzing.ocr.date = null;
				form.date.$setValidity('dategreaterthantoday', true);
				form.date.$setValidity('data', true);
				form.date.$setValidity('mask', true);

				ng.couponAnalyzing.ocr.coo = null;
			}else{
				if (ng.couponAnalyzing.ocr.total == 0){
					form.total.$setValidity('totalminvalue', false);
					//form.total.$invalid(true);
				}else{
					form.total.$setValidity('totalminvalue', true);
				}

				if (_.isEmpty(ng.couponAnalyzing.products)){
					form.total.$setValidity('products', false);
				}else{
					form.total.$setValidity('products', true);
				}



				// dataActualy = new Date();

				// if (dateModeration > dataActualy){
				// 	form.date.$setValidity('dategreaterthantoday', true); // CAMPO VALIDO
				// } else {
				// 	form.date.$setValidity('dategreaterthantoday', false); // CAMPO INVALIDO
				// }


			}

			$timeout(function() {

				if (form.$valid) {

					ng.loaderCouponAnalyzed = true;

					if (_.isEmpty(ng.couponAnalyzing.ocr.date)) {
						dateModeration = new Date(ng.couponAnalyzing.date * 1000);
					} else {
						ng.couponAnalyzing.ocr.date = ng.couponAnalyzing.ocr.date + ng.couponAnalyzing.ocr.year;
						dateModeration = _.toJSDate(ng.couponAnalyzing.ocr.date).toISOString();
					}

					// if (!approved) {
					// 	dateModeration = null;
					// }

					var objAPI = {
						transactionId : ng.couponAnalyzing.transactionId
						, promotionId : ng.couponAnalyzing.promotionId
						, userId      : ng.couponAnalyzing.userId
						, approved    : approved
						, reason      : approved ? '' : ng.couponAnalyzing.reason
						, cnpj        : _.isEmpty(ng.couponAnalyzing.ocr.cnpj) ? null : ng.couponAnalyzing.ocr.cnpj
						, date        : dateModeration
						, coo         : _.isEmpty(ng.couponAnalyzing.ocr.coo) ? null : ng.couponAnalyzing.ocr.coo
						, total       : _.isEmpty(ng.couponAnalyzing.ocr.total) ? 0 : ng.couponAnalyzing.ocr.total
						, products	  : _.isEmpty(ng.couponAnalyzing.products) ? null : ng.couponAnalyzing.products
					}

					if (!_.isEmpty(ng.products)){
						objAPI['products'] = ng.products
					}

					API.postCouponAnalyzed(objAPI)
					.then(function(response) {
						// SUCESSO
						if (_.isEmpty($routeParams.filterCPF)) {
							ng.getCouponForAnalysis();
						}

						notify({
							type: 'success'
							, title: 'Análise enviada :)'
						});

					}, function(response) {
						// ERRO
						ng.formSubmitted = false;
						ng.loaderCouponAnalyzed = false;
						notify({
							type: 'error'
							, title: 'Erro na análise do cupom'
						});

					})
					.finally(function(response) {
						// FINAL
						//ng.formSubmitted = false;
						//ng.loaderCouponAnalyzed = false;
						//ng.getCouponsTotals();
						//ng.getCouponsList();
					})

				}

			}, 10)

		}

		ngRoot.modalInfoPrev = function() {
			if ((ngRoot.modalInfo.index - 1) < 0) {
				ngRoot.modalInfo = ng.reports[ng.reports.length - 1];
			} else {
				ngRoot.modalInfo = ng.reports[ngRoot.modalInfo.index - 1];
			}
		}

		ngRoot.modalInfoNext = function() {
			if ((ngRoot.modalInfo.index + 1) >= ng.reports.length) {
				ngRoot.modalInfo = ng.reports[0];
			} else {
				ngRoot.modalInfo = ng.reports[ngRoot.modalInfo.index + 1];
			}
		}

		ng.init = function(){
			//ng.cooFocus = true;
			data = new Date();
			//ng.year = ""+data.getFullYear();
			//ng.couponAnalyzing.ocr.year = ""+data.getFullYear();
			//console.log(ng.year);
		}

		ng.init()

	}
]);
