app.controller('ReParticipants2Ctrl', [
	'$scope'
	, '$rootScope'
	, 'API'
	, '_'
	, '$routeParams'
	, function(ng, ngRoot, API, _, $routeParams) {

	ng.$routeParams = $routeParams;

	ng.getParticipants = function() {
		var object = {};

		if (!_.isEmpty(ng.filters.searchTerm)) {
			if (ng.filters.searchField == 'cpf') {
				object.cpf = ng.filters.searchTerm;
			} else {
				object.email = ng.filters.searchTerm;
			}
		}

		ng.loaderReports = true;

		if ($routeParams.typePromo == 'BUY_AND_WIN') {
			var promise = API.getPromoBuyAndWin($routeParams.idPromo, object);
		} else {
			var promise = API.getUserCPF($routeParams.idPromo, object.cpf, object);
		}

		promise
		.then(function(response) {
			// SUCESSO
			var data = response.data;
			if (!data) {
				return $q.reject(response);
			}

			ng.reports = data;

		}, function() {
			ng.reports = [];
		})
		.finally(function() {
			ng.loaderReports = false;
		})

	}

	ng.initFilters = function() {
		ng.filters = {
			searchField : 'cpf'
			, searchTerm  : ''
		}
	}
	ng.initFilters();

	ng.reports = [];
	ng.getParticipants();

}]);
