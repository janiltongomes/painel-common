app.controller('ReParticipants2DetailsCtrl', [
	'$scope'
	, '$rootScope'
	, 'API'
	, '$q'
	, '_'
	, '$routeParams'
	, '$filter'
	, '$timeout'
	, function(ng, ngRoot, API, $q, _, $routeParams, $filter, $timeout) {

		ng.$routeParams = $routeParams;

		ng.loaderReports = true;
		ng.loaderCoupon = false;
		ng.disableCupom = false;

		if (_.isEmpty(ngRoot.rejectReasons)) {
			API.getCouponRejectReason()
			.then(function(response) {
				// SUCESSO
				ngRoot.rejectReasons = response.data;

			}, function(response) {
				// ERRO
				notify({
					type: 'error'
					, title: 'Não foi possível recuperar os motivos de reprovação'
				});
			})
			.finally(function(response) {
				// FINAL
			})
		}

		ng.filters = {
			amtPages      : 0
			, currentPage : 1
			, totalItems  : 0
			, object      : {
				skip      : 0
				, top     : 8
			}
		}

		ng.goToPage = function(event) {
			if (event.charCode == 13) {
				if (ng.filters.currentPage == 0) {
					ng.reports = [];
				} else {
					ng.calcSkip();
					ng.getCouponsList();
				}
			}
		}

		ng.calcSkip = function() {
			if (ng.filters.currentPage == 1) {
				ng.filters.object.skip = 0;
			} else {
				ng.filters.object.skip = (ng.filters.currentPage-1)*ng.filters.object.top;
			}
		}

		ng.nextPage = function() {
			if (ng.filters.currentPage < ng.filters.amtPages) {
				ng.filters.currentPage++;
				ng.calcSkip();
				ng.getCouponsList();
			}
		}

		ng.previousPage = function() {
			if (ng.filters.currentPage > 1) {
				ng.filters.currentPage--;
				ng.calcSkip();
				ng.getCouponsList();
			}
		}

		var stringReject = function(value) {
			if (!_.isEmpty(value)) {
				var obj = _.filter(ngRoot.rejectReasons, function(obj) {
					return obj.key == value
				})

				if (!_.isEmpty(obj)) {
					return obj[0].value;
				}
			}
		}


		ng.getCouponsListNNNN = function() {
			if (!_.isEmpty($routeParams.idCard)) {
				API.getPromoById($routeParams.idPromo)
				.then(function(response) {
					// SUCESSO
					var data = response.data;
					if (!data) {
						return $q.reject(response);
					}

					ng.promo = data;

				})

				ng.reports = [];
				ng.loaderReports = true;

				var object = {
					promotionId   : $routeParams.idPromo
					, cardId      : $routeParams.idCard
					, userId      : $routeParams.idUser
				}


				API.getCouponsListbyCard(object)
				.then(function(response) {
					// SUCESSO
					if (_.isEmpty(response.data)) {
						return $q.reject(response);
					}

					ng.reports = [];

					ngRoot.participant = response.data;
					ngRoot.redeemDate  = ngRoot.participant.redeemDate;
					ngRoot.earnedPoints  = ngRoot.participant.earnedPoints;

					ng.transactions = [];

					angular.forEach(ngRoot.participant.participations, function (value, key) {

						// ngRoot.participant.cpf = value.cpf;

						ng.transactions.push({
							id                 : value.transactionId
							, cpf              : value.cpf
							, image            : value.imageUrl
							, approved         : value.approved
							, fnClick          : '$root.modalShow(obj)'
							, cupom_info       : [
								{
									label: 'Tipo'
									, label_class: 'col-8'
									, value: value.paymentMethod
									, value_class: 'col-16'
								}
								, {
									label: 'Data Envio'
									, label_class: 'col-8'
									, value: $filter('data_timestamp_utc')(value.promotionProcessTimeStamp)
									, value_class: 'col-16'
								}
								, {
									label: 'Data Cupom'
									, label_class: 'col-8'
									, value: $filter('data_timestamp')(value.cupomDate)
									, value_class: 'col-16'
								}
								, {
									label: 'CNPJ'
									, label_class: 'col-8'
									, value: $filter('cnpj')(value.cnpj)
									, value_class: 'col-16'
								}
								, {
									label: 'COO'
									, label_class: 'col-8'
									, value: value.coo
									, value_class: 'col-16'
								}

								, {
									label: 'Total'
									, label_class: 'col-8'
									, value: $filter('currency')(value.total)
									, value_class: 'col-16'
								}

								, {
									label: 'Status'
									, label_class: 'col-8'
									, value: (value.approved && value.approved == true ? 'Aprovado' : ((value.approved || value.approved == false) ? 'Reprovado' : ''))
									, value_class: 'col-16'
								}
								, {
									label: 'Produtos'
									, label_class: 'col-8'
									, value: value.products
									, value_class: 'col-16'
								}
								, {
									label: 'Motivo'
									, label_class: 'col-8'
									, value: (value.reason ? value.reason : '')
									, value_class: 'col-16'
								}
							]
						});

					});


					// ng.filters.amtPages   = Math.ceil(response.data.count/ng.filters.object.top);
					// ng.filters.totalItems = response.data.count;

					// if (response.data.count == 0) {
					// 	ng.filters.amtPages = 0;
					// }

				}, function(response) {
					ng.reports = [];
				})
				.finally(function(response) {
					ng.loaderReports = false;



				})
			}
		}

		ng.getCouponsList = function() {
			if (!_.isEmpty($routeParams.idCard)) {
				ng.reports = [];
				ng.loaderReports = true;
				//$routeParams.statusCard
				var object = {
					promotionId   : $routeParams.idPromo
					, cardId      : $routeParams.idCard
					, userId      : $routeParams.idUser
				}

				API.getCouponsListbyCard(object)
				.then(function(response) {
					// SUCESSO
					if (_.isEmpty(response.data)) {
						return $q.reject(response);
					}

					ng.reports = [];

					angular.forEach(response.data.participations, function (value, key) {
						if ($routeParams.statusCard != 'reprovado' && (value.approved == true || value.approved == null)){

						}else{

							//value.approved = null


							if (value.approved == true || value.paymentMethod == 'PAYPAL' ){
								value.statusCupom = "Aprovado";
							}else{
								value.statusCupom = "Reprovado";
							}

							ng.reports.push({
								id                 : value.transactionId
								, index            : key
								, image            : value.imageUrl
								, fnClick          : '$root.modalEditShow(obj)'
								, approvedBack     : value.approved
								, approved         : (value.approved && value.approved == true) ? true : false
								, statusCupom      : value.statusCupom
								, reason           : value.reason
								, reasonBack       : value.reason
								, paymentMethod    : value.paymentMethod
								, cupom_info       : [
									{
										label: 'Data'
										, label_class: 'col-8'
										, value: $filter('data_timestamp')(value.date)
										, value_class: 'tnt-calendario'
									}
									, {
										label: 'Data da Análise'
										, label_class: 'col-8'
										, value: $filter('data_timestamp')(value.statusCupomDate) +' às '+ $filter('hora_timestamp')(value.statusCupomDate)
										, value_class: 'tnt-calendario'
									}
									, {
										label: 'Nome Operador'
										, label_class: 'col-8'
										, value: value.userAdminName
										, value_class: 'tnt-user'
									}
									, {
										label: 'Motivo de reprovação'
										, label_class: 'col-8'
										, value: stringReject(value.reason)
										, value_class: 'tnt-tag'
									},
									{
										label: (value.cardCompleteDate) ? 'Etapa Completada' : ''
										, label_class: 'col-8'
										, value: (value.cardCompleteDate) ? $filter('data_timestamp')(value.cardCompleteDate) +' às '+ $filter('hora_timestamp')(value.cardCompleteDate) : ''
										, value_class: 'tnt-calendario'
									}
								]
							});
						}
					});

					ng.filters.amtPages   = Math.ceil(response.data.count/ng.filters.object.top);
					ng.filters.totalItems = response.data.count;

					if (response.data.count == 0) {
						ng.filters.amtPages = 0;
					}

				}, function(response) {
					ng.reports = [];
				})
				.finally(function(response) {
					ng.loaderReports = false;

					// $timeout(function() {
					// 	var baseHeightItemList = (ngRoot.windowHeight - (70+25+50+15+50+80+60+20+25+30));

					// 	var nodes = $('.block-group.block-image-info .content');
					// 	for (var i = 0; i < nodes.length; i++) {
					// 		// IMAGEM
					// 		var nodesMain = $(nodes[i]).children()
					// 		var heightItemList = (baseHeightItemList - nodesMain[1].clientHeight*2)/2;
					// 		$(nodesMain[0]).children()[0].style.height = heightItemList + 'px';

					// 		// ICONE DE VISUALIZAR
					// 		var nodeIcon = $(nodes[i]).children().children().children()[1];
					// 		nodeIcon.style.height     = (heightItemList+25) + 'px';
					// 		nodeIcon.style.lineHeight = (heightItemList+25) + 'px';
					// 		nodeIcon.style.top        = -(heightItemList+25) + 'px';
					// 	}

					// 	// CONTAINER DA LISTAGEM
					// 	if (ng.reports.length) {
					// 		var nodes = $('.list-coupon').children()[0].clientHeight;
					// 		$('.list-coupon').css('height', ($('.list-coupon').children()[0].clientHeight)*2 + 'px');
					// 	}

					// }, 100)

				})
			}
		}
		ng.getCouponsList();

		ngRoot.modalEditControl = false;
		ngRoot.modalEditShow = function(item) {
			ngRoot.rotateImg(0);

			ngRoot.modalEditControl = true;
			ngRoot.modalOverlay = true;
			ngRoot.modalInfo    = item;
		}

		ngRoot.showRejectReasons = false;

		ng.getCouponForAnalysis = function(id) {

			if (ng.loaderCoupon == false){

				ng.loaderCoupon = true;
				ngRoot.showRejectReasons = false;

				var dateNow = new Date()
				ng.dateCountdownAnalysis = new Date(dateNow);
				ng.dateCountdownAnalysis.setMinutes(dateNow.getMinutes() + 2);
				ng.dateCountdownAnalysis.setSeconds(dateNow.getSeconds() + 2);

				API.getCouponForAnalysis(id, true)
				.then(function(response) {
					// SUCESSO
					if (_.isEmpty(response.data)) {
						ngRoot.closeModalCouponAnalysis()
						notify({
							type: 'success'
							, title: 'Nenhum cupom disponível para análise!'
						});
						//ng.getCouponsTotals();
						return $q.reject(response);
					}

					ngRoot.modalEditShow();

					ng.couponAnalyzing = response.data;

					if (!_.isEmpty(ng.couponAnalyzing.ocr) && !_.isEmpty(ng.couponAnalyzing.ocr.date)) {
						ng.couponAnalyzing.ocr.date  = $filter('data_timestamp')(ng.couponAnalyzing.ocr.date)
						ng.couponAnalyzing.ocr.date  = ng.couponAnalyzing.ocr.date.substring(0, 5);
						ng.couponAnalyzing.ocr.coo  = ng.couponAnalyzing.ocr.coo;
						ng.couponAnalyzing.ocr.total = _.intToFloat(ng.couponAnalyzing.ocr.total)
					}

					ng.couponAnalyzing.ocr.year = ""+dateNow.getFullYear();

				}, function(response) {
					// ERRO
					notify({
						type: 'error'
						, title: 'Ocorreu um erro ao recuperar o cupom para análise.'
					});
				})
				.finally(function(response) {
					// FINAL
					ng.formSubmitted = false;
					ng.loaderCouponAnalyzed = false;

					$timeout(function() {
						ng.loaderCoupon = false;
						ng.disableCupom = false;
					}, 3000)

					$timeout(function() {
						if (document.getElementById("ocrtotal").value == 'R$ 0,00') {
							document.getElementById("ocrtotal").focus();
						}

						if (
							document.getElementById("ocrtotal").value != '' &&
							document.getElementById("ocrdata").value != '' &&
							document.getElementById("ocrcoo").value != ''
						) {
							document.getElementById("aprovar").focus();
						}

					}, 1000)

				})

			}

		}

		ngRoot.closeModalCouponAnalysis = function() {
			ngRoot.modalEditControl = false;
			ngRoot.modalOverlay     = false;
			ng.formSubmitted        = false;
		}

		ng.formSubmitted = false;
		ng.postCouponAnalyzed = function(form, approved) {

			ng.formSubmitted = true;

			$timeout(function() {


				if (form.$valid || ng.couponAnalyzing.paymentMethod == 'QR_CODE') {

					if (_.isEmpty(ng.couponAnalyzing.ocr.date)) {
						dateModeration = new Date();
					} else {
						ng.couponAnalyzing.ocr.date = ng.couponAnalyzing.ocr.date + ng.couponAnalyzing.ocr.year;
						dateModeration = _.toJSDate(ng.couponAnalyzing.ocr.date).toISOString();
					}

					var objAPI = {
						transactionId   : ng.couponAnalyzing.transactionId
						, approved      : approved
						, paymentMethod : ng.couponAnalyzing.paymentMethod
						, rejectReason  : approved ? '' : ng.couponAnalyzing.reason
						, cnpj          : _.isEmpty(ng.couponAnalyzing.ocr.cnpj) ? null : ng.couponAnalyzing.ocr.cnpj
						, date          : dateModeration
						, coo           : _.isEmpty(ng.couponAnalyzing.ocr.coo) ? null : ng.couponAnalyzing.ocr.coo
						, total         : _.isEmpty(ng.couponAnalyzing.ocr.total) ? 0 : ng.couponAnalyzing.ocr.total
					}

					//ng.formSubmitted = false;
					//ng.loaderCouponAnalyzed = false;
					ng.formSubmitted = false;
					ng.reportsModify = []

					angular.forEach(ng.reports, function (value, key) {
						if (value.id == objAPI.transactionId){
							value.approved = objAPI.approved;
							value.reason   = objAPI.reason;
							value.objAPI = objAPI;
						}
						ng.reportsModify.push(value)
						if (value.approved){
							//ng.loaderApproval = true;
						}
					})

					ng.reports = ng.reportsModify;
					//filteredArray = filterFilter(ng.reports, {approved:true});
					filteredArray = $filter('filter')(ng.reports, {approved:true});

					if (($routeParams.statusCard == 'reprovado' && filteredArray.length >= 3) || ($routeParams.statusCard != 'reprovado')){
						ng.saveTransactions = true;
					}

					ngRoot.closeModalCouponAnalysis()
					// notify({
					// 	type: 'success'
					// 	, title: 'Nenhum cupom disponível para análise!'
					// });
					//return $q.reject(response);
				}
			}, 10)


		}

		ng.formSubmitted = false;
		ng.postCouponAnalyzedFinal = function(form, forced) {

			if (forced && forced === true){
				sendforced = true;
			}else{
				sendforced = false;
			}

			ng.formSubmitted = true;

			$timeout(function() {
				if (form.$valid) {
					ng.loaderApproval = true;
					data = {}
					data.transactions = []

					angular.forEach(ng.reports, function (value, key) {
						if (value.approved == true){
							data.transactions.push(value.objAPI)
						}
					})


					if ($routeParams.statusCard == 'reprovado'){
						data.transactionType = "reaproval";
						console.log(1);
					}else{
						console.log(2);
						data.transactionType = "reaproval_v2";
					}

// {
// 	"cpf": "82683944047",
// 	"transactionId": "47e86fa2-0032-4c89-53fe-02f7b201eeba",
// 	"cnpj": "22228979000192",
// 	"coo": "745742",
// 	"date": "03\/10\/2017",
// 	"total": 145.0,
// 	"promotionId": "591f0747a2f90b0c375c68ec",
// 	"forceApprove": true,
// 	"remoderation": true
// }

					data.userId = ng.couponAnalyzing.userId;
					data.promotionId = ng.couponAnalyzing.promotionId;
					data.observation = ngRoot.motivoReapproval;
					data.force = sendforced;
					data.forceApprove = sendforced;
					data.remoderation = true;

					if (data.transactionType == "reaproval"){
						API.postCouponAnalyzedReapproval(data)
						.then(function(response) {
							// SUCESSO
							_.URL.path('/reanalise-de-cupom/' + $routeParams.typePromo + '/' + $routeParams.idPromo + '/');

							notify({
								type: 'success'
								, title: 'Análise enviada :)'
							});

						}, function(response) {
							// ERRO
							ng.formSubmitted = false;
							ng.loaderApproval = false;
							ng.sendFormForced = true;

							notify({
								type: 'error'
								, title: response.data
							});


						})
						.finally(function(response) {
							// FINAL
							//ng.formSubmitted = false;
							//ng.loaderCouponAnalyzed = false;
							//ng.getCouponsList();
							ng.loaderApproval = false;
						})
					}else{
						API.postCouponAnalyzedReapprovalModeration(data)
						.then(function(response) {
							// SUCESSO
							_.URL.path('/reanalise-de-cupom/' + $routeParams.typePromo + '/' + $routeParams.idPromo + '/');

							notify({
								type: 'success'
								, title: 'Análise enviada :)'
							});

						}, function(response) {
							// ERRO
							ng.formSubmitted = false;
							ng.loaderApproval = false;
							ng.sendFormForced = true;

							notify({
								type: 'error'
								, title: response.data
							});


						})
						.finally(function(response) {
							// FINAL
							//ng.formSubmitted = false;
							//ng.loaderCouponAnalyzed = false;
							//ng.getCouponsList();
							ng.loaderApproval = false;
						})
					}


				}
			}, 10)

		}

		ngRoot.modalInfoPrev = function() {
			if ((ngRoot.modalInfo.index - 1) < 0) {
				ngRoot.modalInfo = ng.reports[ng.reports.length - 1];
			} else {
				ngRoot.modalInfo = ng.reports[ngRoot.modalInfo.index - 1];
			}
		}

		ngRoot.modalInfoNext = function() {
			if ((ngRoot.modalInfo.index + 1) >= ng.reports.length) {
				ngRoot.modalInfo = ng.reports[0];
			} else {
				ngRoot.modalInfo = ng.reports[ngRoot.modalInfo.index + 1];
			}
		}

		ng.init = function(){
			//ng.cooFocus = true;
			ng.cupomArray = [];
		 	ng.saveTransactions = false;
		 	ng.sendFormForced = false;
		 	ngRoot.motivoReapproval = "";
		 	// if ($routeParams.statusCard == 'false'){
		 	// 	$routeParams.statusCard = false;
		 	// }else{
		 	// 	$routeParams.statusCard = true;
		 	// }
		}

		ng.init()


	}
]);
