app.controller('ReParticipants2CardsCtrl', [
	'$scope'
	, '$rootScope'
	, '$location'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, function(ng, ngRoot, $location, $routeParams, _, API, $q, $filter) {

	var getCards = function() {
		ng.loaderReports = true;

		API.getPromoRaizenCards($routeParams.idPromo, $routeParams.idUser)
		.then(function(response) {
			// SUCESSO
			if (!response.data) {
				return $q.reject(response);
			}

			ngRoot.cards = [];
			var dataLength = response.data.length;
			for (var i = 0; i < dataLength; i++) {

				statsActualy = API.getstatscardraizen(response.data[i])


				if (statsActualy == 'moderacao'){
					link = "#";
				}else{
					link = '/reanalise-de-cupom/' + $routeParams.typePromo + '/' + $routeParams.idPromo + '/usuario/' + $routeParams.idUser + '/cartoes/' + response.data[i].cardId + '/' + statsActualy;
				}


				ngRoot.cards.push({
					id: response.data[i].cardId
					, main: {
						icon: 'tnt-photo-coupon'
						, href: link
						, listText : [
							{
								text: 'Primeiro envio em ' + $filter('data_timestamp')(response.data[i].firstSubmitionDate)
								, class: 'small'
							}
							, {
								text: ((response.data[i].cardCompleteDate) ? 'Cartão completado em '+ $filter('data_timestamp')(response.data[i].cardCompleteDate) : '')
								, class: 'small'
							}
							, {
								text: 'Status Atual: ' + statsActualy
								, class: 'small'
							}
						]
					}
					// , feature : {
					// 	icon: response.data[i].reedemDate ? 'tnt-check2 cl-n-success' : 'tnt-close cl-n-error'
					// 	, class: 'small'
					// 	, text: response.data[i].reedemDate ? 'Prêmio Resgatado' : 'Prêmio Não Resgatado'
					// }
					, obj: response.data[i]
				})
			}

		}, function(response) {
			if (response.status == 404) {
				notify({
					type: 'error'
					, title: 'Erro'
					, desc: 'O consumidor não possui mais cartões!'
				})
				_.URL.path('/reanalise-de-cupom/' + $routeParams.typePromo + '/' + $routeParams.idPromo + '/')
			}
		})
		.finally(function() {
			ng.loaderReports = false;
		})
	}
	getCards();

}]);
