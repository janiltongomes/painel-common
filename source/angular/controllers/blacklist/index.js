app.controller('BlacklistPromotionCtrl', [
	'$scope'
	, '$rootScope'
	, 'API'
	, '_'
	, '$filter'
	, '$routeParams'
	, function(ng, ngRoot, API, _, $filter, $routeParams) {

		// PAGINACAO
		ng.goToPage = function(event) {
			if (event.charCode == 13) {
				if (ng.filters.currentPage == 0) {
					ng.init = [];
				} else {
					ng.get_blacklist_promotion();
				}
			}
		}

		ng.nextPage = function() {
			if (ng.filters.currentPage < ng.filters.amtPages) {
				ng.filters.currentPage++;
				ng.get_blacklist_promotion();
			}
		}
		ng.previousPage = function() {
			if (ng.filters.currentPage > 1) {
				ng.filters.currentPage--;
				ng.get_blacklist_promotion();
			}
		}

		ng.delete = function (id) {

			if (ng.loaderListBlacklistPromotion == false) {

				ng.loaderListBlacklistPromotion = true;
//ng.idPromo
				API.deleteUpdateBlacklistPromotion(ng.idPromo, id)
				.then(function(response){

					ngRoot.notification.title = 'Usuário removido com sucesso :)';
					ngRoot.notification.type  = 'success';
					notify(ngRoot.notification);

					ng.get_blacklist_promotion();

				}, function(response){

					ngRoot.notification.title = 'Erro ao remover usuário :(';
					ngRoot.notification.type  = 'error';
					notify(ngRoot.notification);

				})
			}
		}


		ng.get_blacklist_promotion = function(resetPage) {

			if (resetPage) {
				ng.filters.currentPage = 1;
				ng.calcSkip();
			}

			ng.loaderListBlacklistPromotion = true;
			ng.listBlacklistPromotion = [];

			var filtro = {
				'page'   : ng.filters.currentPage-1
				, 'size' : ng.filters.amtItemsPage
				, 'cpf'  : ng.filters.cpf
			}

			API.getBlacklistPromotion( ng.idPromo , filtro)
			.then(function(response){

				if (response.data.items.length > 0) {

					ng.filters.totalItems = response.data.totalItems;
					ng.filters.amtPages   = response.data.pageCount;

					angular.forEach(response.data.items, function(value, index){
						value.date = value.date ? $filter('date')(value.date, 'dd/MM/yyyy H:mm:ss') : '-';
						value.cpf  = value.cpf ? $filter('cpf')(value.cpf) : '-';
						ng.listBlacklistPromotion.push(value);
					})
				}

			})
			.finally(function(){
				 ng.loaderListBlacklistPromotion = false;
			})
		}


		ng.init = function() {

			ng.filters    = {
				amtItemsPage    : 10
				, amtPages      : 0
				, currentPage   : 1
				, codigoposto   : ''
				, loaderReports : false
				, totalItems    : 0
				, cpf           : null
			}

			ng.idPromo    = $routeParams.idPromo;

			ng.listAction = [
				{
					label    : 'Excluir'
					, fn     : 'delete'
					, param  : ['id']
				}
			];

			ng.get_blacklist_promotion();
		}

		ng.init();
	}
]);


