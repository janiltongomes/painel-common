app.controller('UltimasTransacoesCtrl', [
	'$scope'
	, '$rootScope'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, '$timeout'
	, function(ng, ngRoot, $routeParams, _, API, $q, $filter, $timeout) {

		ng.convertTimestampToDate = function(timestamp){
			var date, newDate;
			date  = $filter('data_timestamp')(timestamp);
			date  = date.split("/");
			newDate = date[2]+"/"+date[1]+"/"+date[0];
			return newDate;
		}

		ng.convertDatetoUTC = function(data){
			data = data * 1000;
			date_long = new Date(data)
			//data.setTime(data.getTime() + (3*60*60*1000));
			var data = new Date(date_long.getUTCFullYear(), date_long.getUTCMonth(), date_long.getUTCDate(),  date_long.getUTCHours(), date_long.getUTCMinutes(), date_long.getUTCSeconds());
			return data;
		}

		ngRoot.modalEditControl = false;
		ngRoot.modalEditShow = function(item) {
			//ngRoot.rotateImg(0);
			console.log(item);

			ngRoot.modalEditControl = true;
			ngRoot.modalOverlay = true;
			ngRoot.modalInfo    = item;
		}

		ng.getReports = function() {
			ng.loaderReports = true;

			API.getLastTransactions($routeParams.idPromo, 0, 50)
			.then(function(response) {
				// SUCESSO
				var data = response.data;
				if (!data) {
					return $q.reject(response);
				}

				// SUCESSO
				if (!response.data) {
					return $q.reject(response);
				}
				if (response.data.records.length > 0) {
					ng.filters.totalItems = response.data.totalRecordsCount;
					ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
					ng.reports = response.data.records;
				}


			}, function() {
				ng.reports = [];
			})
			.finally(function() {
				ng.loaderReports = false;
			})

		}

		ngRoot.closeModalLastTransactions = function() {
			ngRoot.modalEditControl = false;
			ngRoot.modalOverlay     = false;
		}

		ng.getReports();


		ng.dateRangePicker = function(){
			var dateRangePickerIsHide = angular.element($('.angular-date-range-picker__picker.ng-scope.angular-date-range-picker--ranged')).hasClass('ng-hide');

			$timeout(function() {
				if (dateRangePickerIsHide) {
					angular.element($('.angular-date-range-picker__input')).triggerHandler('click');
				}
			}, 100);
		}

		ng.initFilters = function() {
			var start = new Date();
			start     = new Date(start.getFullYear()+'/'+(start.getMonth() + 1)+'/01' );
			var end   = new Date();



			// ng.range   = '';
			ng.range   = moment().range(start, end);
			ng.filters = {
				datainicial : ''
				, datafinal : ''
				, type : 'dayrly'
			}
		}
		ng.initFilters();


		ng.$watch('range', function() {
			//ng.getReports();
		})

	}
]);
