app.controller('OCRCtrl', ['$scope', '$rootScope', '$routeParams', '_', 'API', '$q', '$filter', function(ng, ngRoot, $routeParams, _, API, $q, $filter) {

	ngRoot.modalControl = false;
	ngRoot.modalShow = function(item) {
		ngRoot.modalControl = true;
		ngRoot.modalOverlay = true;

		API.getPromoTransaction(item.id)
		.then(function(response) {
			// SUCCESS
			ngRoot.modalInfo = response.data;
		}, function(response) {
			// ERROR
		});
	}

	ng.getParticipant = function() {
		ng.loaderReports = true;
		API.getPromoTransactions($routeParams.idPromo)
		.then(function(response) {
			// SUCESSO
			if (!response.data) {
				return $q.reject(response);
			}

			ng.transactions = [];

			angular.forEach(response.data, function (value, key) {

				ng.transactions.push({
					id                 : value.transactionId
					, image            : value.image
					, fnClick          : '$root.modalShow(obj)'
					, cupom_info       : [
						{
							label: 'Data'
							, label_class: 'col-8'
							, value: $filter('data_timestamp')(value.ocrTimeStamp)
							, value_class: 'col-16'
						}
						, {
							label: 'OS'
							, label_class: 'col-8'
							, value: value.osVersion
							, value_class: 'col-16'
						}
						, {
							label: 'Dispositivo'
							, label_class: 'col-8'
							, value: value.deviceModel
							, value_class: 'col-16'
						}
						, {
							label: 'Transação Completada'
							, label_class: 'col-8'
							, value: (value.completedTransaction ? 'Sim' : 'Não')
							, value_class: 'col-16'
						}
						, {
							label: 'Nome de usuário'
							, label_class: 'col-8'
							, value: value.userName
							, value_class: 'col-16'
						}
					]
				});

			});

		})
		.finally(function() {
			ng.loaderReports = false;
		})
	}
	ng.getParticipant();

}]);
