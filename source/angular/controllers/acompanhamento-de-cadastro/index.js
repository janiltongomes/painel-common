app.controller('RegisterCtrl', [
	'$scope'
	, '$rootScope'
	, '$routeParams'
	, 'API'
	, function(ng, ngRoot, $routeParams, API) {

	// PAGINACAO
	ng.goToPage = function(event) {
		if (event.charCode == 13) {
			if (ng.filters.currentPage == 0) {
				ng.users = [];
			} else {
				ng.calcSkip();
				ng.getRegisters();
			}
		}
	}
	ng.calcSkip = function() {
		if (ng.filters.currentPage == 1) {
			ng.filters.skip = 0;
		} else {
			ng.filters.skip = (ng.filters.currentPage-1)*ng.filters.top;
		}
	}
	ng.nextPage = function() {
		if (ng.filters.currentPage < ng.filters.amtPages) {
			ng.filters.currentPage++;
			ng.calcSkip();
			ng.getRegisters();
		}
	}
	ng.previousPage = function() {
		if (ng.filters.currentPage > 1) {
			ng.filters.currentPage--;
			ng.calcSkip();
			ng.getRegisters();
		}
	}

	ng.resetFilters = function() {
		// PAGINATION
		ng.filters.amtPages    = 0;
		ng.filters.currentPage = 1;
		ng.filters.totalItems  = 0;
		ng.filters.skip        = 0;
		ng.filters.top         = 10;
	}

	ng.filters = {}
	ng.resetFilters();

	ng.getRegisters = function() {
		ng.loaderUsers = true;
		ng.users = [];

		API.getReportsRegister($routeParams.idPromo, ng.filters.skip, ng.filters.top)
		.then(function(response) {
			// SUCESSO
			if (!response.data) {
				return $q.reject(response);
			}

			if (response.data.records.length > 0) {
				ng.filters.totalItems = response.data.totalRecordsCount;
				ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
				ng.users = response.data.records;
			}

		}, function() {
			ng.users = [];
		})
		.finally(function() {
			ng.loaderUsers = false;
		})
	}
	ng.getRegisters();

	ng.openExcel = function() {
		ng.loaderExcel = true;

		API.getReportsRegisterExcel_totaly($routeParams.idPromo)
		.then(function(response) {
			_.downloadArchive(response.data, 'excel', 'relatorio.csv');
		})
		.finally(function() {
			ng.loaderExcel = false;
		});
	}

	ng.orderBy = 'date';

}]);
