app.controller('ComparativoPromocoesCtrl', [
	'$scope'
	, '$rootScope'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, '$timeout'
	, function(ng, ngRoot, $routeParams, _, API, $q, $filter, $timeout) {


		ng.getReports3 = function() {
			ng.loaderReports = true;

			API.getevolutioncoupons($routeParams.idPromo)
			.then(function(response) {
				// SUCESSO
				var data = response.data;
				if (!data) {
					return $q.reject(response);
				}
				// SUCESSO
				if (!response.data) {
					return $q.reject(response);
				}
				if (response.data.records.length > 0) {
					ng.filters.totalItems = response.data.totalRecordsCount;
					ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
					ng.reports3 = response.data.records;
				}

				var labels     = [];
				var aa         = [];
				var bb         = [];
				var cc         = [];
				var dd         = [];
				var ee         = [];
				var ff         = [];


				var registros = 0
				var dia = [];

				angular.forEach(data.records, function (value, key) {

					//console.log(value);
					//labels.push('Etapa '+ value.Etapa);
					value.Indice = parseInt(value.Indice);
					typeCount = 'Etapa'
					if (!dia[value.Indice]){
						dia[value.Indice] = [];
					}

					dia[value.Indice][value.Promocao] = value.Quantidade;
					//console.log(value.Promocao);
					registros += 1
				});

				console.log(dia);

				dia.forEach(function (value2, key2) {
					/*if (key2 == 67){
						console.log(value2);
						console.log(value2['Milhas Extras']);
					}*/
					//console.log(value2);
					labels.push('Dia ' + key2);
					aa.push(value2['Ganhe Sempre'] ? value2['Ganhe Sempre'] : null)
					bb.push(value2['Ferrari'] ? value2['Ferrari'] : null)
					cc.push(value2['Milhas'] ? value2['Milhas'] : null)
					dd.push(value2['Milhas Extras'] ? value2['Milhas Extras'] : null)
					ee.push(value2['25 Mil Milhas'] ? value2['25 Mil Milhas'] : null)

				})


				//console.log(dia);

				ng.chartReport3 = {
					x: {
						labels: labels
					}
					, y: {
						label: 'Valores'
					}
					, exporting: {
						enabled : true
						, filename: 'grafico'
					}
					, series: [
						, {
							label: 'Ganhe Sempre'
							, value: aa
						}
						, {
							label: 'Ferrari'
							, value: bb
						}
						, {
							label: 'Milhas'
							, value: cc
						}
						, {
							label: 'Milhas Extras'
							, value: dd
						}
						, {
							label: '25 Mil Milhas'
							, value: ee
						}
					]
				}


			}, function() {
				ng.reports3 = [];
			})
			.finally(function() {
				ng.loaderReports = false;
			})

		}



		ng.getReports4 = function() {
			ng.loaderReports = true;

			API.getevolutioncouponsbycpf($routeParams.idPromo)
			.then(function(response) {
				// SUCESSO
				var data = response.data;
				if (!data) {
					return $q.reject(response);
				}
				//console.log(data);
				// SUCESSO
				if (!response.data) {
					return $q.reject(response);
				}
				if (response.data.records.length > 0) {
					ng.filters.totalItems = response.data.totalRecordsCount;
					ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
					ng.reports4 = response.data.records;
				}

				var labels     = [];
				var aaa        = [];
				var bbb        = [];
				var ccc        = [];
				var ddd        = [];
				var eee        = [];
				var ff         = [];


				var registros = 0
				var dia = [];

				angular.forEach(data.records, function (value, key) {

					//console.log(value);
					//labels.push('Etapa '+ value.Etapa);
					value.Indice = parseInt(value.Indice);
					typeCount = 'Etapa'
					if (!dia[value.Indice]){
						dia[value.Indice] = [];
					}

					dia[value.Indice][value.Promocao] = value.Quantidade;

					registros += 1
				});

				dia.forEach(function (value2, key2) {
					labels.push('Dia ' + key2);
					aaa.push(value2['Ganhe Sempre'] ? value2['Ganhe Sempre'] : null)
					bbb.push(value2['Ferrari'] ? value2['Ferrari'] : null)
					ccc.push(value2['Milhas'] ? value2['Milhas'] : null)
					ddd.push(value2['Milhas Extras'] ? value2['Milhas Extras'] : null)
					eee.push(value2['25 Mil Milhas'] ? value2['25 Mil Milhas'] : null)

				})


				//console.log(dia);

				ng.chartReport4 = {
					x: {
						labels: labels
					}
					, y: {
						label: 'Valores'
					}
					, exporting: {
						enabled : true
						, filename: 'grafico'
					}
					, series: [
						, {
							label: 'Ganhe Sempre'
							, value: aaa
						}
						, {
							label: 'Ferrari'
							, value: bbb
						}
						, {
							label: 'Milhas'
							, value: ccc
						}
						, {
							label: 'Milhas Extras'
							, value: ddd
						}
						, {
							label: '25 Mil Milhas'
							, value: eee
						}
					]
				}


			}, function() {
				ng.reports4 = [];
			})
			.finally(function() {
				ng.loaderReports = false;
			})

		}


		ng.getReports3();
		ng.getReports4();

		ng.dateRangePicker = function(){
			var dateRangePickerIsHide = angular.element($('.angular-date-range-picker__picker.ng-scope.angular-date-range-picker--ranged')).hasClass('ng-hide');

			$timeout(function() {
				if (dateRangePickerIsHide) {
					angular.element($('.angular-date-range-picker__input')).triggerHandler('click');
				}
			}, 100);
		}

		ng.initFilters = function() {
			var start = new Date();
			start     = new Date(start.getFullYear()+'/'+(start.getMonth() + 1)+'/01' );
			var end   = new Date();
			ng.totalreport1 = 0;
			ng.totalreport2 = 0;
			ng.total = {};
			ng.total['Aprovado'] = 0;
			ng.total['EmCurso'] = 0;
			ng.total['EmModeracao'] = 0;
			ng.total['Expirado'] = 0;
			ng.total['Reprovado'] = 0;
			ng.total['QtdUsuariosEmEmModeracao'] = 0;
			ng.total['QtdGenhadores'] = 0;
			ng.total['QtdGanhadoresUnicos'] = 0;
			ng.total['QtdMilhasPadrao'] = 0;
			ng.total['QtdMilhasShell'] = 0;
			ng.total['QtdMilhasSmiles'] = 0;

			// ng.range   = '';
			ng.range   = moment().range(start, end);
			ng.filters = {
				datainicial : ''
				, datafinal : ''
				, type : 'dayrly'
			}
		}
		ng.initFilters();

		ng.$watch('range', function() {
			//ng.getReports();
		})

	}
]);
