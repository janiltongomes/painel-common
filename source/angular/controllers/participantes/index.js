app.controller('ParticipantsCtrl', [
	'$scope'
	, '$rootScope'
	, 'API'
	, '_'
	, '$filter'
	, '$routeParams'
	, '$timeout'
	, '$q'
	, function(ng, ngRoot, API, _, $filter, $routeParams, $timeout, $q) {

	ng.$routeParams = $routeParams;

	// PAGINACAO
	ng.goToPage = function(event) {
		if (event.charCode == 13) {
			if (ng.filters.currentPage == 0) {
				ng.reports = [];
			} else {
				ng.calcSkip();
				ng.getParticipants();
			}
		}
	}
	ng.calcSkip = function() {
		if (ng.filters.currentPage == 1) {
			ng.filters.skip = 0;
		} else {
			ng.filters.skip = (ng.filters.currentPage-1)*ng.filters.top;
		}
	}
	ng.nextPage = function() {
		if (ng.filters.currentPage < ng.filters.amtPages) {
			ng.filters.currentPage++;
			ng.calcSkip();
			ng.getParticipants();
		}
	}
	ng.previousPage = function() {
		if (ng.filters.currentPage > 1) {
			ng.filters.currentPage--;
			ng.calcSkip();
			ng.getParticipants();
		}
	}

	ng.getParticipants = function() {

		ng.loaderReports = true;

		if ($routeParams.typePromo == 'BUY_AND_WIN') {

			var obj_filter = {
				winners : ng.filters.type == 'winners' ? true : false
				, cpf   : ng.filters.searchField == 'cpf' ? ng.filters.searchTerm : ''
				, email : ng.filters.searchField == 'email' ? ng.filters.searchTerm : ''
			}

			if (!_.isEmpty(ng.range)) {
				obj_filter.startDate = ng.range.start._d.toJSON().slice(0,10);
				obj_filter.endDate   = ng.range.end._d.toJSON().slice(0,10);
			}

			var callAPI = API.getPromoBuyAndWinConsumers($routeParams.idPromo, obj_filter)

		} else {

			// FIXME: Remover estes campos
			if (!_.isEmpty(ng.range)) {
				var params = {
					date1: ng.range.start._d.toJSON().slice(0,10)
					, date2: ng.range.end._d.toJSON().slice(0,10)
				}
			}

			if (!_.isEmpty(ng.filters.searchTerm)) {
				ng.filters.type = 'all';
				if (ng.filters.searchField == 'cpf') {
					var callAPI = API.getPromoConsumersByCPF($routeParams.idPromo, ng.filters.searchTerm)
				} else if (ng.filters.searchField == 'email') {
					var callAPI = API.getPromoConsumersByEmail($routeParams.idPromo, ng.filters.searchTerm)
				} else {
					var callAPI = API.getPromoConsumersByConsumerId($routeParams.idPromo, ng.filters.searchTerm)
				}

			} else if (!_.isEmpty(ng.range)) {
				if (ng.filters.type == 'all') {
					var callAPI = API.getPromoConsumersDate($routeParams.idPromo, params, ng.filters.skip, ng.filters.top)
				} else {
					var callAPI = API.getPromoWinnersDate($routeParams.idPromo, params)
				}
			} else {
				if (ng.filters.type == 'all') {
					var callAPI = API.getPromoFoodRewardConsumers($routeParams.idPromo, ng.filters.skip, ng.filters.top)
				} else {
					var callAPI = API.getPromoWinners($routeParams.idPromo, ng.filters.skip, ng.filters.top)
				}
			}

		}

		ng.reports = [];

		callAPI
		.then(function(response) {
			// SUCESSO
			if (!response.data) {
				return $q.reject(response);
			}

			if (response.data.records.length > 0) {
				ng.filters.totalItems = response.data.totalRecordsCount;
				ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
				ng.reports = response.data.records;
			}

		}, function() {
			ng.reports = [];
		})
		.finally(function() {
			ng.loaderReports = false;
		})

	}

	ng.rangeSelectOptions = [
		{
			label : 'Hoje',
			range : moment().range(
				moment().range(moment().startOf('day').startOf('day'),
				moment().endOf('day').startOf('day'))
			)
		}
		, {
			label : 'Esta semana',
			range : moment().range(
				moment().startOf('week').startOf('dom'),
				moment().endOf('week').startOf('sex')
			)
		}
		, {
			label : 'Semana anterior',
			range : moment().range(
				moment().startOf('week').add(-1, 'week').startOf('day'),
				moment().add(-1, 'week').endOf('week').startOf('sex')
			)
		}
		, {
			label : 'Esta quinzena',
			range : moment().range(
				moment().startOf('week').add(-1, 'week').startOf('day'),
				moment().endOf('week').startOf('sex')
			)
		}
		, {
			label : 'Este mês',
			range : moment().range(
				moment().startOf('month').startOf('day'),
				moment().endOf('week').startOf('day')
			)
		}
		, {
			label : 'Mês anterior',
			range : moment().range(
				moment().startOf('month').add(-1, 'month').startOf('day'),
				moment().add(-1, 'month').endOf('month').startOf('day')
			)
		},
	]

	ng.dateRangePicker = function(){
		var dateRangePickerIsHide = angular.element($('.angular-date-range-picker__picker.ng-scope.angular-date-range-picker--ranged')).hasClass('ng-hide');

		$timeout(function() {
			if (dateRangePickerIsHide) {
				angular.element($('.angular-date-range-picker__input')).triggerHandler('click');
			}
		}, 100);
	}

	ng.resetFilters = function() {
		// PAGINATION
		ng.filters.amtPages    = 0;
		ng.filters.currentPage = 1;
		ng.filters.totalItems  = 0;
		ng.filters.skip        = 0;
		ng.filters.top         = 10;
	}

	ng.initFilters = function() {
		var start = new Date();
		start.setDate(start.getDate() - 7);
		var end   = new Date();

		// ng.range   = moment().range(start, end);
		ng.range   = '';
		ng.filters = {
			type          : 'all'
			, searchField : 'cpf'
			, searchTerm  : ''
			// FIXME
			, startDate   : ''
			, endDate     : ''
		}

		ng.resetFilters();
	}
	ng.initFilters();

	ng.reports = [];
	ng.getParticipants();

}]);
