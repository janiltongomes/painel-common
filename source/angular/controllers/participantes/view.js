app.controller('ParticipantsDetailsCtrl', [
	'$scope'
	, '$rootScope'
	, '$location'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, function(ng, ngRoot, $location, $routeParams, _, API, $q, $filter) {

	ng.premioResgatado = function(idUser) {
		ng.loaderPremioResgatado = true;
		API.getPromoWinnerRedeemed($routeParams.idPromo, idUser)
		.then(function() {
			// SUCESSO
			notify({
				type: 'success'
				, title: 'Sucesso!'
			})
		}, function() {
			notify({
				type: 'error'
				, title: 'Erro ao executar a operação!'
			})
		})
		.finally(function() {
			ng.loaderPremioResgatado = false;
			ng.getParticipant();
		})
	}

	ng.transactionsList = [];
	ng.pushCouponTransaction = function(item) {
		if (item.elegible) {
			// APROVADO
			ng.disapproved = false;
		} else {
			// DESAPROVADO
			ng.approved = false;
		}
		var id = item.id.toString();
		if (ng.transactionsList.indexOf(id) == -1) {
			ng.transactionsList.push(id);
		} else {
			var index = ng.transactionsList.indexOf(id);
			ng.transactionsList.splice(index, 1);
		}
	}

	// ngRoot.toggleCouponQualify = function(status, idTransaction) {
	// 	var coupon_obj = {
	// 		userId         : $routeParams.idUser
	// 		, promotionId  : $routeParams.idPromo
	// 		, transactions : idTransaction ? [idTransaction] : ng.transactionsList
	// 	}
	// 	ng.transactionsList = [];
	// 	ngRoot.modalControl = false;

	// 	if (status) {
	// 		// APROVAR

	// 		API.postCouponQualify(coupon_obj)
	// 		.then(function(response) {
	// 			// SUCESSO
	// 			notify({
	// 				type: 'success'
	// 				, title: 'Aprovado com sucesso!'
	// 			})
	// 		}, function() {
	// 			notify({
	// 				type: 'error'
	// 				, title: 'Erro'
	// 				, desc: 'Ocorreu um erro ao qualificar o cupom, tente novamente!'
	// 			})
	// 		})
	// 		.then(function() {
	// 			ng.getParticipant();
	// 		})

	// 	} else {
	// 		// DESAPROVAR

	// 		API.postCouponDisqualify(coupon_obj)
	// 		.then(function(response) {
	// 			// SUCESSO
	// 			notify({
	// 				type: 'success'
	// 				, title: 'Desaprovado com sucesso!'
	// 			})
	// 		}, function() {
	// 			notify({
	// 				type: 'error'
	// 				, title: 'Erro'
	// 				, desc: 'Ocorreu um erro ao desqualificar o cupom, tente novamente!'
	// 			})
	// 		})
	// 		.then(function() {
	// 			ng.getParticipant();
	// 		})
	// 	}
	// }

	ngRoot.toggleCouponProduct = function(status, idTransaction) {
		var coupon_obj = {
			userId         : $routeParams.idUser
			, promotionId  : $routeParams.idPromo
			, transactions : idTransaction ? [idTransaction] : ng.transactionsList
		}
		ng.transactionsList = [];
		ngRoot.modalControl = false;

		if (status) {
			// APROVAR

			API.postProductFound(coupon_obj)
			.then(function(response) {
				// SUCESSO
				notify({
					type: 'success'
					, title: 'Cupom atualizado com sucesso!'
				})
			}, function() {
				notify({
					type: 'error'
					, title: 'Erro'
					, desc: 'Ocorreu um erro ao atualizar cupom, tente novamente!'
				})
			})
			.then(function() {
				ng.getParticipant();
			})

		} else {
			// DESAPROVAR

			API.postProductNotFound(coupon_obj)
			.then(function(response) {
				// SUCESSO
				notify({
					type: 'success'
					, title: 'Cupom atualizado com sucesso!'
				})
			}, function() {
				notify({
					type: 'error'
					, title: 'Erro'
					, desc: 'Ocorreu um erro ao atualizar cupom, tente novamente!'
				})
			})
			.then(function() {
				ng.getParticipant();
			})
		}
	}

	// ng.getParticipant = function() {
	// 	ng.loaderReports = true;
	// 	API.getPromoById($routeParams.idPromo)
	// 	.then(function(response) {
	// 		// SUCESSO
	// 		var data = response.data;
	// 		if (!data) {
	// 			return $q.reject(response);
	// 		}

	// 		ng.promo = data;

	// 		if ($routeParams.typePromo == 'BUY_AND_WIN') {
	// 			return API.getPromoBuyAndWinWinnerDetails($routeParams.idPromo, $routeParams.idUser)
	// 		} else {
	// 			return API.getPromoFoodRewardWinnerDetails($routeParams.idPromo, $routeParams.idUser)
	// 		}
	// 	})

	// 	.then(function(response) {
	// 		// SUCESSO
	// 		if (!response.data) {
	// 			return $q.reject(response);
	// 		}

	// 		ngRoot.participant = response.data;
	// 		ngRoot.redeemDate  = ngRoot.participant.redeemDate;

	// 		ng.transactions = [];

	// 		angular.forEach(ngRoot.participant.transactions, function (value, key) {

	// 			ng.transactions.push({
	// 				id                 : value.transactionId
	// 				, image            : value.image
	// 				, elegible         : value.elegible
	// 				, approved         : value.approved
	// 				, hasFoundProducts : value.hasFoundProducts
	// 				, fnClick          : '$root.modalShow(obj)'
	// 				, cupom_info       : [
	// 					{
	// 						label: 'Tipo'
	// 						, label_class: 'col-8'
	// 						, value: value.paymentMethod
	// 						, value_class: 'col-16'
	// 					}
	// 					, {
	// 						label: 'Data Envio'
	// 						, label_class: 'col-8'
	// 						, value: $filter('data_timestamp')(value.promotionProcessTimeStamp)
	// 						, value_class: 'col-16'
	// 					}
	// 					, {
	// 						label: 'Data Cupom'
	// 						, label_class: 'col-8'
	// 						, value: $filter('data_timestamp')(value.date)
	// 						, value_class: 'col-16'
	// 					}
	// 					, {
	// 						label: 'CNPJ'
	// 						, label_class: 'col-8'
	// 						, value: $filter('cnpj')(value.cnpj)
	// 						, value_class: 'col-16'
	// 					}
	// 					, {
	// 						label: 'COO'
	// 						, label_class: 'col-8'
	// 						, value: value.coo
	// 						, value_class: 'col-16'
	// 					}

	// 					, {
	// 						label: 'Total'
	// 						, label_class: 'col-8'
	// 						, value: $filter('currency')(value.total)
	// 						, value_class: 'col-16'
	// 					}

	// 					, {
	// 						label: 'Status'
	// 						, label_class: 'col-8'
	// 						, value: (value.statusCupom == 'DISAPPROVED' ? 'Reprovado' : (value.statusCupom == 'APPROVED' ? 'Aprovado' : ''))
	// 						, value_class: 'col-16'
	// 					}
	// 					, {
	// 						label: 'Motivo'
	// 						, label_class: 'col-8'
	// 						, value: (value.reason ? value.reason : '')
	// 						, value_class: 'col-16'
	// 					}
	// 				]
	// 			});

	// 		});

	// 	})
	// 	.finally(function() {
	// 		ng.loaderReports = false;
	// 	})
	// }
	//ng.getParticipant();

		ng.convertDatetoUTC = function(data){
			data = data * 1000;
			data = new Date(data)
			data.setTime(data.getTime() + (3*60*60*1000));
			return data;
		}

		ng.getCouponsList = function() {
			if (!_.isEmpty($routeParams.idCard)) {
				API.getPromoById($routeParams.idPromo)
				.then(function(response) {
					// SUCESSO
					var data = response.data;
					if (!data) {
						return $q.reject(response);
					}

					ng.promo = data;

				})

				ng.reports = [];
				ng.loaderReports = true;

				var object = {
					promotionId   : $routeParams.idPromo
					, cardId      : $routeParams.idCard
					, userId      : $routeParams.idUser
				}


				API.getCouponsListbyCard(object)
				.then(function(response) {
					// SUCESSO
					if (_.isEmpty(response.data)) {
						return $q.reject(response);
					}

					ng.reports = [];

					ngRoot.participant = response.data;
					ngRoot.redeemDate  = ngRoot.participant.redeemDate;
					ngRoot.earnedPoints  = ngRoot.participant.earnedPoints;
					ngRoot.pointsProducts  = ngRoot.participant.pointsProducts;

					ng.transactions = [];

					angular.forEach(ngRoot.participant.participations, function (value, key) {

						// ngRoot.participant.cpf = value.cpf;
						dados = {
							id                 : value.transactionId
							, cpf              : value.cpf
							, image            : value.imageUrl
							, approved         : value.approved
							, leapRuleWinner   : value.leapRuleWinner
							, fnClick          : '$root.modalShow(obj)'
							, cupom_info       : [
								{
									label: 'Tipo'
									, label_class: 'col-8'
									, value: value.paymentMethod
									, value_class: 'col-16'
								}
								, {
									label: 'Data Envio'
									, label_class: 'col-8'
									, value: $filter('data_timestamp')(value.promotionProcessTimeStamp) + ' às ' + $filter('hora_timestamp')(value.promotionProcessTimeStamp)
									, valueOriginal: value.promotionProcessTimeStamp
									, value_class: 'col-16'
								}
								, {
									label: 'Data Cupom'
									, label_class: 'col-8'
									, value: (value.paymentMethod == 'QR_CODE' && (value.approved == true || value.approved == false) ) ? $filter('data_timestamp')(value.cupomDate) + ' às ' + $filter('hora_timestamp')(value.cupomDate): $filter('data_timestamp')(value.cupomDate)
									, value_class: 'col-16'
								}
								, {
									label: 'CNPJ'
									, label_class: 'col-8'
									, value: $filter('cnpj')(value.cnpj)
									, value_class: 'col-16'
								}
								, {
									label: 'COO'
									, label_class: 'col-8'
									, value: value.coo
									, value_class: 'col-16'
								}

								, {
									label: 'Total'
									, label_class: 'col-8'
									, value: $filter('currency')(value.total)
									, value_class: 'col-16'
								}

								, {
									label: 'Pontos'
									, label_class: 'col-8'
									, value: (value.pointsProducts) ? $filter('currency')(value.pointsProducts) : null
									, value_class: 'col-16'
								}

								, {
									label: 'Premiado Regra de Pulo'
									, label_class: 'col-8'
									, value: (value.leapRuleWinner && value.leapRuleWinner == true) ? 'SIM' : 'NÃO'
									, value_class: 'col-16'
								}

								, {
									label: 'Status'
									, label_class: 'col-8'
									, value: (value.approved && value.approved == true ? 'Aprovado' : ((value.approved || value.approved == false) ? 'Reprovado' : (value.pointsProducts && value.elegible == true ) ? 'Aprovado' : (value.elegible == false ) ? 'Reprovado' : (API.getpromotionleaprule($routeParams.idPromo) == true && (value.leapRuleWinner == false || value.leapRuleWinner == null)) ? 'Não sorteado' : 'Pendente'))
									, value_class: 'col-16'
								}

								, {
									label: 'Tipo Transação'
									, label_class: 'col-8'
									, value: value.transactionType
									, value_class: 'col-16'
								}
								, {
									label: 'Produtos'
									, label_class: 'col-8'
									, value: value.products
									, value_class: 'col-16'
								}
								, {
									label: 'Itens SEFAZ'
									, label_class: 'col-8'
									, value: (value.items ? value.items : null)
									, value_class: 'col-16'
								}
								, {
									label: 'Motivo'
									, label_class: 'col-8'
									, value: (value.reason ? value.reason : '')
									, value_class: 'col-16'
								}

							]
						};

						if (value.paymentMethod == 'PAYPAL'){
							//items sefaz
							delete dados['cupom_info'][dados['cupom_info'].length -2];
							if (value.transactionType != 'PARTICIPATION'){
								//coo
								delete dados['cupom_info'][4];
								//Data Cupom
								delete dados['cupom_info'][2];
								//Total
								delete dados['cupom_info'][5];
								//Produtos
								delete dados['cupom_info'][10];
								//premiado
								//delete dados['cupom_info'][7];
							}

						}else{
							//pontos
							delete dados['cupom_info'][6];
						}
						//console.log(dados['cupom_info'][11]);
						if (!dados['cupom_info'][11] || dados['cupom_info'][11].value == null){
							delete dados['cupom_info'][11];
						}


						if (!dados['cupom_info'][6] || (dados['cupom_info'][6].value == null)){
							delete dados['cupom_info'][6];
						}

						ng.transactions.push(dados);

					});


					// ng.filters.amtPages   = Math.ceil(response.data.count/ng.filters.object.top);
					// ng.filters.totalItems = response.data.count;

					// if (response.data.count == 0) {
					// 	ng.filters.amtPages = 0;
					// }

				}, function(response) {
					ng.reports = [];
				})
				.finally(function(response) {
					ng.loaderReports = false;



				})
			}
		}
		ng.getCouponsList();

}]);
