app.controller('ParticipantsCardsCtrl', [
	'$scope'
	, '$rootScope'
	, '$location'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, function(ng, ngRoot, $location, $routeParams, _, API, $q, $filter) {

	var getCards = function() {
		ng.loaderReports = true;

		API.getPromoCards($routeParams.idPromo, $routeParams.idUser)
		.then(function(response) {
			// SUCESSO
			if (!response.data) {
				return $q.reject(response);
			}

			ngRoot.cards = [];
			var dataLength = response.data.length;
			for (var i = 0; i < dataLength; i++) {
				if (response.data[i].statusApproved && response.data[i].statusApproved == "CANCEL_BY_RULE_FIRST_SUBMIT_EXPIRED"){
					status = "expirada";
				}else if (response.data[i].approved===true){
					status = "aprovada";
				}else if (response.data[i].approved===false){
					status = "reprovada";
				}else if (response.data[i].hasCompleted && response.data[i].hasCompleted===true){
					status = "em moderação"
				}else{
					status = "em curso"
				}
				ngRoot.cards.push({
					id: response.data[i].cardId
					, main: {
						icon: 'tnt-photo-coupon'
						, href: '/participantes/' + $routeParams.typePromo + '/' + $routeParams.idPromo + '/detalhe/' + $routeParams.idUser + '/cartoes/' + response.data[i].cardId
						, listText : [
							{
								text: 'Primeiro envio em ' + $filter('data_timestamp')(response.data[i].firstSubmitionDate)
								, class: 'small'
							}
							, {
								text: ((response.data[i].cardCompleteDate) ? 'Cartão completado em ' + $filter('data_timestamp')(response.data[i].cardCompleteDate) : 'Cartão em andamento')
								, class: 'small'
							}
							, {
								text: ((response.data[i].cycle) ? 'Ciclo ' + response.data[i].cycle : '')
								, class: 'small'
							}
							, {
								text: ((response.data[i].step) ? 'Etapa: ' + response.data[i].step : '')
								, class: 'small'
							}
							, {
								text: ((response.data[i].cardPoints) ? 'Pontos Cartão: ' + response.data[i].cardPoints: '')
								, class: 'small'
							}
							, {
								text: ((response.data[i].bonusPoints) ? 'Bonus Cartão: ' + response.data[i].bonusPoints: '')
								, class: 'small'
							}
							, {
								text: ((status) ? 'Status: ' + status : '')
								, class: 'small'
							}
						]
					}
					// , feature : {
					// 	icon: response.data[i].reedemDate ? 'tnt-check2 cl-n-success' : 'tnt-close cl-n-error'
					// 	, class: 'small'
					// 	, text: response.data[i].reedemDate ? 'Prêmio Resgatado' : 'Prêmio Não Resgatado'
					// }
					, obj: response.data[i]
				})
			}

		}, function(response) {
			if (response.status == 404) {
				notify({
					type: 'error'
					, title: 'Erro'
					, desc: 'O consumidor não possui mais cartões!'
				})
				_.URL.path('/participantes/' + $routeParams.typePromo + '/' + $routeParams.idPromo + '/')
			}
		})
		.finally(function() {
			ng.loaderReports = false;
		})
	}
	getCards();

}]);
