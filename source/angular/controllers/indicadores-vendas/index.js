app.controller('IndicadoresVendasCtrl', [
	'$scope'
	, '$rootScope'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, '$timeout'
	, function(ng, ngRoot, $routeParams, _, API, $q, $filter, $timeout) {

		ng.calcPercent = function(valor1, valor2){
			if (valor1 > 0){
				return (parseInt(valor1)/parseInt(valor2)*100) + '%'
			}else{
				return ''
			}

		}

		ng.getReports = function(a) {
			if (a != undefined){
				ng.directorId = a;
			}
			//console.log(a);
			ng.loaderReports = true;
			//ng.directorId = '5a37ffd32afbee4a018b8477';

			API.getIndicatorsSalesDirectors($routeParams.idPromo, 0, 50)
			.then(function(response) {
				// SUCESSO
				// data = []
				// data.push(response.data);
				// console.log(data);

				if (!response.data) {
					return $q.reject(response);
				}

				// SUCESSO
				if (!response.data) {
					return $q.reject(response);
				}
				// if (response.data.records.length > 0) {
				// 	ng.filters.totalItems = response.data.totalRecordsCount;
				// 	ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
				// 	ng.reports = response.data.records;
				// }

				ng.directors = [];
				ng.directorsFinal = [];
				ng.directorsCountBrasil = {};
				ng.directorsCountBrasil['avgCouponsPerStationByDay'] = 0;
				ng.directorsCountBrasil['avgParticipationsByCPF'] = 0;
				ng.directorsCountBrasil['avgParticipationsPerDay'] = 0;

				ng.directorsCountBrasil['directorId'] = 0;
				ng.directorsCountBrasil['name'] = 'Brasil';


				ng.directorsCountBrasil['participationsTotal'] = 0;
				ng.directorsCountBrasil['percentStationsWithoutCoupons'] = 0;
				ng.directorsCountBrasil['totalCNPJ'] = 0;
				ng.directorsCountBrasil['uniquesCPF'] = 0;
				ng.directorsCountBrasil['uniquesNewCPF'] = 0;

				angular.forEach(response.data, function (value, key) {
					//console.log(value);

					//console.log(key);
					ng.directorsCountBrasil['avgCouponsPerStationByDay'] += (value['avgCouponsPerStationByDay']);
					ng.directorsCountBrasil['avgParticipationsByCPF'] += (value['avgParticipationsByCPF']);
					ng.directorsCountBrasil['avgParticipationsPerDay'] += (value['avgParticipationsPerDay']);
					ng.directorsCountBrasil['participationsTotal'] += (value['participationsTotal']);
					ng.directorsCountBrasil['percentStationsWithoutCoupons'] += (value['percentStationsWithoutCoupons']);
					ng.directorsCountBrasil['totalCNPJ'] += (value['totalCNPJ']);
					ng.directorsCountBrasil['uniquesCPF'] += (value['uniquesCPF']);
					ng.directorsCountBrasil['uniquesNewCPF'] += (value['uniquesNewCPF']);

					value['avgCouponsPerStationByDay'] = (value['avgCouponsPerStationByDay']);
					value['avgParticipationsByCPF'] = (value['avgParticipationsByCPF']);
					value['avgParticipationsPerDay'] = (value['avgParticipationsPerDay']);
					//value['directorId'] = $filter('numberdecimal')(value['directorId']);
					//value['name'] = $filter('numberdecimal')(value['name']);
					value['participationsTotal'] = (value['participationsTotal']);
					value['percentStationsWithoutCoupons'] = (value['percentStationsWithoutCoupons']);
					value['totalCNPJ'] = (value['totalCNPJ']);
					value['uniquesCPF'] = (value['uniquesCPF']);
					value['uniquesNewCPF'] = (value['uniquesNewCPF']);
					//console.log(value['uniquesCPF']);
					// value.
					// if (value.Nome.indexOf("Média") >= 0){
					// 	qtd = String(value.Qtd);
					// 	value.Qtd = qtd.replace('.', ',');
					// }else{
					// 	value.Qtd = $filter('numberdecimal')(value.Qtd)
					// }


					ng.directors.push(value);
					//registros += 1
				});


				ng.directorsCountBrasil['avgCouponsPerStationByDay'] = (ng.directorsCountBrasil['avgCouponsPerStationByDay']);
				ng.directorsCountBrasil['avgParticipationsByCPF'] = (ng.directorsCountBrasil['avgParticipationsByCPF']);
				ng.directorsCountBrasil['avgParticipationsPerDay'] = (ng.directorsCountBrasil['avgParticipationsPerDay']);
				ng.directorsCountBrasil['participationsTotal'] = (ng.directorsCountBrasil['participationsTotal']);
				ng.directorsCountBrasil['percentStationsWithoutCoupons'] = (ng.directorsCountBrasil['percentStationsWithoutCoupons']);
				ng.directorsCountBrasil['totalCNPJ'] = (ng.directorsCountBrasil['totalCNPJ']);
				ng.directorsCountBrasil['uniquesCPF'] = (ng.directorsCountBrasil['uniquesCPF']);
				ng.directorsCountBrasil['uniquesNewCPF'] = (ng.directorsCountBrasil['uniquesNewCPF']);


				ng.directors.unshift(ng.directorsCountBrasil);


				angular.forEach(ng.directors, function (value, key) {

					if (value['participationsTotal'] == ng.directors[0]['participationsTotal']){

					}else{
						//value['avgCouponsPerStationByDay'] = value['avgCouponsPerStationByDay'] + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ $filter('numberdecimal')(parseInt(value['avgCouponsPerStationByDay'])/parseInt(ng.directors[0]['avgCouponsPerStationByDay'])*100) + '%'
						//value['avgParticipationsByCPF'] = value['avgParticipationsByCPF'] + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						value['avgParticipationsPerDayPCT'] = $filter('numberdecimal')(ng.calcPercent(value['avgParticipationsPerDay'], ng.directors[0]['avgParticipationsPerDay']))
						//value['directorId'] = $filter('numberdecimal')(value['directorId']);
						//value['name'] = $filter('numberdecimal')(value['name']);
						value['participationsTotalPCT'] = $filter('numberdecimal')(ng.calcPercent(value['participationsTotal'], ng.directors[0]['participationsTotal']))
						value['percentStationsWithoutCouponsPCT'] = $filter('numberdecimal')(ng.calcPercent(value['percentStationsWithoutCoupons'], ng.directors[0]['percentStationsWithoutCoupons']))
						value['totalCNPJPCT'] = $filter('numberdecimal')(ng.calcPercent(value['totalCNPJ'], ng.directors[0]['totalCNPJ']))
						value['uniquesCPFPCT'] = $filter('numberdecimal')(ng.calcPercent(value['uniquesCPF'], ng.directors[0]['uniquesCPF']))
						value['uniquesNewCPFPCT'] = ng.calcPercent(value['uniquesNewCPF'], ng.directors[0]['uniquesNewCPF'])


						value['avgParticipationsByCPF'] = $filter('numberdecimal')(value['avgParticipationsByCPF']);
						value['participationsTotal'] = $filter('numberdecimal')(value['participationsTotal']);
						value['percentStationsWithoutCoupons'] = $filter('numberdecimal')(value['percentStationsWithoutCoupons']);
						value['totalCNPJ'] = $filter('numberdecimal')(value['totalCNPJ']);
						value['uniquesCPF'] = $filter('numberdecimal')(value['uniquesCPF']);
						value['uniquesNewCPF'] = $filter('numberdecimal')(value['uniquesNewCPF']);
					}

					ng.directorsFinal.push(value);
					//registros += 1
				});

				ng.directorsFinal[0]['avgCouponsPerStationByDay'] = $filter('numberdecimal')(ng.directorsFinal[0]['avgCouponsPerStationByDay']/(ng.directors.length - 1));
				ng.directorsFinal[0]['avgParticipationsByCPF'] = $filter('numberdecimal')(ng.directorsFinal[0]['avgParticipationsByCPF']/(ng.directors.length - 1));
				ng.directorsFinal[0]['avgParticipationsPerDay'] = $filter('numberdecimal')(ng.directorsFinal[0]['avgParticipationsPerDay']/(ng.directors.length - 1));

				ng.directorsFinal[0]['participationsTotal'] = $filter('numberdecimal')(ng.directorsFinal[0]['participationsTotal']);
				ng.directorsFinal[0]['percentStationsWithoutCoupons'] = $filter('numberdecimal')(ng.directorsFinal[0]['percentStationsWithoutCoupons']);
				ng.directorsFinal[0]['totalCNPJ'] = $filter('numberdecimal')(ng.directorsFinal[0]['totalCNPJ']);
				ng.directorsFinal[0]['uniquesCPF'] = $filter('numberdecimal')(ng.directorsFinal[0]['uniquesCPF']);
				ng.directorsFinal[0]['uniquesNewCPF'] = $filter('numberdecimal')(ng.directorsFinal[0]['uniquesNewCPF']);



				//ng.directors = response.data

				if (ng.directorId == ''){
					return;
				}

				API.getIndicatorsSales($routeParams.idPromo, ng.directorId, 0, 50)
				.then(function(response) {
					// SUCESSO
					data = []
					data.push(response.data);

					if (!data) {
						return $q.reject(response);
					}

					// SUCESSO
					if (!response.data) {
						return $q.reject(response);
					}
					// if (response.data.records.length > 0) {
					// 	ng.filters.totalItems = response.data.totalRecordsCount;
					// 	ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
					// 	ng.reports = response.data.records;
					// }

					ng.reports = []


					angular.forEach(data, function (value, key) {

						value['avgCouponsPerStationByDay'] = $filter('numberdecimal')(value['avgCouponsPerStationByDay']);
						value['avgParticipationsByCPF'] = $filter('numberdecimal')(value['avgParticipationsByCPF']);
						value['avgParticipationsPerDay'] = $filter('numberdecimal')(value['avgParticipationsPerDay']);
						//value['directorId'] = $filter('numberdecimal')(value['directorId']);
						//value['name'] = $filter('numberdecimal')(value['name']);
						value['participationsTotal'] = $filter('numberdecimal')(value['participationsTotal']);
						value['percentStationsWithoutCoupons'] = $filter('numberdecimal')(value['percentStationsWithoutCoupons']);
						value['totalCNPJ'] = $filter('numberdecimal')(value['totalCNPJ']);
						value['uniquesCPF'] = $filter('numberdecimal')(value['uniquesCPF']);
						value['uniquesNewCPF'] = $filter('numberdecimal')(value['uniquesNewCPF']);

						value['cupomParticipations'] = $filter('numberdecimal')(value['cupomParticipations']);
						value['paypalParticipations'] = $filter('numberdecimal')(value['paypalParticipations']);
						value['avgTicketPayPal'] = 'R$ '+ $filter('numberdecimal')(value['avgTicketPayPal']);


						angular.forEach(value.grs, function (value2, key2) {
							value.grs[key2]['avgCouponsPerStationByDay'] = $filter('numberdecimal')(value.grs[key2]['avgCouponsPerStationByDay']);
							value.grs[key2]['avgParticipationsByCPF'] = $filter('numberdecimal')(value.grs[key2]['avgParticipationsByCPF']);
							value.grs[key2]['cnpjs'] = $filter('numberdecimal')(value.grs[key2]['cnpjs']);
							//value.grs[key2]['gts'] = $filter('numberdecimal')(value.grs[key2]['gts']);
							//value.grs[key2]['name'] = $filter('numberdecimal')(value.grs[key2]['name']);
							value.grs[key2]['participationsTotal'] = $filter('numberdecimal')(value.grs[key2]['participationsTotal']);
							value.grs[key2]['uniquesCPF'] = $filter('numberdecimal')(value.grs[key2]['uniquesCPF']);

							angular.forEach(value.grs[key2]['gts'], function (value3, key3) {
								value.grs[key2]['gts'][key3]['avgCouponsPerStationByDay'] = $filter('numberdecimal')(value.grs[key2]['gts'][key3]['avgCouponsPerStationByDay']);
								value.grs[key2]['gts'][key3]['avgParticipationsByCPF'] = $filter('numberdecimal')(value.grs[key2]['gts'][key3]['avgParticipationsByCPF']);
								value.grs[key2]['gts'][key3]['cnpjs'] = $filter('numberdecimal')(value.grs[key2]['gts'][key3]['cnpjs']);
								//value.grs[key['gts'][key3]2]['gts'] = $filter('numberdecimal')(value.grs[key2]['gts']);
								//value.grs[key2]['gts'][key3]['name'] = $filter('numberdecimal')(value.grs[key2]['name']);
								value.grs[key2]['gts'][key3]['participationsTotal'] = $filter('numberdecimal')(value.grs[key2]['gts'][key3]['participationsTotal']);
								value.grs[key2]['gts'][key3]['uniquesCPF'] = $filter('numberdecimal')(value.grs[key2]['gts'][key3]['uniquesCPF']);

							});

						});

						angular.forEach(value.weeks, function (value4, key4) {
							value.weeks[key4]['avgCouponsPerStationByDay'] = $filter('numberdecimal')(value.weeks[key4]['avgCouponsPerStationByDay']);
							value.weeks[key4]['avgParticipationsByCPF'] = $filter('numberdecimal')(value.weeks[key4]['avgParticipationsByCPF']);
							value.weeks[key4]['avgParticipationsPerDay'] = $filter('numberdecimal')(value.weeks[key4]['avgParticipationsPerDay']);
							value.weeks[key4]['endDate'] = $filter('numberdecimal')(value.weeks[key4]['endDate']);
							value.weeks[key4]['finishedSteps'] = $filter('numberdecimal')(value.weeks[key4]['finishedSteps']);
							value.weeks[key4]['milesDelivered'] = $filter('numberdecimal')(value.weeks[key4]['milesDelivered']);
							//value.weeks[key4]['name'] = $filter('numberdecimal')(value.weeks[key4]['name']);
							value.weeks[key4]['participationsTotal'] = $filter('numberdecimal')(value.weeks[key4]['participationsTotal']);
							//value.weeks[key4]['percentPaymentWithShellBox'] = $filter('numberdecimal')(value.weeks[key4]['percentPaymentWithShellBox']);
							//value.weeks[key4]['percentStationsWithoutCoupons'] = $filter('numberdecimal')(value.weeks[key4]['percentStationsWithoutCoupons']);
							//value.weeks[key4]['startDate'] = $filter('numberdecimal')(value.weeks[key4]['startDate']);
							value.weeks[key4]['uniquesCPF'] = $filter('numberdecimal')(value.weeks[key4]['uniquesCPF']);

						});



						// angular.forEach(value.grs, function (value2, key2) {
						// 	console.log(value2);
						// });

						ng.reports.push(value);
						//registros += 1
					});

					//console.log(ng.reports);
					ng.directorActualy = ng.reports[0]['directorName'];

					// ng.chartReport = {
					// 	x: {
					// 		labels: ['Shell Box - Foto Cupom', 'Shell Box - Pgto APP']
					// 	}
					// 	, y: {
					// 		label: 'Valores'
					// 	}
					// 	, series: [
					// 		{
					// 			label: 'Shell Box - Foto Cupom'
					// 			, value: ng.reports[0].cupomParticipations
					// 		}
					// 		, {
					// 			label: 'Shell Box - Pgto APP'
					// 			, value: ng.reports[0].paypalParticipations
					// 		}
					// 	]
					//     // , series: [{
					//     //     name: 'Promocao',
					//     //     data: [
					//     //         { name: 'Shell Box - Foto Cupom', y: ng.reports[0].cupomParticipations },
					//     //         { name: 'Shell Box - Pgto APP', y: ng.reports[0].paypalParticipations },
					//     //     ]
					//     // }]
					// 	, column: [
					// 		{
					// 			label: 'Pontos'
					// 			, data: [2,3,4,5,6,7]
					// 			, visible: false
					// 		}
					// 		, {
					// 			label: 'Transações'
					// 			, data: [2,3,4,5,6,7]
					// 		}
					// 	]

					//     , plotOptions: {
					//         pie: {
					//             allowPointSelect: true,
					//             cursor: 'pointer',
					//             dataLabels: [{
					//                 enabled: true,
					//                 format: '<b>bbb</b><br> %',
					//                 distance: -50,
					//                 filter: {
					//                     property: 'percentage',
					//                     operator: '>',
					//                     value: 4
					//                 }
					//             },{
					//                 enabled: true,
					//                 format: '<b>aaaa</b><br> %',
					//                 distance: -50,
					//                 filter: {
					//                     property: 'percentage',
					//                     operator: '>',
					//                     value: 4
					//                 }
					//             }]
					//         }
					//     },


						ng.chartReport = {
						    chart: {
						        plotBackgroundColor: null,
						        plotBorderWidth: null,
						        plotShadow: false,
						        type: 'pie'
						    },
						    title: {
						        text: ''
						    },
						    tooltip: {
						        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						    },
						    plotOptions: {
						        pie: {
						            allowPointSelect: true,
						            cursor: 'pointer',
						            dataLabels: {
						                enabled: true,
						                format: '<br>{point.percent} %',
						                distance: -50,
						                filter: {
						                    property: 'percentage',
						                    operator: '>',
						                    value: 4
						                }
						            }
						        }
						    },
						    series: [{
						        name: 'Brands',
						        data: [
						            { name: 'Shell Box - Foto Cupom', y: parseFloat(response.data['cupomParticipations']), percent: String(response.data['percentCupom']) },
						            { name: 'Shell Box - Pgto APP', y: parseFloat(response.data['paypalParticipations']), percent: String(response.data['percentPaypal']) }
						        ]
						    }]
						    // series: [{
						    //     name: 'Promocao',
						    //     data: [
						    //         { name: 'Shell Box - Foto Cupom', y: ng.reports[0].cupomParticipations },
						    //         { name: 'Shell Box - Pgto APP', y: ng.reports[0].paypalParticipations },
						    //     ]
						    // }]
						};


					    // , plotOptions: {
					    //     pie: {
					    //         dataLabels: {
					    //             enabled: true,
					    //             format: '<b>bbbbb</b>%',
					    //             distance: -50,
					    //             filter: {
					    //                 property: 'percentage',
					    //                 operator: '>',
					    //                 value: 4
					    //             }
					    //         }
					    //     }
					    // }
					//};


				}, function() {
					ng.reports = [];
				})
				.finally(function() {
					ng.loaderReports = false;

				})

			}, function() {
				ng.director
			})
			.finally(function() {
				ng.loaderReports = false;
			})


		}



		ng.dateRangePicker = function(){
			var dateRangePickerIsHide = angular.element($('.angular-date-range-picker__picker.ng-scope.angular-date-range-picker--ranged')).hasClass('ng-hide');

			$timeout(function() {
				if (dateRangePickerIsHide) {
					angular.element($('.angular-date-range-picker__input')).triggerHandler('click');
				}
			}, 100);
		}

		ng.initFilters = function() {
			ng.directorId = ''
			var start = new Date();
			start     = new Date(start.getFullYear()+'/'+(start.getMonth() + 1)+'/01' );
			var end   = new Date();

			// ng.range   = '';
			ng.range   = moment().range(start, end);
			ng.filters = {
				datainicial : ''
				, datafinal : ''
				, type : 'dayrly'
			}

			ng.getReports();
		}
		ng.initFilters();

		ng.$watch('range', function() {
			//ng.getReports();
		})

	}
]);
