app.controller('ProductsCtrl', [
	'$scope'
	, '$rootScope'
	, 'API'
	, '_'
	, '$filter'
	, '$routeParams'
	, '$timeout'
	, '$q'
	, function(ng, ngRoot, API, _, $filter, $routeParams, $timeout, $q) {

	ng.$routeParams = $routeParams;

	// PAGINACAO
	ng.goToPage = function(event) {
		if (event.charCode == 13) {
			if (ng.filters.currentPage == 0) {
				ng.reports = [];
			} else {
				ng.calcSkip();
				ng.getParticipants();
			}
		}
	}
	ng.calcSkip = function() {
		if (ng.filters.currentPage == 1) {
			ng.filters.skip = 0;
		} else {
			ng.filters.skip = (ng.filters.currentPage-1)*ng.filters.top;
		}
	}
	ng.nextPage = function() {
		if (ng.filters.currentPage < ng.filters.amtPages) {
			ng.filters.currentPage++;
			ng.calcSkip();
			ng.getParticipants();
		}
	}
	ng.previousPage = function() {
		if (ng.filters.currentPage > 1) {
			ng.filters.currentPage--;
			ng.calcSkip();
			ng.getParticipants();
		}
	}

	ng.getParticipants = function() {

		ng.loaderReports = true;

		// FIXME: Remover estes campos
		if (!_.isEmpty(ng.range)) {
			var params = {
				date1: ng.range.start._d.toJSON().slice(0,10)
				, date2: ng.range.end._d.toJSON().slice(0,10)
			}
		}

		if (!_.isEmpty(ng.filters.searchTerm)) {
			var callAPI = API.getPromoProductsByText(ng.filters.searchTerm, ng.filters.type)
		} else {
			var callAPI = API.getPromoProducts(ng.filters.skip, ng.filters.top, ng.filters.type)
		}

		ng.reports = [];

		callAPI
		.then(function(response) {
			// SUCESSO
			if (!response.data) {
				return $q.reject(response);
			}
			console.log(response.data.records);
			if (response.data.records.length > 0) {
				ng.filters.totalItems = response.data.totalRecordsCount;
				ng.filters.amtPages   = Math.ceil(response.data.totalRecordsCount/ng.filters.top);
				ng.reports = response.data.records;
			}

		}, function() {
			ng.reports = [];
		})
		.finally(function() {
			ng.loaderReports = false;
		})

	}

	ng.rangeSelectOptions = [
		{
			label : 'Hoje',
			range : moment().range(
				moment().range(moment().startOf('day').startOf('day'),
				moment().endOf('day').startOf('day'))
			)
		}
		, {
			label : 'Esta semana',
			range : moment().range(
				moment().startOf('week').startOf('dom'),
				moment().endOf('week').startOf('sex')
			)
		}
		, {
			label : 'Semana anterior',
			range : moment().range(
				moment().startOf('week').add(-1, 'week').startOf('day'),
				moment().add(-1, 'week').endOf('week').startOf('sex')
			)
		}
		, {
			label : 'Esta quinzena',
			range : moment().range(
				moment().startOf('week').add(-1, 'week').startOf('day'),
				moment().endOf('week').startOf('sex')
			)
		}
		, {
			label : 'Este mês',
			range : moment().range(
				moment().startOf('month').startOf('day'),
				moment().endOf('week').startOf('day')
			)
		}
		, {
			label : 'Mês anterior',
			range : moment().range(
				moment().startOf('month').add(-1, 'month').startOf('day'),
				moment().add(-1, 'month').endOf('month').startOf('day')
			)
		},
	]

	ng.dateRangePicker = function(){
		var dateRangePickerIsHide = angular.element($('.angular-date-range-picker__picker.ng-scope.angular-date-range-picker--ranged')).hasClass('ng-hide');

		$timeout(function() {
			if (dateRangePickerIsHide) {
				angular.element($('.angular-date-range-picker__input')).triggerHandler('click');
			}
		}, 100);
	}

	ng.openExcel = function() {
		ng.loaderExcel = true;

		API.getPromoProductsExport(ng.filters.type)
		.then(function(response) {
			_.downloadArchive(response.data, 'excel', 'relatorio.csv');
		})
		.finally(function() {
			ng.loaderExcel = false;
		});
	}

	ng.resetFilters = function() {
		// PAGINATION
		ng.filters.amtPages    = 0;
		ng.filters.currentPage = 1;
		ng.filters.totalItems  = 0;
		ng.filters.skip        = 0;
		ng.filters.top         = 10;
	}

	ng.initFilters = function() {
		var start = new Date();
		start.setDate(start.getDate() - 7);
		var end   = new Date();

		// ng.range   = moment().range(start, end);
		ng.range   = '';
		ng.filters = {
			type          : 'products'
			, searchField : 'cpf'
			, searchTerm  : ''
			// FIXME
			, startDate   : ''
			, endDate     : ''
		}

		ng.resetFilters();
	}
	ng.initFilters();

	ng.reports = [];
	ng.getParticipants();

}]);
