app.controller('Participants2DetailsCtrl', [
	'$scope'
	, '$rootScope'
	, '$location'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, function(ng, ngRoot, $location, $routeParams, _, API, $q, $filter) {

	var redirectToCards = function() {
		if ($routeParams.typePromo == 'BUY_AND_WIN') {
			_.URL.path('/analise-de-cupom/' + $routeParams.typePromo + '/' + $routeParams.idPromo + '/')
		} else {
			_.URL.path('/analise-de-cupom/' + $routeParams.typePromo + '/' + $routeParams.idPromo + '/usuario/' + $routeParams.idUser + '/')
		}
	}

	if ($routeParams.typePromo != 'BUY_AND_WIN' && !ngRoot.cards) {
		redirectToCards();
	}

	var getActualCard = function() {
		if ($routeParams.typePromo != 'BUY_AND_WIN') {
			ng.actualCard = _.filter(ngRoot.cards, function(obj) {
				return obj.id == $routeParams.idCard
			})

			if (!ng.actualCard) {
				redirectToCards()
			}

			ng.actualCard = ng.actualCard[0];
		}

		ng.transactions   = [];
		var coupons       = ng.actualCard.obj.participations
		var couponsLength = coupons.length
		for (var i = 0; i < couponsLength; i++) {

			ng.transactions.push({
				id                 : coupons[i].transactionId
				, index            : i
				, image            : coupons[i].urlImage
				, approved         : coupons[i].approved
				, rejectReason     : coupons[i].rejectReason ? coupons[i].rejectReason : ''
				, submitionDate    : $filter('data_timestamp')(coupons[i].submitionDate)
				, fnClick          : '$root.modalShow(obj)'
				, coupon_user_info : [
					{
						label: 'CNPJ do Consumidor'
						, label_class: 'col-9'
						, value: coupons[i].userParticipationTypedFields.cnpj
						, value_class: 'col-15 aright'
					}
					, {
						label: 'COO do Consumidor'
						, label_class: 'col-9'
						, value: coupons[i].userParticipationTypedFields.coo
						, value_class: 'col-15 aright'
					}
					, {
						label: 'Data do Consumidor'
						, label_class: 'col-9'
						, value: $filter('data_timestamp')(coupons[i].userParticipationTypedFields.cupomDate)
						, value_class: 'col-15 aright'
					}
					, {
						label: 'Total do Consumidor'
						, label_class: 'col-9'
						, value: $filter('currency')(coupons[i].userParticipationTypedFields.total)
						, value_class: 'col-15 aright'
					}
				]
				, coupon_ocr_info  : [
					{
						label: 'CNPJ do OCR'
						, label_class: 'col-8'
						, value: coupons[i].ocrparticipationFoundFields.cnpj
						, value_class: 'col-16'
					}
					, {
						label: 'COO do OCR'
						, label_class: 'col-8'
						, value: coupons[i].ocrparticipationFoundFields.coo
						, value_class: 'col-16'
					}
					, {
						label: 'Data do OCR'
						, label_class: 'col-8'
						, value: $filter('data_timestamp')(coupons[i].ocrparticipationFoundFields.cupomDate)
						, value_class: 'col-16'
					}
					, {
						label: 'Total do OCR'
						, label_class: 'col-8'
						, value: $filter('currency')(coupons[i].ocrparticipationFoundFields.total)
						, value_class: 'col-16'
					}
				]
			});

		};
	}

	ng.formSubmitted = false;
	ng.approveCoupons = function(form) {
		ng.formSubmitted = true;

		if (form.$valid) {
			ng.loaderApproval = true;
			var coupon_obj = {
				userId         : $routeParams.idUser
				, promotionId  : $routeParams.idPromo
				, transactions : []
			}
			angular.forEach(ng.transactions, function (value, key) {
				coupon_obj.transactions.push({
					transactionId  : value.id
					, approved     : value.approved
					, rejectReason : value.rejectReason
				})
			});

			ngRoot.modalControl = false;

			if ($routeParams.typePromo == 'BUY_AND_WIN') {
				var promise = API.postBuyAndWinCouponApproval($routeParams.idPromo, coupon_obj);
			} else {
				var promise = API.postFoodRewardCouponApproval($routeParams.idPromo, coupon_obj);
			}

			promise
			.then(function(response) {
				// SUCESSO
				notify({
					type: 'success'
					, title: 'Aprovado com sucesso!'
				})

				redirectToCards();

			}, function(response) {
				notify({
					type    : 'error'
					, title : 'Erro'
					, desc  : 'Ocorreu um erro ao aprovar o cupom, tente novamente!'
				})
			})
			.finally(function(response) {
				ng.formSubmitted  = false;
				ng.loaderApproval = false;
			})
		}
	}

	ngRoot.modalInfoPrev = function() {
		if ((ngRoot.modalInfo.index - 1) < 0) {
			ngRoot.modalInfo = ng.transactions[ng.transactions.length - 1];
		} else {
			ngRoot.modalInfo = ng.transactions[ngRoot.modalInfo.index - 1];
		}
	}

	ngRoot.modalInfoNext = function() {
		if ((ngRoot.modalInfo.index + 1) >= ng.transactions.length) {
			ngRoot.modalInfo = ng.transactions[0];
		} else {
			ngRoot.modalInfo = ng.transactions[ngRoot.modalInfo.index + 1];
		}
	}

	ng.loaderReasons = true;
	API.getDisaprovalReasons()
	.then(function(response) {
		// SUCESSO
		ngRoot.disaprovalReasons = response.data;
	}, function(response) {
		// ERRO
		notify({
			type: 'error'
			, title: 'Erro ao executar a operação!'
		})
	})
	.then(function(response) {
		if ($routeParams.typePromo == 'BUY_AND_WIN') {
			return API.getPromoBuyAndWinCoupons($routeParams.idPromo, $routeParams.idUser)
		} else {
			return $q.reject(response);
		}
	})
	.then(function(response) {
		// SUCESSO
		if (!response.data) {
			return $q.reject(response);
		}

		ng.actualCard = {
			obj : response.data[0]
		};

	}, function(response) {
		if ($routeParams.typePromo == 'BUY_AND_WIN' && response.status == 404) {
			notify({
				type: 'error'
				, title: 'Erro'
				, desc: 'O consumidor não possui cupons!'
			})
			_.URL.path('/analise-de-cupom/' + $routeParams.typePromo + '/' + $routeParams.idPromo + '/')
		}
	})
	.finally(function() {
		getActualCard();
		ng.loaderReasons = false;
	})

}]);
