app.controller('Participants2CardsCtrl', [
	'$scope'
	, '$rootScope'
	, '$location'
	, '$routeParams'
	, '_'
	, 'API'
	, '$q'
	, '$filter'
	, function(ng, ngRoot, $location, $routeParams, _, API, $q, $filter) {

	var getCards = function() {
		ng.loaderReports = true;

		API.getPromoFoodRewardCards($routeParams.idPromo, $routeParams.idUser)
		.then(function(response) {
			// SUCESSO
			if (!response.data) {
				return $q.reject(response);
			}

			ngRoot.cards = [];
			var dataLength = response.data.length;
			for (var i = 0; i < dataLength; i++) {
				ngRoot.cards.push({
					id: response.data[i].cardId
					, main: {
						icon: 'tnt-photo-coupon'
						, href: '/analise-de-cupom/' + $routeParams.typePromo + '/' + $routeParams.idPromo + '/usuario/' + $routeParams.idUser + '/cartoes/' + response.data[i].cardId
						, listText : [
							{
								text: 'Primeiro envio em ' + $filter('data_timestamp')(response.data[i].firstSubmitionDate)
								, class: 'small'
							}
							, {
								text: 'Cartão completado em ' + $filter('data_timestamp')(response.data[i].cardCompleteDate)
								, class: 'small'
							}
						]
					}
					// , feature : {
					// 	icon: response.data[i].reedemDate ? 'tnt-check2 cl-n-success' : 'tnt-close cl-n-error'
					// 	, class: 'small'
					// 	, text: response.data[i].reedemDate ? 'Prêmio Resgatado' : 'Prêmio Não Resgatado'
					// }
					, obj: response.data[i]
				})
			}

		}, function(response) {
			if (response.status == 404) {
				notify({
					type: 'error'
					, title: 'Erro'
					, desc: 'O consumidor não possui mais cartões!'
				})
				_.URL.path('/analise-de-cupom/' + $routeParams.typePromo + '/' + $routeParams.idPromo + '/')
			}
		})
		.finally(function() {
			ng.loaderReports = false;
		})
	}
	getCards();

}]);
