app.controller('SimuladorController', [
	'$scope'
	, '$rootScope'
	, 'API'
	, '$routeParams'
	, '_'
	, function(ng, ngRoot, API, $routeParams, _) {

		ng.send_transaction = function() {

			ng.loaderSimulator = true;

			var obj  = angular.copy(ng.data);

			obj.date = _.replaceAll(obj.date, '[-\ :]', '');
			obj.date = obj.date.substring(0, 4) + '-' +
								 obj.date.substring(4, 6) + '-' +
								 obj.date.substring(6, 8) + 'T' +
								 obj.date.substring(8, 10) + ':' +
								 obj.date.substring(10, 12) + ':' +
								 obj.date.substring(12, 14) + '.000Z';

			if (ng.data.coo != null) {
				obj.coo  = obj.coo.toString();
			}

			if (ng.data.cpf != '') {
				obj.cpf  = _.replaceAll(obj.cpf, /[.\-\/]/, '');
			}

			if (ng.data.cnpf != '') {
				obj.cnpj = _.replaceAll(obj.cnpj, /[.\-\/]/, '');
			}

			if (ng.data.applyBonus != ''){
				obj.applyBonus = [obj.applyBonus];
			}else{
				obj.applyBonus = [];
			}


			var promotionPaymentValidation = false;

			ng.data.productsNew = [];

			angular.forEach(ng.data.products, function(value, index2) {
				prodNew = {}
				prodNew['productId'] = value;
				if (ng.products_quantity[index2] != false && ng.products_quantity[index2] != undefined){
					prodNew['quantity'] = ng.products_quantity[index2];
					promotionPaymentValidation = true;
				}
				if (ng.products_subtotal[index2] != false && ng.products_subtotal[index2] != undefined){
					prodNew['subtotal'] = ng.products_subtotal[index2];
				}
				ng.data.productsNew.push(prodNew)
			})

			if (promotionPaymentValidation == true){
				if (ng.data.productsNew.length > 0){
					obj.products = ng.data.productsNew;
				}
			}

			console.log(obj);

			API.promotionValidation(obj, ng.BasicTransaction, promotionPaymentValidation)
			.then(function(result){

				ng.transaction_whatch(obj.transactionId);

			}, function(result){

				ng.ErrorSend = result;

			})
			.finally(function(){
				ng.loaderSimulator = false;
			})

		}

		ng.transaction_whatch = function(transaction_id) {

			ng.loaderSimulator = true;

			API.promotionResult(transaction_id, ng.BasicTransaction)
			.then(function(result){


				if (result.data) {

					ng.loaderSimulator    = false;
					ng.result_transaction = result.data;

				} else {
					setTimeout(function(){
						ng.transaction_whatch(transaction_id);
					}, 2000)
				}

			})
		}

		ng.add_remove_product = function(index) {

			if (ng.products_checked[index]) {

				ng.data.products.push(ng.products[index].text);

			} else {

				var title_check = ng.products[index].text;

				angular.forEach(ng.data.products, function(value, index2) {

					if (value == title_check) {
						ng.data.products.splice(index2, 1);
					}
				})
			}
		}


		ng.getParticipations = function(cpf) {

			var cpf_send = angular.copy(cpf);
			cpf_send  = _.replaceAll(cpf_send, /[.\-\/]/, '');

			ng.loaderExtratic = true;
			ng.user_not_found = false;

			if (cpf_send.length == 11) {

				API.promotionParticipations($routeParams.idPromo, cpf_send, ng.BasicTransaction)
				.then(function(result){

					ng.extract_participations = result.data;

				}, function(result){
					if (result.status == 404) {
						ng.user_not_found = true;
					}
				})
				.finally(function(response){
					ng.loaderExtratic = false;
				})
			}

		}

		ng.tryAgain = function() {
			ng.ErrorSend = false;
		}

		ng.init = function() {
			var date  = new Date();
			var today = date.getFullYear() + '-' +
									("0" + (date.getMonth() + 1)).slice(-2) + '-' +
									("0" + (date.getDate())).slice(-2) + '-' +
									("0" + (date.getHours())).slice(-2) + ':' +
									("0" + (date.getMinutes())).slice(-2) + ':' +
									("0" + (date.getSeconds())).slice(-2)

			ng.data = {
				promotionId: $routeParams.idPromo
				, transactionId: _.guid()
				, products : []
				, imageUrl: 'https://s3.amazonaws.com/freepy-images/e1a00d24-d9b5-42fd-8948-1abfec25bdde.jpg'
				, date: today
				, total: null
				, applyBonus: ''

				, cpf: ''
				, cnpj: ''
				, coo: null
				, paymentMethod: 'PAYPAL'
			}

			ng.products = [
			    {
			        "text" : "100",
			        "score" : 85,
			        "excluded" : false,
			        "minimumValue" : 69.94
			    },
			    {
			        "text" : "12",
			        "score" : 85,
			        "excluded" : false,
			        "minimumValue" : 69.94
			    },
			    {
			        "text" : "13",
			        "score" : 85,
			        "excluded" : false,
			        "minimumValue" : 69.94
			    },
			    {
			        "text" : "22",
			        "score" : 85,
			        "excluded" : false,
			        "minimumValue" : 69.94
			    },
			    {
			        "text" : "32",
			        "score" : 85,
			        "excluded" : false,
			        "minimumValue" : 69.94
			    },
			    {
			        "text" : "200",
			        "score" : 85,
			        "excluded" : false,
			        "minimumValue" : 49.99
			    },
			    {
			        "text" : "300",
			        "score" : 85,
			        "excluded" : false,
			        "minimumValue" : 24.94
			    },
			    {
			        "text" : "VPOWER",
			        "score" : 85,
			        "excluded" : false,
			        "minimumValue" : 69.94
			    }
			]

			ng.products_checked = [];
			ng.products_quantity  = [];
			ng.products_subtotal= [];
			ng.result_transaction = false;
		}


		ng.result_transaction = false;
		ng.loaderSimulator = true;
		ng.menu = 'simulator';
		ng.response_code = '';

		API.getTransactionToken()
		.then(function(result) {

			//ng.BasicTransaction = 'Basic ' + btoa(result.data.token)
			ng.BasicTransaction = result.data.token
			ng.loaderSimulator = false;
			ng.init();
		})

	}
]);
