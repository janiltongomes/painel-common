// ROTAS
app.config([
	'$routeProvider'
	, '$sceDelegateProvider'
	, function($routeProvider, $sceDelegateProvider) {

	$routeProvider

	// INDICADORES
	.when('/indicadores/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/indicadores/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'indicadores';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/indicadores/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// PARTICIPANTES
	.when('/participantes/:typePromo/:idPromo/detalhe/:idUser/cartoes/:idCard', {
		templateUrl : function(attr) {
			return '/common/pages/participantes/view.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'participantes';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/participantes/:typePromo/:idPromo/usuario/:idUser/', {
		templateUrl : function(attr) {
			return '/common/pages/participantes/cards.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'participantes';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/participantes/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/participantes/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'participantes';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/participantes/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// ACOMPANHAMENTO DE CADASTRO
	.when('/acompanhamento-de-cadastro/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/acompanhamento-de-cadastro/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'acompanhamento-de-cadastro';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/acompanhamento-de-cadastro/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// EXPORTACAO
	.when('/exportacao/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/exportacao/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'exportacao';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/exportacao/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// RESGATES
	.when('/resgates/:typePromo/:idPromo/detalhe/:idUser', {
		templateUrl : function(attr) {
			return '/common/pages/resgates/view.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'resgates';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/resgates/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/resgates/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'resgates';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/resgates/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// ANALISE DE CUPOM
	.when('/analise-de-cupom/:typePromo/:idPromo/usuario/:idUser/cartoes/:idCard/', {
		templateUrl : function(attr) {
			return '/common/pages/analise-de-cupom/view.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'analise-de-cupom';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/analise-de-cupom/:typePromo/:idPromo/usuario/:idUser/', {
		templateUrl : function(attr) {
			if (attr.typePromo == 'BUY_AND_WIN') {
				return '/common/pages/analise-de-cupom/view.html';
			} else {
				return '/common/pages/analise-de-cupom/cards.html';
			}
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'analise-de-cupom';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/analise-de-cupom/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/analise-de-cupom/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'analise-de-cupom';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/analise-de-cupom/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// OCR
	.when('/analise-de-ocr/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/analise-de-ocr/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'analise-de-ocr';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/analise-de-ocr/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// REANALISE DE CUPOM
	.when('/reanalise-de-cupom/:typePromo/:idPromo/usuario/:idUser/cartoes/:idCard/', {
		templateUrl : function(attr) {
			return '/common/pages/reanalise-de-cupom/view.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'reanalise-de-cupom';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/reanalise-de-cupom/:typePromo/:idPromo/usuario/:idUser/cartoes/:idCard/:statusCard/', {
		templateUrl : function(attr) {
			return '/common/pages/reanalise-de-cupom/view.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'reanalise-de-cupom';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/reanalise-de-cupom/:typePromo/:idPromo/usuario/:idUser/', {
		templateUrl : function(attr) {
			if (attr.typePromo == 'BUY_AND_WIN') {
				return '/common/pages/reanalise-de-cupom/view.html';
			} else {
				return '/common/pages/reanalise-de-cupom/cards.html';
			}
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'reanalise-de-cupom';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/reanalise-de-cupom/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/reanalise-de-cupom/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'reanalise-de-cupom';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/reanalise-de-cupom/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// OCR
	.when('/reanalise-de-ocr/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/analise-de-ocr/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'analise-de-ocr';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/reanalise-de-ocr/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// BLACKLIST
	.when('/blacklist/:idPromo/novo/', {
		templateUrl : function(attr) {
			return '/common/pages/blacklist/insert-update.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'blacklist';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/blacklist/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/blacklist/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'blacklist';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/blacklist/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	.when('/simulador-promocoes/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/simulador-promocoes/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'simulador-promocoes';
				$rootScope.activationMenu.init();
			}
		}
	})

	.when('/simulador-promocoes/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// ANALISE DE CUPOM 2
	.when('/analise-de-cupom-2/:typeFilter/', {
		templateUrl : function(attr) {
			return '/common/pages/analise-de-cupom-2/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'analise-de-cupom-2';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/analise-de-cupom-2/:typeFilter/:filterCPF/', {
		templateUrl : function(attr) {
			return '/common/pages/analise-de-cupom-2/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'analise-de-cupom-2';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/analise-de-cupom-2/', {
		templateUrl : function(attr) {
			return '/common/pages/analise-de-cupom-2/index.html';
		}
	})

	.when('/top-ganhadores/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/top-ganhadores/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'top-ganhadores';
				$rootScope.activationMenu.init();
			}
		}
	})

	.when('/top-ganhadores/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// INDICADORES
	.when('/participacoes-etapa/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/participacoes-etapa/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'participacoes-etapa';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/participacoes-etapa/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// ULTIMAS TRANSACOES
	.when('/ultimas-transacoes/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/ultimas-transacoes/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'ultimas-transacoes';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/ultimas-transacoes/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// INDICADORES
	.when('/comparativo-promocoes/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/comparativo-promocoes/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'comparativo-promocoes';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/comparativo-promocoes/', {
		// templateUrl : function(attr) {
		// 	return '/common/pages/promocao/list.html';
		// }
		templateUrl : function(attr) {
			return '/common/pages/comparativo-promocoes/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'comparativo-promocoes';
				$rootScope.activationMenu.init();
			}
		}
	})

	// INDICADORES
	.when('/detalhes-etapas/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/detalhes-etapas/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'detalhes-etapas';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/detalhes-etapas/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	//products
	// .when('/top-ganhadores/:typePromo/:idPromo/', {
	// 	templateUrl : function(attr) {
	// 		return '/common/pages/top-ganhadores/index.html';
	// 	}
	// 	, controller : function($rootScope, $routeParams) {
	// 		if ($rootScope.activationMenu) {
	// 			$rootScope.activationMenu.currentSlug = 'top-ganhadores';
	// 			$rootScope.activationMenu.init();
	// 		}
	// 	}
	// })

	.when('/products/', {
		templateUrl : function(attr) {
			return '/common/pages/products/index.html';
		}
	})

	// INDICADORES
	.when('/indicadores-performance/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/indicadores-performance/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'indicadores-performance';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/indicadores-performance/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})


	// INDICADORES
	.when('/indicadores-engajamento/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/indicadores-engajamento/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'indicadores-engajamento';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/indicadores-engajamento/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// INDICADORES
	.when('/indicadores-recusa/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/indicadores-recusa/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'indicadores-recusa';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/indicadores-recusa/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// INDICADORES
	.when('/indicadores-vendas/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/indicadores-vendas/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'indicadores-vendas';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/indicadores-vendas/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// BLACKLIST
	.when('/promocoes-cnpj/:idPromo/novo/', {
		templateUrl : function(attr) {
			return '/common/pages/promocoes-cnpj/insert-update.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'promocoes-cnpj';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/promocoes-cnpj/:typePromo/:idPromo/', {
		templateUrl : function(attr) {
			return '/common/pages/promocoes-cnpj/index.html';
		}
		, controller : function($rootScope, $routeParams) {
			if ($rootScope.activationMenu) {
				$rootScope.activationMenu.currentSlug = 'promocoes-cnpj';
				$rootScope.activationMenu.init();
			}
		}
	})
	.when('/promocoes-cnpj/', {
		templateUrl : function(attr) {
			return '/common/pages/promocao/list.html';
		}
	})

	// PADRAO | DEFAULT
	.when('/:slug/novo/', {
		templateUrl : function(attr) {
			return '/pages/' +  attr.slug + '/insert-update.html'
		}
	})
	.when('/:slug/editar/', {
		templateUrl : function(attr) {
			return '/pages/' +  attr.slug + '/insert-update.html'
		}
	})
	.when('/:slug/', {
		templateUrl : function(attr) {
			return '/common/pages/' + attr.slug + '/index.html';
		}
	})

	$sceDelegateProvider
	.resourceUrlWhitelist([
		'self'
		, '#PATH_TRINITY/**'
		, 'data**'
		// , 'http://ftccards.s3.amazonaws.com'
	]);

}])

app.directive('countdown', [
	'$interval'
	, '$rootScope'
	, function($interval, ngRoot) {
		return {
			restrict: 'A',
			scope: {
				date       : '@'
				, callback : '&'
			},
			link: function(scope, element, attrs) {

				var dhms = function(t) {
					var days, hours, minutes, seconds;
					days = Math.floor(t / 86400);
					t -= days * 86400;
					hours = Math.floor(t / 3600) % 24;
					t -= hours * 3600;
					minutes = Math.floor(t / 60) % 60;
					t -= minutes * 60;
					seconds = t % 60;
					// return [/*days + 'd', hours + 'h', */('0'+minutes.slice(-2)) + ':', ('0'+seconds.slice(-2))].join('');
					return (('0'+minutes).slice(-2)) + ':' + (('0'+seconds).slice(-2));
				}

				attrs.$observe('date', function(value) {
					if (!value) {
						return false;
					}

					$interval.cancel(scope.countdownTime);

					var future = new Date(_.replaceAll(scope.date, '(\"|\')', ''));

					scope.countdownTime = $interval(function() {
						var diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);

						element.text(dhms(diff));

						if (diff == 0) {
							scope.callback();

							$interval.cancel(scope.countdownTime);
						}
					}, 1000);

				});

				ngRoot.$on('$routeChangeSuccess', function(evt, absNewUrl, absOldUrl) {
					$interval.cancel(scope.countdownTime);
				});

			}
		};
	}
])
