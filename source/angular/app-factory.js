app.factory('API', [
	'$http'
	, '$rootScope'
	, function($http, ngRoot) {

	var
		projectHash   = _.Storage.get('project_hash')
		, dataFactory = {
			guid: function() {
			  function s4() {
			    return Math.floor((1 + Math.random()) * 0x10000)
			      .toString(16)
			      .substring(1);
			  }
			  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
			    s4() + '-' + s4() + s4() + s4();
			},
			getUrlTypes: function(typePromo) {
				url = ''
				if (typePromo == 'BUY_AND_WIN') {
					url = 'buyandwin';
				} else {
					url = 'foodreward';
				}
				return url
			}
			// PROMOCAO
			, getPromos: function() {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/promotion'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}
			, getPromoById: function(id) {
				return $http({
					method   : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/promotion/' + id
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			// GANHADOR RESGATOU
			, getPromoWinnerRedeemed: function(idPromo, idUser) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/promotion/' + idPromo + '/redeemed/' + idUser
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			// FIXME: Remover Futuramente
			// GANHADORES
			, getPromoWinners: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/promotion/' + idPromo + '/users/winners/' + skip + '/' + limit
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}
			, getPromoWinnersDate: function(idPromo, params, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/promotion/' + idPromo + '/users/winner/' + skip + '/' + limit
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}
			, getPromoConsumersDate: function(idPromo, params, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/promotion/' + idPromo + '/users/' + skip + '/' + limit
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}
			, getPromoConsumersByCPF: function(idPromo, cpf) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/promotion/' + idPromo + '/user/?cpf=' + cpf
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}
			, getPromoConsumersByEmail: function(idPromo, email, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/promotion/' + idPromo + '/user/?email=' + email
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}
			, getPromoConsumersByConsumerId: function(idPromo, consumerid, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/promotion/' + idPromo + '/user/?consumerid=' + consumerid
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}
			// FIXME

			// CONSUMIDORES
			, getPromoFoodRewardConsumers: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/promotion/' + idPromo + '/users/' + skip + '/' + limit
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}
			, getPromoBuyAndWinConsumers: function(idPromo, params) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/promotion/buyandwin/' + idPromo + '/all'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}

			// GANHADORES
			, getPromoFoodRewardWinnerDetails: function(idPromo, idUser) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/promotion/' + idPromo + '/winner/' + idUser
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}
			, getPromoBuyAndWinWinnerDetails: function(idPromo, idUser) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/promotion/buyandwin/' + idPromo + '/detail/' + idUser
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			// PRODUTO
			, postProductFound: function(obj) {
				return $http({
					method     : 'PUT'
					, url      : ngRoot.conf.api_url + 'admin/product/found/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
					, data     : JSON.stringify(obj)
				});
			}
			, postProductNotFound: function(obj) {
				return $http({
					method     : 'PUT'
					, url      : ngRoot.conf.api_url + 'admin/product/notfound/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
					, data     : JSON.stringify(obj)
				});
			}

			// CUPOM
			, postCouponDisqualify: function(obj) {
				return $http({
					method     : 'POST'
					, url      : ngRoot.conf.api_url + 'admin/coupon/disqualified/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
					, data     : JSON.stringify(obj)
				});
			}
			, postCouponQualify: function(obj) {
				return $http({
					method     : 'POST'
					, url      : ngRoot.conf.api_url + 'admin/coupon/qualified/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
					, data     : JSON.stringify(obj)
				});
			}

			// RELATORIOS
			, getReports: function(object) {
				return $http.get(
					ngRoot.conf.api_url + 'client'
					, {
						params : object
						, headers  : {
							'Content-Type'    : 'application/json'
							, 'Authorization' : ngRoot.user.authorization
							, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
						}
					}
				);
			}
			// , getReportIndicators: function(idPromo, params) {
			// 	console.log(ngRoot.conf.api_url);
			// 	return $http({
			// 		method     : 'GET'
			// 		, url      : ngRoot.conf.api_url + 'ftc/admin/indicators/' + idPromo
			// 		, headers  : {
			// 			'Content-Type'    : 'application/json'
			// 			, 'Authorization' : ngRoot.user.authorization
			// 			, 'UserId'        : ngRoot.user.userId
			// 			, 'ProjectId'     : projectHash
			// 		}
			// 		, params   : params
			// 		, dataType : 'json'
			// 	});
			// }
			, getReportIndicators: function(type, params) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_consolidacao + 'consolidate/' + type
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}
			, getReportResgates: function(idPromo, typePromo, params, excel, skip, limit) {

				excel_comp = '';
				typePromo  = 'foodreward';
				if (excel === true) {
					excel_comp += 'csv/'
				}

				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/promotion/' + typePromo + '/' + idPromo + '/allWinners/' + skip + '/' + limit + '/' + excel_comp + params
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			// TRANSACOES
			, getPromoTransactions: function(idPromo) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'ftc/admin/transactions/' + idPromo
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}
			, getPromoTransaction: function(idTransaction) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'ftc/admin/transaction/' + idTransaction
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}
			, getReportsRegister: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'ftc/admin/users/' + idPromo + '/' + skip + '/' + limit
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}
			, getReportsRegisterExcel: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'ftc/admin/users/' + idPromo + '/' + skip + '/' + limit + '/csv'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
				});
			}
			, getReportsRegisterExcel_totaly: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'ftc/admin/users/' + idPromo + '/csv'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
				});
			}
			, getReportsRegisterDetailsExcel: function(idPromo) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'ftc/admin/transactions/' + idPromo + '/csv'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
				});
			}

			// PROMOCAO MECANICA 2
			, getPromoFoodReward: function(idPromo, params) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/promotion/foodreward/' + idPromo + '/review'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}
			, getPromoFoodRewardCards: function(idPromo, idUser) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/promotion/' + idPromo + '/foodreward/' + idUser + '/cardsToReview'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			// PROMOCAO MECANICA 3
			, getPromoBuyAndWin: function(idPromo, params) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/promotion/buyandwin/' + idPromo + '/review'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}
			, getPromoBuyAndWinCoupons: function(idPromo, idUser) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/promotion/' + idPromo + '/buyandwin/' + idUser + '/couponsToReview'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			// RAZOES PARA DESAPROVACAO
			, getDisaprovalReasons: function(idPromo, params) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/promotion/foodreward/disaprovalreasons'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}

			, postFoodRewardCouponApproval: function(idPromo, obj) {
				return $http({
					method     : 'POST'
					, url      : ngRoot.conf.api_url + 'admin/promotion/' + idPromo + '/foodreward/approval'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, data     : JSON.stringify(obj)
					, dataType : 'json'
				});
			}
			, postBuyAndWinCouponApproval: function(idPromo, obj) {
				return $http({
					method     : 'POST'
					, url      : ngRoot.conf.api_url + 'admin/promotion/' + idPromo + '/buyandwin/approval'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, data     : JSON.stringify(obj)
					, dataType : 'json'
				});
			}

			/// PROMOÇÕES
			, getPromotions: function(obj) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'admin/setup/promotion/foodreward/all'
					, params   : obj
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}
			, getClientsOne: function(object) {
				return $http.get(
					ngRoot.conf.api_one + 'clients'
					, {
						params : object
						, headers : {'Authorization' : ngRoot.user.authorization}
					}
				);
			}
			, savePromotion: function(json, methodSave) {
				return $http(
					{
						method   : methodSave
						, url      : ngRoot.conf.api_url + 'admin/setup/promotion/foodreward'
						, data     : JSON.stringify(json)
						, headers  : {'Authorization' : ngRoot.user.authorization}
						, dataType : 'json'
					}
				)
			}
			, getPromotion: function(id) {
				return $http(
					{
						method   : 'GET'
						, url      : ngRoot.conf.api_url + 'admin/setup/promotion/foodreward/'+id
						, headers  : {
							'Content-Type'    : 'application/json'
							, 'Authorization' : ngRoot.user.authorization
							, 'UserId'        : ngRoot.user.userId
							, 'ProjectId'     : projectHash
						}
						, dataType : 'json'
					}
				)
			}
			, getHtmlContent: function(link) {
				return $http(
					{
						method   : 'GET'
						, url      : ngRoot.conf.html_content+'/'+link
						, headers  : {
							'Content-Type'    : 'text/html'
						}
					}
				)
			}
			// RESGATE DE PREMIO
			, getWinner: function(idPromo, typePromo,  params) {
				type = dataFactory.getUrlTypes(typePromo)
				return $http(
					{
						method     : 'GET'
						, url      : ngRoot.conf.api_url + 'admin/promotion/' + idPromo + '/' + type +'/webwinner'
						, headers  : {
							'Content-Type'    : 'application/json'
							, 'Authorization' : ngRoot.user.authorization
							, 'UserId'        : ngRoot.user.userId
							, 'ProjectId'     : projectHash
						}
						, params   : params
						, dataType : 'json'
					}
				)
			}
			// RESGATE DE PREMIO
			, getBuyAndWinWinner: function(idPromo, params) {
				return $http(
					{
						method     : 'GET'
						, url      : ngRoot.conf.api_url + 'admin/promotion/' + idPromo + '/buyandwin/webwinner'
						, headers  : {
							'Content-Type'    : 'application/json'
							, 'Authorization' : ngRoot.user.authorization
							, 'UserId'        : ngRoot.user.userId
							, 'ProjectId'     : projectHash
						}
						, params   : params
						, dataType : 'json'
					}
				)
			}
			, postRedeem: function(idPromo, typePromo, params) {
				type = dataFactory.getUrlTypes(typePromo)
				return $http(
					{
						method     : 'POST'
						, url      : ngRoot.conf.api_url + 'admin/promotion/' + idPromo + '/' + type + '/webredeem'
						, headers  : {
							'Content-Type'    : 'application/json'
							, 'Authorization' : ngRoot.user.authorization
							, 'UserId'        : ngRoot.user.userId
							, 'ProjectId'     : projectHash
						}
						, data     : JSON.stringify(params)
						, dataType : 'json'
					}
				)
			}
			, postBuyAndWinRedeem: function(idPromo, params) {
				return $http(
					{
						method     : 'POST'
						, url      : ngRoot.conf.api_url + 'admin/promotion/' + idPromo + '/buyandwin/webredeem'
						, headers  : {
							'Content-Type'    : 'application/json'
							, 'Authorization' : ngRoot.user.authorization
							, 'UserId'        : ngRoot.user.userId
							, 'ProjectId'     : projectHash
						}
						, data     : JSON.stringify(params)
						, dataType : 'json'
					}
				)
			}


			// PROMOCAO RAIZEN
			, getPromosRaizen: function() {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_raizen + 'promotion/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}
			, getPromosSearchRaizen: function(params) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_raizen + 'promotion/search/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}
			, getPromoRaizenById: function(id) {
				return $http({
					method   : 'GET'
					, url      : ngRoot.conf.api_raizen + 'promotion/' + id
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, savePromotionRaizen: function(json, methodSave) {
				return $http(
					{
						method   : methodSave
						, url      : ngRoot.conf.api_raizen + 'promotion/'
						, data     : JSON.stringify(json)
						, headers  : {
							'Content-Type'    : 'application/json'
							, 'Authorization' : ngRoot.user.authorization
							, 'UserId'        : ngRoot.user.userId
							, 'ProjectId'     : projectHash
						}
						, dataType : 'json'
					}
				)
			}

			// BLACKLIST PROMOCAO
			, getBlacklistPromotion: function(id, params) {
				return $http({
					method   : 'GET'
					, url      : ngRoot.conf.api_raizen + 'blacklist/promotion/' + id
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}


			, insertBlacklistPromotion: function(params) {
				return $http({
					method     : 'POST'
					, url      : ngRoot.conf.api_raizen + 'blacklist/'
					//, url      : 'http://painellojista.localhost/api2/blacklist/'

					, data     : JSON.stringify(params)
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				})
			}

			, deleteBlacklistPromotion: function(id) {
				return $http({
					method     : 'DELETE'
					, url      : ngRoot.conf.api_raizen + 'blacklist/' + id
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				})
			}

			, deleteUpdateBlacklistPromotion: function(promoId, id) {
				return $http({
					method     : 'DELETE'
					, url      : ngRoot.conf.api_raizen + 'blacklist/promotion/' + promoId + '/id/' + id
					//, url      : 'http://painellojista.localhost/api2/blacklist/promotion/' + promoId + '/id/' + id
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				})
			}

			, getTransactionToken: function() {
				var exp = "homolog";
				var mystring = window.location.href;
				if(!(mystring.match(exp)))
				{
				  token = 'Basic cmFpemVuOjI2OTMxZjJjLWE3ZjctNDVkNC1iN2FiLWQ2MTk0MmE2M2MxZA=='
				}
				else
				{
				  token = 'Basic cmFpemVuOjYxYzZkZTVmLTM2NDUtNDMyYi05OWIzLWEyMzEyY2ZjMTQ3Yg=='
				}
				return $http({
					method   : 'GET'
					, url      : ngRoot.conf.api_raizen_common + 'authentication/token'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : token
					}
					, dataType : 'json'
				});
			}

			, promotionValidation: function(params, basic, promotionValidation) {
				if (promotionValidation == true){
					compurl = 'payment/';
				}else{
					compurl = '';
				}

				return $http({
					method   : 'POST'
					, url      : ngRoot.conf.api_raizen_common + 'promotion/'+ compurl + 'validation'
					, data     : JSON.stringify(params)
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : basic
					}
					, dataType : 'json'
				});
			}

			, promotionResult: function(transaction_id, basic) {
				return $http({
					method   : 'GET'
					, url      : ngRoot.conf.api_raizen_common + 'promotion/raizen/result/' + transaction_id
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : basic
					}
					, dataType : 'json'
				});
			}

			, promotionParticipations: function(promotion_id, cpf, basic) {
				return $http({
					method   : 'GET'
					, url      : ngRoot.conf.api_raizen_common + 'promotion/' + promotion_id + '/raizen/results/' + cpf
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : basic
					}
					, dataType : 'json'
				});
			}

			// ANALISE DE CUPOM 2
			, getCouponsTotals: function() {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_app + 'api_approval/participations/count'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getCouponForAnalysis: function(id, allCupons) {
				if (id && id != undefined){
					comp_id = '/' + id;
				}else{
					comp_id = '';
				}

				if (allCupons && allCupons != undefined){
					if (allCupons == true){
						comp_id += '/allcupons/true';
					}
				}
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_app + 'api_approval/participation' + comp_id
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, postCouponAnalyzed: function(params) {
				return $http({
					method     : 'POST'
					, url      : ngRoot.conf.api_app + 'api_approval/participation'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, data     : JSON.stringify(params)
					, dataType : 'json'
				});
			}

			, postCouponAnalyzedReapproval: function(params) {
				return $http({
					method     : 'POST'
					, url      : ngRoot.conf.api2 + 'reaproval'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, data     : JSON.stringify(params)
					, dataType : 'json'
				});
			}

			, postCouponAnalyzedReapprovalModeration: function(params) {
				return $http({
					method     : 'POST'
					, url      : ngRoot.conf.api2 + 'transactionmoderation'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, data     : JSON.stringify(params)
					, dataType : 'json'
				});
			}

			, postReleaseCouponForAnalysis: function(params) {
				return $http({
					method     : 'POST'
					, url      : ngRoot.conf.api_app + 'api_approval/transaction/updatecupom'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, data     : JSON.stringify(params)
					, dataType : 'json'
				});

			}

			, getCouponRejectReason: function() {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_app + 'api_approval/rejectreason'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getCouponsList: function(params) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_app + 'api_approval/participations'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}

			, getCouponsListbyCard: function(params) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_app + 'api_approval/participationsbycard'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}


			// ATIVAR PROMOCAO
			, getPromoEnable: function(idPromo) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_app + 'api_approval/promotion/' + idPromo + '/active/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			// DESATIVAR PROMOCAO
			, getPromoDisable: function(idPromo) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_app + 'api_approval/promotion/' + idPromo + '/disable/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			// ALTERAR ULTIMA PARTICIPACAO PROMOCAO
			, getPromoChangeLastParticipantion: function(cpfUser, idPromo) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_app + 'api_approval/user/' + cpfUser + '/promotion/' + idPromo + '/changelastparticipation/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			// ALTERAR LIMITES DIARIOS
			, getPromoChangeLimits: function(idPromo, days, week) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_app + 'api_approval/promotion/' + idPromo + '/limits/days/' + days + '/week/' + week + '/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			// CONSULTAR LIMITES DIARIOS
			, getPromoLimits: function(idPromo) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_app + 'api_approval/promotion/' + idPromo + '/limits/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Accept'        : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getReportRobots: function(obj) {
				return $http({
					method     : 'POST'
					, url      : ngRoot.conf.api_app + 'robots'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
					, data     : JSON.stringify(obj)
				});
			}
			, getPromoRaizenCards: function(idPromo, idUser, idCard) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/promotion/' + idPromo + '/' + idUser + '/cardsToReview'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getPromoCards: function(idPromo, idUser, idCard) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/promotion/' + idPromo + '/' + idUser + '/cardsToReview'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getUserCPF: function(idPromo, cpf, params) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/' + cpf + '/promotion/' + idPromo
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getPromoConsumersTopGanhadoresByCPF: function(idPromo, cpf) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/top_consumer/' + idPromo + '/cpf/' + cpf
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getPromoConsumersTopGanhadores: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/top_consumer/' + idPromo + '/' + skip + '/' + limit
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getPromoConsumersTopGanhadoresExport: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/top_consumer/' + idPromo + '/export'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getParticipationStep: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/participation_step/' + idPromo + '/' + skip + '/' + limit
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getWinnerStep: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/winner_step/' + idPromo + '/' + skip + '/' + limit
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getevolutioncoupons: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/evolution_coupons/' + idPromo + '/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getevolutioncouponsbycpf: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/evolution_coupons_cpf/' + idPromo + '/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getLastTransactions: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/transactions/' + idPromo + '/' + skip + '/' + limit
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getstepdetails: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/step_details/' + idPromo + '/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getPromoProductsByText: function(text, type) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/promotion/' + type + '/text/' + text
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getPromoProducts: function(skip, limit, type) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/promotion/' + type + '/' + skip + '/' + limit
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getPromoProductsExport: function(type) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/promotion/' + type + '/export'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getstatscardraizen: function(data) {
				if (data.approved == true){
					statsActualy = 'aprovado';
				}else if (data.statusApproved && data.statusApproved == 'CANCEL_BY_RULE_FIRST_SUBMIT_EXPIRED'){
					statsActualy = 'expirado';
				}else if (data.approved == false){
					statsActualy = 'reprovado';
				}else{
					statsActualy = 'andamento';
				}

				if (data.hasCompleted == true && _.isEmpty(data.approved)){
					statsActualy = 'moderacao';
				}

				return statsActualy;
			}

			, getIndicatorsEngagement: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/indicators_engagement/' + idPromo + '/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getIndicatorsEngagementExport: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/indicators_engagement/' + idPromo + '/export'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getIndicatorsPerformance: function(idPromo) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/indicators_performance/' + idPromo + '/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getIndicatorsPerformanceExport: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/indicators_performance/' + idPromo + '/export'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getIndicatorsError: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/indicators_error/' + idPromo + '/'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getIndicatorsErrorExport: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_url + 'api_approval/user/indicators_error/' + idPromo + '/export'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getIndicatorsSalesDirectors: function(idPromo, skip, limit) {
				return $http({
					method     : 'GET'
					//, url      : ngRoot.conf.api_url + 'api_approval/user/participation_step/' + idPromo + '/' + skip + '/' + limit
					//, url      : 'http://painellojista.localhost/indicadoresvendas.json'
					, url 		 : ngRoot.conf.api_consolidacao + 'sales/directors'
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getIndicatorsSales: function(idPromo, idDirector, skip, limit) {
				return $http({
					method     : 'GET'
					//, url      : ngRoot.conf.api_url + 'api_approval/user/participation_step/' + idPromo + '/' + skip + '/' + limit
					//, url      : 'http://painellojista.localhost/indicadoresvendas.json'
					, url 		 : ngRoot.conf.api_consolidacao + 'sales/information/' + idDirector
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				});
			}

			, getReportIndicators: function(type, params) {
				return $http({
					method     : 'GET'
					, url      : ngRoot.conf.api_consolidacao + 'consolidate/' + type
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}


			// BLACKLIST PROMOCAO
			, getPromotionStations: function(id, params) {
				return $http({
					method   : 'GET'
					, url      : ngRoot.conf.api_raizen + 'promotionstations/promotion/' + id
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, params   : params
					, dataType : 'json'
				});
			}


			, insertPromotionStations: function(params) {
				return $http({
					method     : 'POST'
					, url      : ngRoot.conf.api_raizen + 'promotionstations/'
					//, url      : 'http://painellojista.localhost/api2/blacklist/'

					, data     : JSON.stringify(params)
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				})
			}

			, deletePromotionStations: function(id) {
				return $http({
					method     : 'DELETE'
					, url      : ngRoot.conf.api_raizen + 'promotionstations/' + id
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				})
			}

			, deleteUpdatePromotionStations: function(promoId, id) {
				return $http({
					method     : 'DELETE'
					, url      : ngRoot.conf.api_raizen + 'promotionstations/promotion/' + promoId + '/id/' + id
					//, url      : 'http://painellojista.localhost/api2/blacklist/promotion/' + promoId + '/id/' + id
					, headers  : {
						'Content-Type'    : 'application/json'
						, 'Authorization' : ngRoot.user.authorization
						, 'UserId'        : ngRoot.user.userId
						, 'ProjectId'     : projectHash
					}
					, dataType : 'json'
				})
			}

			, getpromotionleaprule: function(promotionId) {
				if (promotionId == '5a736410824614e4965e9306'){
					return true;
				}

				return false;
			}

			, roundToTwo: function(num) {
				return +(Math.round(num + "e+2")  + "e-2");
			}


		}

	;

	return dataFactory;
}]);
