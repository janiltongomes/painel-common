'use strict';

/* DEPENDECIAS
-------------------------------------- */
var gulp        = require('gulp')
	, $           = require('gulp-load-plugins')()
	, opn         = require('opn')
	, del         = require('del')
	, args        = require('yargs').argv
	, runSequence = require('run-sequence')
	, browserSync = require('browser-sync')
	, reload      = browserSync.reload
	, config      = require('./gulpconf/config.json')
;

/* CONTANTS
-------------------------------------- */
var
	PROJECT_NAME       = 'painel.freepy'
	, TRINITY_URL      = config.URL_TRINITY
	, TRINITY_INCLUDES = TRINITY_URL + '/latest/html/includes'
	, PATH_TRINITY     = TRINITY_URL + '/latest'
	, PATH_ASSETS      = './assets'
	, PATH_PAGES       = '/common/pages'
	, PATH_INCLUDES    = '/common/includes'
	, AUTOPREFIXER     = [
		'ie >= 10',
		'ie_mob >= 10',
		'ff >= 30',
		'chrome >= 34',
		'safari >= 7',
		'opera >= 23',
		'ios >= 7',
		'android >= 4.4',
		'bb >= 10'
	]
;

/* FUNCOES
-------------------------------------- */
function showError(error) {
	// Exibe o erro no console
	console.log(error.toString());
	this.emit('end');
}

/* OBJ TASKS
-------------------------------------- */
var tasks = {
	Angular: function(prefix) {
		gulp.src([
			prefix + '/angular/**/*.js'
		])

		// Altera a constant para o valor
		.pipe($.replace(/#TRINITY_INCLUDES/g, TRINITY_INCLUDES))

		.pipe($.replace(/#PATH_TRINITY/g, PATH_TRINITY))

		.pipe($.sourcemaps.init())

			// Concatena os arquivos JavaScript
			.pipe($.concat('app.js'))

			// Exibe erros no console
			.on('error', showError)

			// Minifica o JavaScript
			.pipe($.uglify({
				preserveComments: false,
				mangle: false,
				compress: {
					// drop_console: true
				}
			}))

			// Exibe erros no console
			.on('error', showError)

		.pipe($.sourcemaps.write('maps'))

		// Destino dos arquivos de produção
		.pipe(gulp.dest('./build/' + prefix + '/assets/js/'))
	},
	Compress: function() {
		var date = new Date();
		var build_date = date.getFullYear() + '-' +
						("0" + (date.getMonth() + 1)).slice(-2) + '-' +
						("0" + (date.getDate())).slice(-2) + '-' +
						("0" + (date.getHours())).slice(-2) + '-' +
						("0" + (date.getMinutes())).slice(-2) + '-' +
						("0" + (date.getSeconds())).slice(-2)

		gulp.src([
			'./build/**/*',
		], {
			base: "../"
		})

		.pipe($.zip('build_' + build_date + '.zip'))

		// Show errors in terminal
		.on('error', showError)

		// Envia os arquivos gerados para a pasta themes/
		.pipe(gulp.dest('./versions/'));
	},
	Copy: function(prefix) {
		gulp.src([
			prefix + '/*',
			prefix + '/**/*.{css,js,gif,jpg,png}',
			'!' + prefix + '/images/',
			'!' + prefix + '/images/**',
			'!' + prefix + '/javascript/',
			'!' + prefix + '/javascript/**',
			'!' + prefix + '/sass/',
			'!' + prefix + '/sass/**',
			'!' + prefix + '/html/',
			'!' + prefix + '/angular/',
			'!' + prefix + '/angular/**',
			'!' + prefix + '/server/'
		], {
			dot: true
		})

		// Copia todos os arquivo que não são gerados pelo Gulp para produção
		.pipe(gulp.dest('./build/' + prefix + ''));

		// Copia os arquivos da pasta server para a raiz do wordpress
		gulp.src([
			prefix + '/server/**/*'
		], {
			dot: true
		})

		// Copia todos os arquivo que não são gerados pelo Gulp para produção
		.pipe(gulp.dest('./build/'));
	},
	CSS: function(prefix) {
		gulp.src([
			prefix + '/sass/main-styles/**/*.scss'
		])

		.pipe($.replace(/#PATH_TRINITY/g, PATH_TRINITY))

		.pipe($.cssimport({
			filter: /^(http:\/\/|https:\/\/)/gi
		}))

		.pipe($.sourcemaps.init())

			// Compila o SASS
			.pipe($.sass())

			// Exibe erros no console
			.on('error', showError)

			// Compila o SASS
			.pipe($.combineMq())

			// Exibe erros no console
			.on('error', showError)

			// Adiciona os prefixos dos browsers
			.pipe($.autoprefixer({
				browsers: AUTOPREFIXER
			}))

			// Minifica o CSS
			.pipe($.cssnano())

			// Exibe erros no console
			.on('error', showError)

		.pipe($.sourcemaps.write('maps'))

		// Destino dos arquivos de produção
		.pipe(gulp.dest('./build/' + prefix + '/assets/css/'))
	},
	HTML: function(prefix) {
		gulp.src([
			prefix + '/html/**/*.html'
		])

		.pipe($.htmlmin({
			removeComments        : true
			// , removeAttributeQuotes : true
			// , collapseWhitespace    : true
			// , minifyJS              : true
			// , minifyCSS             : false
		}))

		.pipe($.replace(/#PATH_ASSETS/g, PATH_ASSETS))

		.pipe($.replace(/#PATH_PAGES/g, PATH_PAGES))

		.pipe($.replace(/#PATH_INCLUDES/g, PATH_INCLUDES))

		.pipe($.replace(/#PATH_TRINITY/g, PATH_TRINITY))

		.pipe($.replace(/#TRINITY_INCLUDES/g, TRINITY_INCLUDES))

		// Destino dos arquivos de produção
		.pipe(gulp.dest('./build/' + prefix + '/'))
	},
	Img: function(prefix) {
		gulp.src([
			prefix + '/images/**/*.*',
			'!**/sprite/*',
			'!**/sprite/',
			'!**/touch/*',
			'!**/touch/'
		])

		// Otimiza as imagens
		.pipe($.imagemin({
			progressive: true,
			interlaced: true
		}))

		// Exibe erros no console
		.on('error', showError)

		// Destino dos arquivos de produção
		.pipe(gulp.dest('./build/' + prefix + '/assets/img/'));
	},
	JS: function(prefix) {
		gulp.src([
			prefix + '/javascript/**/*.js'
		])

		.pipe($.sourcemaps.init())

		// Concatena os arquivos JavaScript
		.pipe($.concat('all.js'))

		// Exibe erros no console
		.on('error', showError)

		// Minifica o JavaScript
		.pipe($.uglify({
			preserveComments: false,
			compress: {
				// drop_console: true
			}
		}))

		// Exibe erros no console
		.on('error', showError)

		.pipe($.sourcemaps.write('maps'))

		// Destino dos arquivos de produção
		.pipe(gulp.dest('./build/' + prefix + '/assets/js/'))
	},
	Server: function(prefix) {
		browserSync({
			notify      : false
			, logPrefix : PROJECT_NAME.toUpperCase()
			, ghostMode : false
			// , server    : "./build/source"
			, proxy     : PROJECT_NAME + '.dev'
		});
	},
	Sprite: function(prefix) {
		var spriteData = gulp.src(prefix + '/images/sprite/**/*.{png,jpg,gif}')
			.pipe($.spritesmith({
				imgName       : 'sprite.png' // Nome da imagem
				, imgPath     : '../img/sprite.png' // Caminho da imagem
				, cssName     : 'sprite-compiled.scss' // Arquivo compilado
				, cssFormat   : 'css'
				, cssTemplate : './gulpconf/spriteStr.scss.handlebars' // Formato da imagem
				, padding     : 4
				, algorithm   : 'binary-tree'
				, cssVarMap   : function(sprite) {
					sprite.image = '../img/sprite.png';
				}
			}))

		// Exibe erros no console
		.on('error', showError);

		// Envia a imagem compilada para produção
		spriteData.img.pipe(gulp.dest('./build/' + prefix + '/assets/img/'));

		// Envia o arquivo compilado com as classes para desenvolvimento
		spriteData.css.pipe(gulp.dest(prefix + '/sass/content/'));

		// Gera um arquivo de sprite com as variáveis de cada imagem
		var spriteData = gulp.src(prefix + '/images/sprite/**/*.{png,jpg,gif}')
			.pipe($.spritesmith({
				imgName: 'sprite.png', // Nome da imagem
				imgPath: '../img/sprite.png', // Caminho da imagem
				cssName: 'sprite-variables.scss', // Arquivo com as variáveis
				padding: 4,
				cssVarMap: function(sprite) {
					sprite.image = '../img/sprite.png';
				},
				algorithm: 'binary-tree'
			}))

		// Exibe erros no console
		.on('error', showError);

		// Envia o arquivo com as variáveis das imagens para desenvolvimento
		spriteData.css.pipe(gulp.dest(prefix + '/sass/_settings/'));
	},
	Svgicons : function(prefix) {
		gulp.src(prefix + '/images/svgicons/**/*')

		.pipe($.iconfont({
			fontName: PROJECT_NAME
			, appendUnicode: false
			, normalize: true
		}))

		.on('glyphs', function(glyphs) {
			var options = {
				glyphs: glyphs.map(function(glyph) {
					return {
						name: glyph.name
						, codepoint: glyph.unicode[0].charCodeAt(0)
					}
				})
				, fontName: PROJECT_NAME
				, fontPath: '../fonts/'
				, className: 'tnt'
			};

			gulp.src('./gulpconf/icon-fonts.scss')
			.pipe($.consolidate('lodash', options))
			.pipe(gulp.dest(prefix + '/sass/_settings/'))
		})

		.pipe(gulp.dest('./build/' + prefix + '/assets/fonts/'));
	},
	Watch: function(prefix) {
		gulp.watch([
			prefix + '/html/**/*.html'
		], ['html', reload]);
		gulp.watch([
			prefix + '/sass/**/*.{scss,css}'
		], ['css', reload]);
		gulp.watch([
			prefix + '/javascript/**/*.js'
		], ['js', reload]);
		gulp.watch([
			prefix + '/angular/**/*.js'
		], ['angular', reload]);
		gulp.watch([
			prefix + '/images/**/*',
			'!' + prefix + '/images/sprite/**/*',
			'!' + prefix + '/images/svgicons/**/*'
		], ['img', reload]);
		gulp.watch([
			prefix + '/images/sprite/**/*'
		], ['sprite', reload]);
		gulp.watch([
			prefix + '/images/svgicons/**/*'
		], ['svgicons', reload]);
		gulp.watch([
			prefix + '/*',
			prefix + '/**/*.{css,js}',
			'!' + prefix + '/images/',
			'!' + prefix + '/images/**',
			'!' + prefix + '/javascript/',
			'!' + prefix + '/javascript/**',
			'!' + prefix + '/sass/',
			'!' + prefix + '/sass/**'
		], ['copy', reload]);
	}
}

/* GULP TASK
-------------------------------------- */
gulp.task('html', function() {
	tasks.HTML('source');
});
gulp.task('css', function() {
	tasks.CSS('source');
});
gulp.task('js', function() {
	tasks.JS('source');
	runSequence(['angular']);
});
gulp.task('angular', function() {
	tasks.Angular('source');
});
gulp.task('img', function() {
	tasks.Img('source');
});
gulp.task('sprite', function() {
	tasks.Sprite('source');
	runSequence(['css', 'img']);
});
gulp.task('svgicons', function() {
	tasks.Svgicons('source');
});

gulp.task('compress', function() {
	tasks.Compress('source');
});
gulp.task('copy', function() {
	tasks.Copy('source');
});
gulp.task('clean', function(cb) {
	del(['./build/'], {
		force: true
	}, cb);
});

gulp.task('server', ['clean'], function() {
	runSequence('css', 'js', 'sprite', 'svgicons', 'html', 'copy', 'img');
	tasks.Server('source');

	tasks.Watch('source');
});

gulp.task('build', ['clean'], function(cb) {
	runSequence('css', 'js', 'sprite', 'svgicons', 'html', 'copy', 'img', cb);
});

/* TASK PADRAO
-------------------------------------- */
gulp.task('default', ['clean'], function() {
	runSequence('css', 'js', 'sprite', 'svgicons', 'html', 'copy');

	tasks.Watch('source');

	if (args.s) {
		// Passou o parametro -s
		runSequence('server');

	} else if (args.b) {
		// Passou o parametro -b
		runSequence('build');

	} else if (args.c) {
		// Passou o parametro -c
		runSequence('compress');

	}

});
